<?php

use App\Role;
use App\Permission;
use App\User;

use Illuminate\Database\Seeder;

class SetupAppTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name = "admin";
        $admin->display_name = "Admin";
        $admin->description = "User is Admin of Iconiq";
        $admin->save();

        $client = new Role();
        $client->name = "client";
        $client->display_name = "Client";
        $client->description = "User is Client of Iconiq";
        $client->save();

        $createUser = new Permission();
        $createUser->name = "create-users";
        $createUser->display_name = "Create Users";
        $createUser->description = "Create New Users";
        $createUser->save();

        $editUser = new Permission();
        $editUser->name = "edit-users";
        $editUser->display_name = "Edit Users";
        $editUser->description = "Edit Users";
        $editUser->save();

        $deleteUser = new Permission();
        $deleteUser->name = "delete-users";
        $deleteUser->display_name = "Delete Users";
        $deleteUser->description = "Delete Users";
        $deleteUser->save();
        
        $admin->attachPermissions(array($createUser, $editUser, $deleteUser));
        

        $user = new User;
        $user->first_name = "Dhaval";
        $user->last_name = "Patel";
        $user->email = "thummardhaval64@gmail.com";
        $user->password = Hash::make('admin');
        $user->phone_number = "9687510257";
        $user->save();

        $user->attachRole($admin);

        $user1 = new User;
        $user1->first_name = "Mayur";
        $user1->last_name = "Patel";
        $user1->email = "meerhirpara009@gmail.com";
        $user1->password = Hash::make('admin');
        $user1->phone_number = "1234567890";
        $user1->save();

        $user1->attachRole($admin);

        $user2 = new User;
        $user2->first_name = "Punit";
        $user2->last_name = "Kathiriya";
        $user2->email = "punitkathiriya@gmail.com";
        $user2->password = Hash::make('admin');
        $user2->phone_number = "1234567890";
        $user2->save();

        $user2->attachRole($admin);

        $user3 = new User;
        $user3->first_name = "Jatin";
        $user3->last_name = "Raiyani";
        $user3->email = "jatin@micrasolution.com";
        $user3->password = Hash::make('admin');
        $user3->phone_number = "8469536345";
        $user3->save();

        $user3->attachRole($admin);
    }
}
