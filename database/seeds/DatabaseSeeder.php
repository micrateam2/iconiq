<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name = "admin";
        $admin->display_name = "Admin";
        $admin->description = "User is Admin of Iconiq";
        $admin->save();

        $client = new Role();
        $client->name = "client";
        $client->display_name = "Client";
        $client->description = "User is Client of Iconiq";
        $client->save();

        $createUser = new Permission();
        $createUser->name = "create-users";
        $createUser->display_name = "Create Users";
        $createUser->description = "Create New Users";
        $createUser->save();

        $editUser = new Permission();
        $editUser->name = "edit-users";
        $editUser->display_name = "Edit Users";
        $editUser->description = "Edit Users";
        $editUser->save();

        $deleteUser = new Permission();
        $deleteUser->name = "delete-users";
        $deleteUser->display_name = "Delete Users";
        $deleteUser->description = "Delete Users";
        $deleteUser->save();
    }
}
