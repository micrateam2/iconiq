<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class inquiryProduct extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
     
       Schema::create('inquiryProduct', function (Blueprint $table) {
       $table->increments('id');
        $table->integer('inquiryId')->unsigned();
       $table->foreign('inquiryId')->references('id')->on('inquiry')->onDelete('cascade');
       $table->integer('productId')->unsigned();
       $table->foreign('productId')->references('id')->on('product')->onDelete('cascade');
       $table->string('productQuantity');
       $table->string('productPrice');
       $table->string('totalAmount');
       $table->string('createBy');
       $table->string('modifyBy');
       $table->timestamps();
   });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
      Schema::drop('inquiryProduct');
   }
}