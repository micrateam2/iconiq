<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class productImage extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::create('productImage', function (Blueprint $table) {
      $table->increments('id'); 
      $table->integer('productId')->unsigned();
      $table->foreign('productId')->references('id')->on('product')->onDelete('cascade');
      $table->string('productImageName');
      $table->enum('status', ['true', 'false']);
      $table->string('createBy');
      $table->string('modifyBy');
      $table->timestamps();

      });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::drop('productImage');
  }
}