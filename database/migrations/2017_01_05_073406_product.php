<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class product extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
     Schema::create('product', function (Blueprint $table) {
     $table->increments('id');
     $table->integer('categoryId')->unsigned();
     $table->foreign('categoryId')->references('id')->on('category')->onDelete('cascade');
     $table->integer('subCategoryId')->unsigned();       
     $table->foreign('subCategoryId')->references('id')->on('subcategory')->onDelete('cascade');
     $table->string('productName');
     $table->string('productItemNumber');  
     /*Diamond Department */    
     $table->string('diamondShape')->nullable();
     $table->string('diamondQualitySize')->nullable();
     $table->string('diamondPisces')->nullable();
     $table->string('diamondWeight')->nullable();
     $table->integer('diamondRate')->nullable();
     $table->integer('diamondAmount');

     /*Gold Department*/
     $table->string('goldQuality');
     $table->string('goldWeight');
     $table->integer('goldRate');
     $table->integer('goldAmount');

     /*Stone Department*/
     $table->string('stoneShape')->nullable();
     $table->string('stoneQuality')->nullable();
     $table->string('stoneSize')->nullable();
     $table->string('stonePisces')->nullable();
     $table->string('stoneWeight')->nullable();
     $table->integer('stoneRate')->nullable();
    $table->integer('stoneAmount');

    /*labour Department*/
     $table->integer('labourRate')->nullable();
     $table->integer('labourAmount')->nullable();

     $table->integer('totalAmount');
    $table->enum('isActive', ['true', 'false']);
     $table->string('gender');
          $table->enum('featureProduct', ['true', 'false']);

     $table->string('createBy');
     $table->string('modifyBy');
     $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::drop('product');
  }
}