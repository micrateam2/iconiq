<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Country extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('country', function (Blueprint $table) {
      $table->increments('id');
      $table->string('sortname');    
      $table->string('countryName');      
      $table->string('createBy');
      $table->string('modifyBy');
      $table->timestamps();
  });
     
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::drop('country');
   }
}