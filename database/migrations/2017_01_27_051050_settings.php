<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Settings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('settings', function (Blueprint $table) {
          $table->increments('id'); 
          $table->string('email');
          $table->string('phoneNumber');
          $table->string('officeNumber');
          $table->text('address');
          $table->enum('status', ['true', 'false']);
          $table->string('createBy');
          $table->string('modifyBy');
          $table->timestamps();
      });
  }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
