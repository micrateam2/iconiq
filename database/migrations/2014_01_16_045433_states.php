<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class States extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('states', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('countryId')->unsigned();
      $table->foreign('countryId')->references('id')->on('country')->onDelete('cascade');
      $table->string('stateName');      
      $table->string('createBy');
      $table->string('modifyBy');
      $table->timestamps();
  });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::drop('states');
   }
}