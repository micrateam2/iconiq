<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class contactUs extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::create('contactUs',function(Blueprint $table){
          $table->increments('id');
          $table->string('firstName');
          $table->string('lastName');
          $table->string('email');
          $table->string('phoneNumber');
          $table->text('message');
          $table->string('createBy')->nullable();
          $table->string('modifyBy')->nullable(); 
          $table->timestamps();
     });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::drop('contactUs');
  }
}