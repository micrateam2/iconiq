<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cities extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('cities', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('stateId')->unsigned();
      $table->foreign('stateId')->references('id')->on('states')->onDelete('cascade');
      $table->string('cityName');      
      $table->string('createBy');
      $table->string('modifyBy');
      $table->timestamps();
  });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::drop('cities');
   }
}