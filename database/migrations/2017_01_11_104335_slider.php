<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Slider extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::create('slider',function (Blueprint $table){
        $table->increments('id');
        $table->string('sliderName');
        $table->string('sliderImage');
        $table->enum('sliderStatus', ['true', 'false']);
        $table->string('createBy');
        $table->string('modifyBy');
        $table->timestamps();


       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slider');
    }
}
