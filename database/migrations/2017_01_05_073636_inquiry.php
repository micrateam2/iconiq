<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Inquiry extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
       Schema::create('inquiry', function (Blueprint $table) {

       $table->increments('id');
        $table->integer('clientId')->unsigned();
       $table->foreign('clientId')->references('id')->on('client')->onDelete('cascade');

       $table->string('clientEmail');
       $table->string('clientPhoneNumber');
       $table->string('createBy');
       $table->string('modifyBy');
       $table->timestamps();
   });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
        Schema::drop('inquiry');
   }
}