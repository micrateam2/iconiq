<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Category extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('category', function (Blueprint $table) {
       $table->increments('id');
       $table->string('categoryName');
       $table->string('categoryImage');

       $table->string('createBy');
       $table->string('modifyBy');
       $table->timestamps();
   });
  }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::drop('category');
   }
}