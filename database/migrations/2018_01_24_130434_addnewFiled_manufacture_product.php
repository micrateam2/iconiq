<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddnewFiledManufactureProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->foreign('manufactureId')->references('id')->on('manufacture')->onDelete('cascade')->onUpdate('cascade')->nullable();
            $table->integer('manufactureId')->unsigned();
            $table->string('isblocked')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product', function (Blueprint $table) {
            $table->dropColumn('manufactureId','isblocked');
        });
    }
}
