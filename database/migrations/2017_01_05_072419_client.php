<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Client extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('client', function (Blueprint $table) {
       $table->increments('id');
       $table->integer('userId')->unsigned();
       $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
       $table->string('firstName');
       $table->string('lastName');
       $table->string('address');
       $table->string('phoneNumber');
       $table->string('email')->nullable();
       $table->string('password')->nullable();
       $table->integer('countryID')->unsigned();
       $table->foreign('countryID')->references('id')->on('country')->onDelete('cascade');
       $table->integer('stateID')->unsigned();
       $table->foreign('stateID')->references('id')->on('states')->onDelete('cascade');
       $table->integer('cityID')->unsigned();
       $table->foreign('cityID')->references('id')->on('cities')->onDelete('cascade');
       $table->string('pinCode');
       $table->string('clientImage');
       $table->string('createBy');
       $table->string('modifyBy');
       $table->timestamps();
       
   });
  }
     /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::drop('client');
   }
}