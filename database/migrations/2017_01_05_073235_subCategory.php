<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubCategory extends Migration
{
   /**
    * Run the migrations.
    *
    * @return void
    */
   public function up()
   {
      Schema::create('subCategory', function (Blueprint $table) {
       $table->increments('id');
       $table->integer('categoryId')->unsigned();
           $table->foreign('categoryId')->references('id')->on('category')->onDelete('cascade');
      $table->string('subCategoryName');
       $table->string('subCategoryImage');
        $table->string('createBy');
       $table->string('modifyBy');
       $table->timestamps();
   });
   }

   /**
    * Reverse the migrations.
    *
    * @return void
    */
   public function down()
   {
       Schema::drop('subCategory');
   }
}