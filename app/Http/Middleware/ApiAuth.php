<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\Comman\Comman;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $user = Comman::checkApisSession($request->token);

            if(empty($user)){
                return response()->json(['message'=>'Token Expired','status'=>4],498);
            }

            $request['user'] = $user;

        } catch(exception $e){
            if(empty($user)){
                return response()->json(['message'=>'Token Expired','status'=>4],498);
            } else if(empty($request->token)){
                return response()->json(['message'=>'Token Required','status'=>4],499);
            }

        }

        return $next($request);
    }
}
