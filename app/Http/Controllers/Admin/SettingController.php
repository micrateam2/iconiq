<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Auth;
use DB;
use App\Settings;

class SettingController extends Controller
{

	public function index()
	{		
		$setting = Settings::orderBy('id', 'desc')
							->get();
		return view('Admin.setting.index',compact('setting'));
	}

	public function create()
	{
		return view('Admin.setting.create');

	}
	public function store(Request $request)
	{


		$rules = [
		'email' => 'required',
		'address'=>'required',
		'phoneNumber'=>'required',

		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return redirect()->back()
			->withErrors($validator)
			->withInput();
		}

		$data = $request->all();
		$data['createBy']= Auth::user()->id;
		$data['modifyBy']= Auth::user()->id;

		$setting = new Settings;
		$setting->fill($data);

		if ($setting->save()) {

			Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Setting Added Sucessfully.!! </div>'); 

			return redirect('admin/setting');
		} else {
			return redirect()->back()
			->withErrors(['email' => 'Something went wrong.',])
			->withInput();
		}

	}


	public function edit($id){
		$checkid= Settings::where('id','=',$id)->first();

		if($checkid != ""){

			$setting = Settings::where('id',$id)->first();

			return view('Admin.setting.edit',compact('setting'));
		}
		else{
			return view('errors.404Admin');
		}


	}

	public function update(Request $request,$id){

		$checkid= Settings::where('id','=',$id)->first();

		if($checkid != ""){


			$rules = [
			'email' => 'required',
			'address'=>'required',
			'phoneNumber'=>'required',

			];

			$validator = Validator::make($request->all(), $rules);

			if ($validator->fails()) {
				return redirect()->back()
				->withErrors($validator)
				->withInput();
			}

			$data['email']=$request->get('email');
			$data['phoneNumber']=$request->get('phoneNumber');
			$data['officeNumber']=$request->get('officeNumber');
			$data['address']=$request->get('address');
			$update_setting = DB::table('settings')->where('id', '=', $id)->update($data);

			Session::flash('message', '<div class="alert alert-info"><strong>Success!</strong> Your Settings Updated Sucessfully.!! </div>'); 

			return redirect('admin/setting');  
		}
		else{
			return view('errors.404Admin');
		}



	}

	/*Remove*/

/*public function remove($id)
{

  $checkid= Product::where('id','=',$id)->first();

  if($checkid != ""){
  	 $deleteproduct = DB::table('settings')->where('id', '=', $id)->delete();


  Session::flash('message', '<div class="alert alert-danger"><strong>Success!</strong> Your Setting Deleted Sucessfully.!! </div>'); 


  return redirect('admin/setting');
}
else{
  return view('errors.404Admin');
}
*/
}
