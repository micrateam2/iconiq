<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Inquiry;
use App\Client;
use DB;
class InquiryController extends Controller
{
   public function index()
    {
    	$inquiry = Inquiry::orderBy('id', 'desc')
                            ->get();
        return view('Admin.inquiry.index',compact('inquiry'));
      
    }

     public function remove($id){
	    $inquiryId=$id;		
		$deleteinquiry = DB::table('inquiry')->where('id', '=', $inquiryId)->delete();

        Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Record Delete Sucessfully.!! </div>');

		return redirect('admin/inquiry');
 

	}
}
