<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Auth;
use DB;
use App\Manufacture;
use URL;


class ManufactureController extends Controller
{
    public function index(){
        $data = Manufacture::orderBy('id','DESC')->get();
            return view('Admin.manufacture.index',compact('data'));
    }

    public function create(){
        return view('Admin.manufacture.create');
    }

    public function store(Request $request){

        $rules = [
            'name' => 'required|unique:manufacture',
            'phoneNumber' => 'digits:10',
//            'address' => 'required',
            'image' => 'required|mimes:jpeg,bmp,png,gif',
            'status' => 'required',

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data = $request->all();


        /*image Upload Code Start*/
        $image = $request->file('image');
        $extension = $image->getClientOriginalExtension();
        $destinationpath = public_path().'/upload/manufacture';
        $filename = 'manufacture' . '_' .rand (1,999). '.' . $extension;
        $move = $image->move($destinationpath, $filename);

        /* image upload code over*/

        $data = $request->all();
        $data['createBy']= Auth::user()->id;
        $data['modifyBy']= Auth::user()->id;

        $data['image'] =  'upload/manufacture/' . $filename;

        $manufacture = new Manufacture;
        $manufacture->fill($data);

        if ($manufacture->save()) {

            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Manufacture Added Sucessfully.!! </div>');

            return redirect('admin/manufacture');
        } else {
            return redirect()->back()
                ->withErrors(['name' => 'Something went wrong.',])
                ->withInput();
        }
    }

    public function edit($id){
        $data = Manufacture::findorFail($id);

        return view('admin.manufacture.edit',compact('data'));
    }

    public function update(Request $request,$id){
        $rules = [
            'name' => 'required|unique:manufacture,id,' . $id,
            'phoneNumber' => 'digits:10',
//            'address' => 'required',
            'image' => 'mimes:jpeg,bmp,png,gif',
            'status' => 'required',

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = $request->all();
        $data = $request->except('_token','_method','image');

        if($request->hasFile('image'))
        {
            /* Delete Image when we upload new Image code start*/
            $getimage = Manufacture::where('id',$id)->value('image');
            if($getimage !=""){
//                $explodeimage = explode('/', $getimage);
                $filename = public_path().'/upload/manufacture/'.$getimage;
                \File::delete($filename);
            }

            /*code over*/
            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension();
            $destinationpath = public_path().'/upload/manufacture';
            $filename = 'manufacture' . '_' . rand (1,999) . '.' . $extension;
            $move = $image->move($destinationpath, $filename);


            $data['image'] =  'upload/manufacture/' . $filename;

        }

        $newdata['status'] = $request->get('status');

        if($newStatus == 'Active'){
            $newdata['isblocked'] = 'False';
        } else {
            $newdata['isblocked'] = 'True';
        }

        $upproduct = Product::where('manufactureId',$id)->update($newdata);


        $update = Manufacture::where('id',$id)->update($data);

        Session::flash('message', '<div class="alert alert-info"><strong>Success!</strong> Your Manufacture Updated Sucessfully.!! </div>');

        return redirect('admin/manufacture');

    }

    public function destroy($id){

        $getimage = Manufacture::where('id', $id)->value('image');
        if ($getimage != "") {

            $filename = public_path() . '/upload/category/' . $getimage;
            \File::delete($filename);
        }


        $deleteManufacture = Manufacture::where('id',$id)->delete();

        Session::flash('message', '<div class="alert alert-danger"><strong>Success!</strong> Your Manufacture Deleted Sucessfully.!! </div>');


        return redirect('admin/manufacture');
    }

    public function changestatus(Request $request,$id){
            $data = Manufacture::findorFail($id);

            $newStatus = $data['status'] == 'Active' ? 'Deactive' : 'Active';

            $updata = Manufacture::where('id',$id)->update(['status'=>$newStatus]);

            $newdata['status'] = $newStatus;

            if($newStatus == 'Active'){
                $newdata['isblocked'] = 'False';
            } else {
                $newdata['isblocked'] = 'True';
            }

            $upproduct = Product::where('manufactureId',$id)->update($newdata);

        Session::flash('message', '<div class="alert alert-info"><strong>Success!</strong> Your Manufacture Status Updated Sucessfully.!! </div>');


        return Redirect::to('admin/manufacture');
    }
}
