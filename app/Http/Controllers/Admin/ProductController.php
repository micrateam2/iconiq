<?php

namespace App\Http\Controllers\Admin;

use App\Manufacture;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\SubCategoryController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Response;
use Auth;
use App\Category;
use App\SubCategory;
use DB;
use Excel;
use App\Product;
use App\ProductImage;

class ProductController extends Controller
{

    public function index()
    {

        $product = Product::orderBy('id', 'desc')
            ->get();
        foreach ($product as $row) {

            $row->productImage = ProductImage::where('productId', '=', $row->id)->groupBy()->value('productImageName');
        }

        return view('Admin.product.index', compact('product'));


    }


    public function create()
    {
        $category = ['' => 'Category Name'] + Category::pluck('categoryName', 'id')->all();
        $SubCategory = ['' => 'Sub Category Name'] + SubCategory::pluck('subCategoryName', 'id')->all();

        $manufacture = ['' => 'Manufacture Name'] + Manufacture::pluck('name','id')->all();

        return view('Admin.product.create', compact('category', 'SubCategory','manufacture'));
    }

    public function store(Request $request)
    {

        $rules = [
            'categoryId' => 'required',
            'subCategoryId' => 'required',
            'gender' => 'required',
            'productItemNumber' => 'required|unique:product',
            'goldQuality' => 'required',
            'goldWeight' => 'required',
            'manufactureId' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        // $data = $request->all();

        $data = $request->all();
        $data['subCategoryId'] = $request->get('subCategoryId');
        $data['manufactureId'] = $request->get('manufactureId');
        $data['diamondShape'] = $request->get('diamondShape') == '' ? 0 : $request->get('diamondShape');
        $data['diamondQualitySize'] = $request->get('diamondQualitySize') == '' ? 0 : $request->get('diamondQualitySize');
        $data['diamondPisces'] = $request->get('diamondPisces') == '' ? 0 : $request->get('diamondPisces');
        $data['diamondWeight'] = $request->get('diamondWeight') == '' ? 0 : $request->get('diamondWeight');
        $data['stoneShape'] = $request->get('stoneShape') == '' ? 0 : $request->get('stoneShape');
        $data['stoneQuality'] = $request->get('stoneQuality') == '' ? 0 : $request->get('stoneQuality');
        $data['stoneSize'] = $request->get('stoneSize') == '' ? 0 : $request->get('stoneSize');
        $data['stonePisces'] = $request->get('stonePisces') == '' ? 0 : $request->get('stonePisces');
        $data['stoneWeight'] = $request->get('stoneWeight') == '' ? 0 : $request->get('stoneWeight');


        $data['isActive'] = "true";
        $data['status'] = "Active";
        $data['featureProduct'] = 'true';
        $data['createBy'] = Auth::user()->id;
        $data['modifyBy'] = Auth::user()->id;

        $product = new Product;
        $product->fill($data);

        if ($product->save()) {


            /*image Upload Code Start*/
            $images = $request->file('productImageName');
            if (!empty($images)) {
                foreach ($images as $image) {

                    $extension = $image->getClientOriginalExtension();
                    $destinationpath = public_path() . '/upload/product';
                    $filename = 'product' . '_' . rand(1, 9999) . '.' . $extension;
                    $move = $image->move($destinationpath, $filename);

                    $productdata['productId'] = $product->id;
                    $productdata['productImageName'] = 'upload/product/' . $filename;
                    $productdata['status'] = "true";
                    $productdata['createBy'] = Auth::user()->id;
                    $productdata['modifyBy'] = Auth::user()->id;
                    $productimage = new ProductImage();
                    $productimage->fill($productdata);
                    $productimage->save();


                }
            }
            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Product Added Sucessfully.!! </div>');

            return redirect('admin/product');

        } else {
            return redirect()->back()
                ->withErrors(['productName' => 'Something went wrong.',])
                ->withInput();
        }

    }


    public function edit($id)
    {
        $checkid = Product::where('id', '=', $id)->first();

        if ($checkid != "") {

            $category = ['' => 'Category Name'] + Category::pluck('categoryName', 'id')->all();
            $subCategory = ['' => 'Sub Category Name'] + SubCategory::pluck('subCategoryName', 'id')->all();
            $manufacture = ['' => 'Manufacture Name'] + Manufacture::pluck('name','id')->all();
            $product = Product::where('id', $id)->first();

            $productImage = ProductImage::where('productId', $id)->get();

            return view('Admin.product.edit', compact('product', 'category', 'subCategory', 'productImage','manufacture'));

        } else {
            return view('errors.404Admin');
        }

    }

    // image remove start
    public function imageremove($id)
    {

        $checkid = ProductImage::where('id', '=', $id)->first();

        if ($checkid != "") {

            $getimage = ProductImage::where('id', $id)->value('productImageName');
            if ($getimage != "") {
                $explodeimage = explode('/', $getimage);
                $filename = public_path() . '/upload/product/' . $explodeimage[2];
                \File::delete($filename);

            }

            $deleteCategory = DB::table('productimage')->where('id', '=', $id)->delete();

            return redirect()->back();

        } else {
            return view('errors.404Admin');
        }

    }

    // image remove end

    public function update(Request $request, $id)
    {
        $checkid = Product::where('id', '=', $id)->first();

        if ($checkid != "") {

            $productId = $id;

            $rules = [
                'categoryId' => 'required',
                'subCategoryId' => 'required',
                'gender' => 'required',
                'goldQuality' => 'required',
                'goldWeight' => 'required',
                'manufactureId' => 'required',

            ];

            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)
                    ->withInput();
            }

            $productItemNumber = $request['productItemNumber'];
            $getProductItemNumber = Product::where('id', $productId)->where('productItemNumber', $productItemNumber)->value('productItemNumber');

            $flag1 = false;
            if ($productItemNumber == $getProductItemNumber) {
                $flag1 = true;

            } else {
                if ($productItemNumber != $getProductItemNumber) {
                    $checkProductItemNumber = Product::where('productItemNumber', $productItemNumber)->first();
                    $flag1 = $checkProductItemNumber == null ? true : false;
                    $checkProductItemNumber == null ? '' : $errors['productItemNumber'] = 'productItemNumber already exist.';
                }

            }
            if ($flag1) {
                $data = $request->all();
                $data = $request->except('_token', '_method', 'productImageName');

                $images = $request->file('productImageName');

                $img = count($images);

                if ($img != '0') {

                    foreach ($images as $image) {

                        $extension = $image->getClientOriginalExtension();
                        $destinationpath = public_path() . '/upload/product';
                        $filename = 'product' . '_' . rand(1, 9999) . '.' . $extension;
                        $move = $image->move($destinationpath, $filename);

                        $productdata['productId'] = $productId;
                        $productdata['productImageName'] = 'upload/product/' . $filename;

                        $productdata['status'] = "true";
                        $productdata['createBy'] = Auth::user()->id;
                        $productdata['modifyBy'] = Auth::user()->id;
                        $productimage = new ProductImage();
                        $productimage->fill($productdata);
                        $productimage->save();
                    }

                }
                $update_product = DB::table('product')->where('id', '=', $productId)->update($data);

                Session::flash('message', '<div class="alert alert-info"><strong>Success!</strong> Your Product Updated Sucessfully.!! </div>');

                return redirect('admin/product');


            } else {
                return redirect()->back()
                    ->withErrors($errors)
                    ->withInput();
            }

        } else {
            return view('errors.404Admin');
        }

    }


    public function remove($id)
    {

        $checkid = Product::where('id', '=', $id)->first();

        if ($checkid != "") {

            $productId = $id;

            $getimage = ProductImage::where('productId', $productId)->get();
            foreach ($getimage as $row) {
                if ($row->productImageId != "") {
                    $explodeimage = explode('/', $row->productImageName);
                    $filename = public_path() . '/upload/product/' . $explodeimage[2];
                    \File::delete($filename);
                    $deleteImages = DB::table('productimage')->where('productImageId', '=', $row->productImageId)->delete();

                }
            }
            $deleteproduct = DB::table('product')->where('id', '=', $id)->delete();


            Session::flash('message', '<div class="alert alert-danger"><strong>Success!</strong> Your Category Deleted Sucessfully.!! </div>');


            return redirect('admin/product');
        } else {
            return view('errors.404Admin');
        }

    }


    public function getdetail(Request $request)
    {
        $id = $request->get('id');
        $getid = Product::where('id', '=', $id)->first();

        return ['value' => $getid, 'status' => true];
    }

    public function importProduct()
    {

        if (Input::hasFile('bulkproduct')) {

            $userid = Auth::user()->id;

            $path = Input::file('bulkproduct')->getRealPath();
            $data = Excel::load($path, function ($reader) {

            })->get();
            $wrongdata = array();
            // dd($data);
            foreach ($data as $key => $value) {

                $category = $value->category;
                $categoryId = Category::where('categoryName', '=', $category)->value('id');
                $subCategory = $value->subcategory;
                $subCategoryId = SubCategory::where('subCategoryName', '=', $subCategory)->value('id');
                $manufacture = $value->manufacture;
                $manufactureId = Manufacture::where('name', '=', $manufacture)->value('id');
                $gender = $value->gender;
                $productItemNumber = $value->productitemnumber;
                $goldQuality = $value->goldquality;
                $goldWeight = $value->goldweight;
                $diamondShape = $value->diamondshape;
                $diamondQualitySize = $value->diamondqualitysize;
                $diamondPisces = $value->diamondpisces;
                $diamondWeight = $value->diamondweight;


                if ($category != "") {
                    $flag1 = true;
                } else {

                    $category = $category . "(category Name Required)";
                    $flag1 = false;
                }

                if ($subCategory != "") {
                    $flag2 = true;
                } else {

                    $subCategory = $subCategory . "(subcategory Name Required)";
                    $flag2 = false;
                }

                if ($manufacture != "") {
                    $flag10 = true;
                } else {

                    $manufacture = $manufacture . "(Manufacture Name Required)";
                    $flag10 = false;
                }

                if ($gender != "") {
                    $flag3 = true;
                } else {

                    $gender = $gender . "(gender Required)";
                    $flag3 = false;
                }

                if ($productItemNumber != "") {
                    $flag4 = true;
                } else {

                    $productItemNumber = $productItemNumber . "(product Item Number Required)";
                    $flag4 = false;
                }

                if ($goldQuality != "") {
                    $flag5 = true;
                } else {

                    $goldQuality = $goldQuality . "(Gold Quality Required)";
                    $flag5 = false;
                }

                if ($goldWeight != "") {
                    $flag6 = true;
                } else {

                    $goldWeight = $goldWeight . "(Gold Weight Required)";
                    $flag6 = false;
                }

                if ($categoryId != "") {
                    $flag7 = true;
                } else {
                    $category = $category . "(Category not found)";
                    $flag7 = false;
                }

                if ($subCategoryId != "") {
                    $flag8 = true;
                } else {
                    $subCategory = $subCategory . "(Subcategory not found)";
                    $flag8 = false;
                }

                if ($diamondShape != "") {
                    $flag9 = true;
                } else {
                    $diamondShape = $diamondShape . "(diamond Shape not found)";
                    $flag9 = false;
                }

                if ($manufactureId != "") {
                    $flag11 = true;
                } else {
                    $manufacture = $manufactureId . "(Manufacture not found)";
                    $flag11 = false;
                }

//                 if($diamondQualitySize != "")
//                {
//                    $flag10 = true;
//                }
//                else
//                {
//                    $diamondQualitySize = $diamondQualitySize . "(Diamond Size not found)";
//                    $flag10 = false;
//                }
//
//                 if($diamondPisces != "")
//                {
//                    $flag11 = true;
//                }
//                else
//                {
//                    $diamondPisces = $diamondPisces . "(Diamond Pisces not found)";
//                    $flag11 = false;
//                }

                if ($diamondWeight != "") {
                    $flag12 = true;
                } else {
                    $diamondWeight = $diamondWeight . "(Diamond Weight not found)";
                    $flag12 = false;
                }

                $insert = [
                    'categoryId' => $categoryId,
                    'subCategoryId' => $subCategoryId,
                    'gender' => $gender,
                    'productItemNumber' => $productItemNumber,
                    'goldQuality' => $goldQuality,
                    'goldWeight' => $goldWeight,
                    'diamondShape' => $diamondShape,
                    'diamondQualitySize' => $diamondQualitySize,
                    'diamondPisces' => $diamondPisces,
                    'diamondWeight' => $diamondWeight,
                    'manufactureId' =>$manufactureId,
                    'isblocked' =>'False',

                ];

                if ($flag1 && $flag2 && $flag3 && $flag4 && $flag5 && $flag6 && $flag7 && $flag8 && $flag9 && $flag10 && $flag11 && $flag12) {
                    $insertdata[] = array(
                        'categoryId' => $categoryId,
                        'subCategoryId' => $subCategoryId,
                        'gender' => $gender,
                        'productItemNumber' => $productItemNumber,
                        'goldQuality' => $goldQuality,
                        'goldWeight' => $goldWeight,
                        'diamondShape' => $diamondShape,
                        'diamondQualitySize' => $diamondQualitySize,
                        'diamondPisces' => $diamondPisces,
                        'diamondWeight' => $diamondWeight,
                        'manufactureId' =>$manufactureId,
                        'isblocked' =>'False',
                        'createBy' => Auth::user()->id,
                        'modifyBy' => Auth::user()->id,

                    );
                } else {
                    $wrongdata[] = array(

                        'categoryId' => $categoryId,
                        'subCategoryId' => $subCategoryId,
                        'manufactureId' =>$manufactureId,
                        'gender' => $gender,
                        'productItemNumber' => $productItemNumber,
                        'goldQuality' => $goldQuality,
                        'goldWeight' => $goldWeight,
                        'category' => $category,
                        'subCategory' => $subCategory,
                        'diamondShape' => $diamondShape,
                        'diamondQualitySize' => $diamondQualitySize,
                        'diamondPisces' => $diamondPisces,
                        'diamondWeight' => $diamondWeight,
                        'manufacture' => $manufacture,

                    );
                }

            }
            // print_r(count($wrongdata));
            $status = true;
            if (count($wrongdata) == 0) {
                foreach ($insertdata as $row) {
                    $property = Product::create($row);
                }

                return View('Admin.product.uploaderror')->with('wrongdata', $wrongdata)->with('status', $status);
            } else {
                return View('Admin.product.uploaderror')->with('wrongdata', $wrongdata)->with('status', $status);
            }
        }

    }

    public function sampledownload()
    {
        $file = "public/upload/bulkproductupload.xlsx";
        return Response::download($file);
        return redirect()->back();
    }

    public function changestatus(Request $request,$id){

        $data = Product::findorFail($id);

        $newStatus = $data['status'] == 'Active' ? 'Deactive' : 'Active';

        if($data['isblocked'] == 'False'){
            $updata = Product::where('id',$id)->update(['status'=>$newStatus]);

            Session::flash('message', '<div class="alert alert-info"><strong>Success! </strong> Your Product Status Updated Sucessfully.!! </div>');

        } else {
            Session::flash('message', '<div class="alert alert-danger"><strong>Alert! </strong>  This Product Manufacture has not Aceess Change Status.!! </div>');

        }


        return Redirect::to('admin/product');
    }


}
