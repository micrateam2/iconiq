<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\InquiryProduct;
use App\Product;
use App\ProductImage;
use App\Client;
use App\Inquiry;
use App\Settings;

class InquiryProductController extends Controller
{
	public function index($id)
	{
		$product_inquiry =Inquiryproduct::where('inquiryId','=',$id)
										->orderBy('id', 'desc')
										->get();
		foreach ($product_inquiry as $row) {

			$row->productImage = ProductImage::where('productId','=',$row->productId)->groupBy()->value('productImageName');
		}

         $client_inquiry=Inquiry::where('id','=',$id)->first(); 
        $contact = Settings::first();
		return view('Admin.inquiryProduct.index',compact('product_inquiry','client_inquiry','contact'));

	}
	

}