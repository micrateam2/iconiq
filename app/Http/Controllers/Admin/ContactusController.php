<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Auth;
use DB;
use App\Contactus;


class ContactusController extends Controller
{
	public function index(){
		$contactus = Contactus::orderBy('id', 'desc')
								->get();
		return view('Admin.Contactus.index',compact('contactus'));  	 
	}

	public function remove($id){

		$checkid= Contactus::where('id','=',$id)->first();

		if($checkid != ""){
			

			$deleteContactUs = DB::table('contactus')->where('id', '=', $id)->delete();
			
			Session::flash('message', '<div class="alert alert-danger"><strong>Success!</strong> Your ContactUs Deleted Sucessfully.!! </div>'); 

			
			return redirect('admin/contactus');

		}
		else{
			return view('errors.404Admin');
		}

	}
	public function getdetail(Request $request){
		$id=$request->get('id');		
		$getid= Contactus::where('id','=',$id)->first();
		return ['value'=>$getid,'status'=>true]; 
	}
}