<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Auth;
use DB;
use App\Slider;
class SliderController extends Controller
{
 public function index(){
    $slider = Slider::orderBy('id', 'desc')
                      ->get();
    return view('Admin.slider.index',compact('slider'));
  }

  public function create(){
   return view('Admin.slider.create');
 }

 public function store(Request $request){

  $rules = [
  'sliderName' => 'required|unique:slider',
  'sliderImage'=>'required|unique:slider',
  
  ];

  $validator = Validator::make($request->all(), $rules);

  if ($validator->fails()) {
   return redirect()->back()
   ->withErrors($validator)
   ->withInput();
 }
 $data = $request->all();


 /*image Upload Code Start*/
 $image = $request->file('sliderImage');
 $extension = $image->getClientOriginalExtension();
 $destinationpath = public_path().'/upload/slider';
 $filename = 'slider' . '_' .rand (1,999). '.' . $extension;
 $move = $image->move($destinationpath, $filename);

 /* image upload code over*/

 $data = $request->all();
 $data['createBy']= Auth::user()->id;
 $data['modifyBy']= Auth::user()->id;

 $data['sliderImage'] =  'upload/slider/' . $filename;

 $slider = new slider;
 $slider->fill($data);

 if ($slider->save()) {
      
    Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Slider Added Sucessfully.!! </div>'); 

   return redirect('admin/slider');
 } else {
   return redirect()->back()
   ->withErrors(['sliderName' => 'Something went wrong.',])
   ->withInput();
 }
}

public function edit($id){
  $checkid= Slider::where('id','=',$id)->first();

  if($checkid != ""){

  $sliderId=$id;
  $slider = Slider::where('id',$sliderId)->first();

  return view('Admin.slider.edit',compact('slider'));

}
else{
  return view('errors.404Admin');
}

}

public function update(Request $request,$id){

$checkid= Slider::where('id','=',$id)->first();

  if($checkid != ""){
    
 $sliderId=$id;
 $rules = [
 'sliderName' => 'required',
 'sliderImage'=>'required',

 ];
 $validator = Validator::make($request->all(),$rules);

 if($validator->fails()){
   return redirect()->back()
   ->withErrors($validator)
   ->withInput();

 }


  $sliderName = $request['sliderName'];
         $getSliderName = Slider::where('id',$sliderId)->where('sliderName',$sliderName)->value('sliderName');
    
        $flag1 = false;
       if($sliderName == $getSliderName)
       {
           $flag1 = true;
 
       }
       else
       {
           if($sliderName != $getSliderName)
           {
               $checkSliderName = Slider::where('sliderName',$sliderName)->first();
               $flag1 = $checkSliderName == null ? true : false;
               $checkSliderName == null ? '': $errors['sliderName'] = 'Slider Name already exist.';
           }
               
       }
        if($flag1)
       {

       $data['sliderName'] = $request->get('sliderName');
       $request->except('sliderImage');
       if($request->hasFile('sliderImage'))
       {


        /* Delete Image when we upload new Image code start*/
        $getimage = Slider::where('id',$sliderId)->value('sliderImage');
         if($getimage !=""){
        $explodeimage = explode('/', $getimage);
        $filename = public_path().'/upload/slider/'.$explodeimage[2];
        \File::delete($filename); 
          }

        /*code over*/
        $image = $request->file('sliderImage');
        $extension = $image->getClientOriginalExtension();
        $destinationpath = public_path().'/upload/slider';
        $filename = 'slider' . '_' . rand (1,999) . '.' . $extension;
        $move = $image->move($destinationpath, $filename);


        $data['sliderImage'] =  'upload/slider/' . $filename;
        
      }


      $update_projectname = DB::table('slider')->where('id', '=', $sliderId)->update($data);

       
          Session::flash('message', '<div class="alert alert-info"><strong>Success!</strong> Your Slider Updated Sucessfully.!! </div>'); 

      return redirect('admin/slider');  

      }
      else
       {
                  return redirect()->back()
                   ->withErrors($errors)
                   ->withInput();
       }
}
else{
  return view('errors.404Admin');
}

}

public function remove($id){
 $sliderId=$id;
 $getimage = Slider::where('id',$sliderId)->value('sliderImage');
 if($getimage !=""){
 $explodeimage = explode('/', $getimage);
 $filename = public_path().'/upload/slider/'.$explodeimage[2];
 \File::delete($filename); 
}
 
 
 $deleteSlider = DB::table('slider')->where('id', '=', $sliderId)->delete();
 
 Session::flash('message', '<div class="alert alert-danger"><strong>Success!</strong> Your Slider Deleted Sucessfully.!! </div>'); 

 
 return redirect('admin/slider');

}

public function block($id){
  
  $getstatus=Slider::where('id',$id)->value('sliderStatus');
  if($getstatus=='true'){
    $upstatus="false"; 
  }elseif($getstatus=='false'){
    $upstatus="true";
  }

  $data['sliderStatus']=$upstatus;

   $update_projectname = DB::table('slider')->where('id', '=', $id)->update($data);

       
          Session::flash('message', '<div class="alert alert-info"><strong>Success!</strong> Your Slider Updated Sucessfully.!! </div>'); 

      return redirect('admin/slider');  
}
  
}
