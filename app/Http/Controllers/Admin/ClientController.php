<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Country;
use App\Client;
use App\State;
use App\City;
use Auth;
use DB;
use App\User;
use Hash;
use Mail;
use App\Role;

class ClientController extends Controller
{
	public function index(){


		$client = Client::orderBy('id', 'desc')
						->get();

		return view('Admin.client.index',compact('client'));


	}

	public function create(){



		$country = ['' => 'Select country'] + Country::pluck('countryName','id')->all();


		$states = ['' => 'Select state'] + State::pluck('stateName','id')->all();


		$city=[''=>'Select city'] + City::pluck('cityName','id')->all();


		return view('Admin.client.create',compact('states','country','city'));

	}

	public function store(Request $request){

		$rules = [
		'firstName' => 'required',
		'lastName' => 'required',
		'address' => 'required',
		'phoneNumber' => 'required',
		'email' => 'required|unique:users',
		'password' => 'required',		
		'clientImage' => 'required',
		'countryID'=>'required',
		'stateID'=>'required',
		'cityID'=>'required'

		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return redirect()->back()
			->withErrors($validator)
			->withInput();
		}
		
		$user = new User();
		$data['first_name']=$request->get('firstName');
		$data['last_name']=$request->get('lastName');
		$data['email'] = $request->get('email');
		$data['password'] = Hash::make($request->get('password'));
		$user->fill($data);
		
		if ($user->save()){
			$clientrole=Role::where('name','client')->first();
			$user->attachRole($clientrole);

		}


		$data = $request->all();

		$image = $request->file('clientImage');
		$extension = $image->getClientOriginalExtension();
		$destinationpath = public_path().'/upload/client';
		$filename = 'client' . '_' . rand (1,999) . '.' . $extension;
		$move = $image->move($destinationpath, $filename);

		$data['clientImage'] = 'upload/client/' . $filename;

		$data['createBy']= Auth::user()->id;
		$data['modifyBy']= Auth::user()->id;

		$data['clientImage'] =  'upload/client/' . $filename;

		$data['password']= Hash::make($request->get('password'));
		$data['userId'] = $user->id;

		$client = new client;
		$client->fill($data);

		

		if ($client->save()) {

			$title = "Welcome to ICONIQ Login Details ";
			$clientName = $request->get('firstName'). '' .$request->get('lastName');
			$address = $request->get('address');
			$phoneNumber = $request->get('phoneNumber');                
			$email = $request->get('email');
			$password = $request->get('password');
			$data = array('title' => $title,'clientName'=>$clientName,
				'address'=>$address,'phoneNumber'=>$phoneNumber,'email' => $email,'password' => $password);

			Mail::send('Admin.email.createclient', $data, function($message) use ($data) {
				$message->from('info@iconiqjewellery.com')->subject($data['title']);
				$message->to($data['email']);
			});
			
			Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Record Added Sucessfully.!! </div>');
			
			return redirect('admin/client');
		} else {
			return redirect()->back()
			->withErrors(['clientName' => 'Something went wrong.',])
			->withInput();

		}
		
	}

	public function edit($id){   

		$checkid= Client::where('id','=',$id)->first();

		if($checkid != ""){	


			$country = ['' => 'Select country'] + Country::pluck('countryName','id')->all();

$state = ['' => 'Select state'] + State::pluck('stateName','id')->all();



			$city=[''=>'Select city'] + City::pluck('cityName','id')->all();

			$clientId=$id;	
			$client = Client::where('id',$clientId)->first();
			return view('Admin.client.edit',compact('country','state','city','client'));

			

		}
		else{
			return view('errors.404Admin');
		}

	}

	public function update(Request $request,$id){

		$checkid= Client::where('id','=',$id)->first();

		if($checkid != ""){	

			$clientId=$id;	

			$rules = [
			'firstName' => 'required',
			'lastName' => 'required',
			'address' => 'required',
			'phoneNumber' => 'required',
			'email' => 'required',
			

			];



			$validator = Validator::make($request->all(),$rules);

			if($validator->fails())
			{
				return redirect()->back()->withErrors($validator);

			}

			$email = $request['email'];
			$getemail = client::where('id',$clientId)->where('email',$email)->value('email');

			$flag1 = false;
			if($email == $getemail)
			{
				$flag1 = true;

			}
			else
			{
				if($email != $getemail)
				{
					$checkemail = client::where('email',$email)->first();
					$flag1 = $checkemail == null ? true : false;
					$checkemail == null ? '': $errors['email'] = 'email already exist.';
				}

			}

			if($flag1)
			{
				$data=$request->all();
				$data = $request->except('_token','_method');
				if($request->hasFile('clientImage'))
				{
					$image = $request->file('clientImage');
					$extension = $image->getClientOriginalExtension();
					$destinationpath = public_path().'/upload/client';
					$filename = 'client' . '_' . rand (1,999) . '.' . $extension;
					$move = $image->move($destinationpath, $filename);
					$data['clientImage'] =  'upload/client/' . $filename;
				}

				$updateemail = Client::where('id', '=', $clientId)->update($data);

				if($request['password'] != "")
				{
					Auth::logout();
					return redirect('admin/login');
				}
				Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Record Updated Sucessfully.!! </div>');

				return redirect('admin/client');

			}
			else
			{
				return redirect()->back()
				->withErrors($errors)
				->withInput();
			}



			$email = $request['email'];
			$getemail = User::where('id',$request->get('userid'))->where('email',$email)->value('email');

			$flag1 = false;
			if($email == $getemail)
			{
				$flag1 = true;

			}
			else
			{
				if($email != $getemail)
				{
					$checkemail = User::where('email',$email)->first();
					$flag1 = $checkemail == null ? true : false;
					$checkemail == null ? '': $errors['email'] = 'email already exist.';
				}

			}

			if($flag1)
			{
				$data=$request->all();

				$data = $request->except('_token','_method');
				if($request->hasFile('clientImage'))
				{
					$image = $request->file('clientImage');
					$extension = $image->getClientOriginalExtension();
					$destinationpath = public_path().'/upload/client';
					$filename = 'client' . '_' . rand (1,999) . '.' . $extension;
					$move = $image->move($destinationpath, $filename);
					$data['clientImage'] =  'upload/client/' . $filename;
				}

				$updateemail = User::where('id',$request->get('userid'))->update(['email' => $request->get('email'),'first_name'=>$request->get('firstName'),'last_name'=>$request->get('lastName')]);
				$updateclient = Client::where('id', '=', $clientId)->update($data);

				if($request['password'] != "")
				{
					Auth::logout();
					return redirect('admin/login');
				}
				Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Record Updated Sucessfully.!! </div>');

				return redirect('admin/client');

			}
			else{
				return view('errors.404Admin');
			}




		}
	}
	public function changepassword(Request $request){


		$clientid = $request->get('clientid');
		print_r($clientid);
		$userid = Client::where('id',$clientid)->value('userId');


		$rules = [
		'password' => 'required',
		'confirm_password' => 'required'	

		];
		$validator = Validator::make($request->all(),$rules);

		if($validator->fails())
		{
			return redirect()->back()->withErrors($validator);

		}

		$data['password'] = Hash::make($request->get('password'));

		$update_password = User::where('id', '=', $userid)->update($data);

		Session::flash('message', '<div class="alert alert-info"><strong>Success!</strong> Your PassWord Updated Sucessfully.!! </div>');

		return redirect('admin/client');




	}

	public function remove($id){

		$checkid= Client::where('id','=',$id)->first();

		if($checkid != ""){	

			$clientId=$id;
			$getimage = Client::where('id',$clientId)->value('clientImage');
			$explodeimage = explode('/', $getimage);
			$filename = public_path().'/upload/client/'.$explodeimage[2];
			\File::delete($filename); 


			$deleteclient = DB::table('client')->where('id', '=', $clientId)->delete();

			Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Record Deleted Sucessfully.!! </div>');

			return redirect('admin/client');


		}

		else{
			return view('errors.404Admin');
		}

	}

	public function block($id){
		$getclient=Client::where('id',$id)->value('clientStatus');
		if($getclient=='true'){
			$upstatus="false";
		}elseif ($getclient=='false') {
              $upstatus="true";			
		}
  $data['clientStatus']=$upstatus;		
  $up_clientstatus=DB::table('client')->where('id','=',$id)->update($data);
  if($upstatus=='true'){
  Session::flash('message', '<div class="alert alert-info"><strong>Success!</strong> Client UnBlock  Sucessfully.!! </div>');
} else {
	Session::flash('message', '<div class="alert alert-info"><strong>Success!</strong> Client Block  Sucessfully.!! </div>');
}
return redirect('admin/client');
	}
}
