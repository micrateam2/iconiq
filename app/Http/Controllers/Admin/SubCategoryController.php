<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\CategoryController;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Category;
use App\SubCategory;
use Auth;
use DB;

class SubCategoryController extends Controller
{
 public function index(){


    $subcategory = SubCategory::orderBy('id', 'desc')
                                ->get();
    
  return view('Admin.subcategory.index',compact('subcategory'));


}

public function create(){

  $category = ['' => 'Select category'] + Category::pluck('categoryName','id')->all();


  return view('Admin.subcategory.create',compact('category'));
}

public function store(Request $request){

  $rules = [
  'categoryId' => 'required',
  'subCategoryName' => 'required|unique:subcategory',
  'subCategoryImage' => 'required'
  ];
  
  $validator = Validator::make($request->all(), $rules);

  if ($validator->fails()) {
   return redirect()->back()
   ->withErrors($validator)
   ->withInput();
 }
 $data = $request->all();

 $image = $request->file('subCategoryImage');
 $extension = $image->getClientOriginalExtension();
 $destinationpath = public_path().'/upload/subcategory';
 $filename = 'subcategory' . '_' .rand (1,999) . '.' . $extension;
 $move = $image->move($destinationpath, $filename);

 $data = $request->all();
 $data['createBy']= Auth::user()->id;
 $data['modifyBy']= Auth::user()->id;

 $data['subCategoryImage'] =  'upload/subcategory/' . $filename;

 $subcategory = new SubCategory;
 $subcategory->fill($data);

 if ($subcategory->save()) {
      
 Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Sub Category Added Sucessfully.!! </div>'); 

   return redirect('admin/subcategory');
 } else {
   return redirect()->back()
   ->withErrors(['subCategoryName' => 'Something went wrong.',])
   ->withInput();
 }
}

public function edit($id){

   $checkid= SubCategory::where('id','=',$id)->first();

  if($checkid != ""){


  $category = ['' => 'Select category'] + Category::pluck('categoryName','id')->all();

  $subcategoryId=$id;
  $subcategory = SubCategory::where('id',$subcategoryId)->first();

  return view('Admin.subcategory.edit',compact('subcategory','category'));
}
else{
  return view('errors.404Admin');
}



}

public function update(Request $request,$id){

$checkid= SubCategory::where('id','=',$id)->first();

  if($checkid != ""){

 $subCategoryId=$id;
 $rules = [
 'categoryId'=> 'required',
 'subCategoryName'=> 'required',

 ];
 $validator = Validator::make($request->all(),$rules);

 if($validator->fails()){
   return redirect()->back()
   ->withErrors($validator)
    ->withInput();
 }



  $subCategoryName = $request['subCategoryName'];
         $getSubCategoryName = SubCategory::where('id',$subCategoryId)->where('subCategoryName',$subCategoryName)->value('subCategoryName');
    
        $flag1 = false;
       if($subCategoryName == $getSubCategoryName)
       {
           $flag1 = true;
 
       }
       else
       {
           if($subCategoryName != $getSubCategoryName)
           {
               $checkSubCategoryName = SubCategory::where('subCategoryName',$subCategoryName)->first();
               $flag1 = $checkSubCategoryName == null ? true : false;
               $checkSubCategoryName == null ? '': $errors['subCategoryName'] = 'Sub-Category Name already exist.';
           }
               
       }
        if($flag1)
       {

             $data['categoryId'] = $request->get('categoryId');
             $data['subCategoryName'] = $request->get('subCategoryName');

             $request->except('subCategoryImage');
             
             if($request->hasFile('subCategoryImage'))
             {
               /* Delete Image when we upload new Image code start*/
               

              /* Delete Image when we upload new Image code start*/
              $getimage = SubCategory::where('id',$subCategoryId)->value('subCategoryImage');
               if($getimage !=""){
              $explodeimage = explode('/', $getimage);
              $filename = public_path().'/upload/subcategory/'.$explodeimage[2];
              \File::delete($filename); 
               
                }

              /*code over*/
              $image = $request->file('subCategoryImage');
              $extension = $image->getClientOriginalExtension();
              $destinationpath = public_path().'/upload/subcategory';
              $filename = 'category' . '_' . rand (1,999) . '.' . $extension;
              $move = $image->move($destinationpath, $filename);


              $data['subCategoryImage'] =  'upload/subcategory/' . $filename;
              
             }


             $update_projectname = DB::table('subcategory')->where('id', '=', $subCategoryId)->update($data);

             Session::flash('message', '<div class="alert alert-info"><strong>Success!</strong> Your Sub Category Updated Sucessfully.!! </div>'); 

             return redirect('admin/subcategory');  
           }
           else
       {
                  return redirect()->back()
                   ->withErrors($errors)
                   ->withInput();
       }
}

else{
  return view('errors.404Admin');
}


}

public function remove($id){
  $checkid= SubCategory::where('id','=',$id)->first();

  if($checkid != ""){

  $subCategoryId=$id;
  $getimage = SubCategory::where('id',$subCategoryId)->value('subCategoryImage');
   if($getimage !=""){
  $explodeimage = explode('/', $getimage);
  $filename = public_path().'/upload/subcategory/'.$explodeimage[2];
  \File::delete($filename); 
  
}
  $deleteSubCategory = DB::table('subcategory')->where('id', '=', $subCategoryId)->delete();
  
   Session::flash('message', '<div class="alert alert-danger"><strong>Success!</strong> Your Sub Category Deleted Sucessfully.!! </div>'); 


  return redirect('admin/subcategory');


}
else{
  return view('errors.404Admin');
}



}
public function show($subCategoryId){
  return view('');

}
}
