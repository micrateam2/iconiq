<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Auth;
use DB;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $category = Category::orderBy('id', 'desc')
            ->get();
        return view('Admin.category.index', compact('category'));
    }

    public function create()
    {
        return view('Admin.category.create');
    }

    public function store(Request $request)
    {

        $rules = [
            'categoryName' => 'required|unique:category',

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $data = $request->all();


        /*image Upload Code Start*/
        $image = $request->file('categoryImage');
        $extension = $image->getClientOriginalExtension();
        $destinationpath = public_path() . '/upload/category';
        $filename = 'category' . '_' . rand(1, 999) . '.' . $extension;
        $move = $image->move($destinationpath, $filename);

        /* image upload code over*/

        $data = $request->all();
        $data['createBy'] = Auth::user()->id;
        $data['modifyBy'] = Auth::user()->id;

        $data['categoryImage'] = 'upload/category/' . $filename;

        $category = new category;
        $category->fill($data);

        if ($category->save()) {

            Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Your Category Added Sucessfully.!! </div>');

            return redirect('admin/category');
        } else {
            return redirect()->back()
                ->withErrors(['categoryName' => 'Something went wrong.',])
                ->withInput();
        }
    }

    public function edit($id)
    {

        $checkid = Category::where('id', '=', $id)->first();

        if ($checkid != "") {


            $categoryId = $id;
            $category = Category::where('id', $categoryId)->first();

            return view('Admin.category.edit', compact('category'));

        } else {
            return view('errors.404Admin');
        }

    }

    public function update(Request $request, $id)
    {

        $checkid = Category::where('id', '=', $id)->first();

        if ($checkid != "") {

            $categoryId = $id;
            $rules = [
                'categoryName' => 'required',

            ];
            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();

            }


            $categoryName = $request['categoryName'];
            $getCategoryName = Category::where('id', $categoryId)->where('categoryName', $categoryName)->value('categoryName');

            $flag1 = false;
            if ($categoryName == $getCategoryName) {
                $flag1 = true;

            } else {
                if ($categoryName != $getCategoryName) {
                    $checkCategoryName = Category::where('categoryName', $categoryName)->first();
                    $flag1 = $checkCategoryName == null ? true : false;
                    $checkCategoryName == null ? '' : $errors['categoryName'] = 'Category Name already exist.';
                }

            }
            if ($flag1) {


                $data['categoryName'] = $request->get('categoryName');
                $request->except('categoryImage');
                if ($request->hasFile('categoryImage')) {


                    /* Delete Image when we upload new Image code start*/
                    $getimage = Category::where('id', $categoryId)->value('categoryImage');
                    if ($getimage != "") {
                        $explodeimage = explode('/', $getimage);
                        $filename = public_path() . '/upload/category/' . $explodeimage[2];
                        \File::delete($filename);
                    }

                    /*code over*/
                    $image = $request->file('categoryImage');
                    $extension = $image->getClientOriginalExtension();
                    $destinationpath = public_path() . '/upload/category';
                    $filename = 'category' . '_' . rand(1, 999) . '.' . $extension;
                    $move = $image->move($destinationpath, $filename);


                    $data['categoryImage'] = 'upload/category/' . $filename;

                }


                $update_projectname = DB::table('category')->where('id', '=', $categoryId)->update($data);


                Session::flash('message', '<div class="alert alert-info"><strong>Success!</strong> Your Category Updated Sucessfully.!! </div>');

                return redirect('admin/category');

            } else {
                return redirect()->back()
                    ->withErrors($errors)
                    ->withInput();
            }
        } else {
            return view('errors.404Admin');
        }


    }

    public function remove($id)
    {
        $categoryId = $id;
        $getimage = Category::where('id', $categoryId)->value('categoryImage');
        if ($getimage != "") {
            $explodeimage = explode('/', $getimage);
            $filename = public_path() . '/upload/category/' . $explodeimage[2];
            \File::delete($filename);
        }


        $deleteCategory = DB::table('category')->where('id', '=', $categoryId)->delete();

        Session::flash('message', '<div class="alert alert-danger"><strong>Success!</strong> Your Category Deleted Sucessfully.!! </div>');


        return redirect('admin/category');

    }


}
