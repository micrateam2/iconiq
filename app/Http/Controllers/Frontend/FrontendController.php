<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\SubCategoryController;
use App\Http\Controllers\Admin\SettingController;
use Illuminate\Support\Facades\Input;
use Auth;
use DB;
use Mail;
use App\Category;
use App\SubCategory;
use App\Product;
use App\ProductImage;
use App\Contactus;
use App\Slider;
use App\Settings;


class FrontendController extends Controller
{
    public function index()
    {

        $subcategory = SubCategory::join('product', 'product.subcategoryId', '=', 'subcategory.id')->select('subcategory.*')->distinct()->get();

//    $fetureproduct = SubCategory::inRandomOrder()->get()->take(3);

//      $product = Product::orderBy('id', 'desc')
//    ->distinct('subCategoryId')
//    ->limit(100)
//    ->get();
//    foreach ($product as $row) {
//
//      $row->productImage = ProductImage::where('productId','=',$row->id)->groupBy()->value('productImageName');
//    }

        $slider = Slider::orderBy('id', 'desc')
            ->where('sliderStatus', '=', 'true')
            ->limit(5)
            ->get();

        return view('frontend.index', compact('subcategory', 'slider'));


    }

    public function collection($id)
    {

        $checkid = Product::where('gender', '=', $id)->where('status', 'Active')->first();

        if ($checkid != "") {


            $product = Product::where('gender', '=', $id)
                ->paginate(10);

            foreach ($product as $row) {

                $row->productImage = ProductImage::where('productId', '=', $row->id)->groupBy()->value('productImageName');
            }

            $subcategory = SubCategory::get();


            return view('frontend.product.products', compact('product', 'subcategory', 'id'));
        } else {
            return view('errors.404');
        }


    }

    public function contactus()
    {

        $contact = Settings::first();
        return view('frontend.contactus', compact('contact'));
    }

    public function store(Request $request)
    {

        $rules = [
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email',
            'phoneNumber' => 'required',
            'subject' => 'required',
            'message' => 'required',

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = $request->all();
        if (!empty(Auth::user()->id)) {
            $data['createBy'] = Auth::user()->id;
            $data['modifyBy'] = Auth::user()->id;
        } else {
            $data['createBy'] = 0;
            $data['modifyBy'] = 0;
        }

        $contactus = new Contactus;

        $contactus->fill($data);


        if ($contactus->save()) {

            $title = "Thanks for Being Our Awesome Customer! ";
            $title1 = "Hello.!! Admin You Get A New Conatct Inquiry";
            $name = $request->get('firstName') . '' . $request->get('lastName');
            $email = $request->get('email');
            $phoneNumber = $request->get('phoneNumber');
            $subject = $request->get('subject');
            $message1 = $request->get('message');
            $data1 = array('title' => $title, 'name' => $name, 'email' => $email);
            $data = array('title' => $title1, 'name' => $name, 'phoneNumber' => $phoneNumber, 'subject' => $subject, 'message1' => $message1, 'email' => $email);

            Mail::send('Admin.emails.contactusEmail', $data1, function ($message) use ($data1) {
                $message->from('info@iconiqjewellery.com')->subject($data1['title']);
                $message->to($data1['email']);
            });
            Mail::send('Admin.emails.contactusAdmin', $data, function ($message) use ($data) {
                $message->from($data['email'])->subject($data['title']);
                $message->to('info@iconiqjewellery.com');
            });


            Session::flash('message', '<div class="alert alert-success"><centre><strong>Thank You</strong> For Contact Us</centre> </div>');

            return redirect('contactus');

        } else {
            return redirect()->back()
                ->withErrors(['email' => 'Something went wrong.',])
                ->withInput();
        }

    }

    public function aboutus()
    {
        return view('frontend.aboutus');
    }

    public function cart(Request $request)
    {

        if (!Auth::check()) {

            return redirect::to('login');
        }

        // $items = Session::get('cart', []);

        // $id=Input::get('id');
        // $qty=Input::get('qty');

        // $cartproduct=Product::where('id',$id)->first();
        // $productImage=ProductImage::where('productId',$id)->groupBy()->first();
        // print_r($productImage);
        // exit();
        // $cartproduct->productImage = $productImage->productImageName;


        $items = Session::get('cart', []);

        // $request->session()->push('cart', $cartproduct);
        $data = $request->session()->get('cart');

        return view('frontend.productcart')->with('data', $data)->render();


        // return view('frontend.productcart');
    }

    public function searchdata(Request $request)
    {
        $search = Input::get('search');
        $product = Product::orderBy('id', 'desc')
            ->where('status', 'Active')
            ->where('productItemNumber', 'LIKE', '%' . $search . '%')
            ->paginate(10);
        foreach ($product as $row) {

            $row->productImage = ProductImage::where('productId', '=', $row->id)->groupBy()->value('productImageName');
        }

        $subcategory = SubCategory::orderBy('subCategoryName', 'asc')
            ->get();
// dd($product);
        return view('frontend.product.search', compact('product', 'subcategory'));

    }

}
