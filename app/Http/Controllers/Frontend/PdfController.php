<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\FrontendController;
use App\Http\Controllers\Frontend\ProductController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Barryvdh\Dompdf;
use PDF;
use Auth;
use Session;
use App\Inquiry;
use App\Client;
use App\InquiryProduct;
use App\Settings;


class PdfController extends Controller
{


    public function quatation()
    {


        // $data =::findOrFail($id);
        $pdf = PDF::loadView('frontend.quatation');
        return $pdf->download('Iconiq-Quatation.pdf');

    }


    public function getquotation(Request $request)
    {

        if (!Auth::check()) {

            return redirect::to('login');
        }

        $data = Session::get('cart');

        if (count($data) <= 0) {
            return redirect::to('/');

        }

        $goldRate = Input::get('goldRate');
        $diamondRate = Input::get('diamondRate');
        $labourRate = Input::get('labourRate');


        $ordertotal = 0;
        foreach ($data as $key => $value) {
            $qty = $value['qty'];
            $diamondWeight = $value['diamondWeight'];
            $goldWeight = $value['goldWeight'];
            $gold = $goldRate * $goldWeight;
            $diamond = $diamondRate * $diamondWeight;
            $weight = $diamondWeight + $goldWeight;
            $totallabour = $weight * $labourRate;
            $price = $diamond + $gold + $totallabour;
            $subtotal = $qty * $price;
            $ordertotal += $subtotal;
        }

        $pro1 = Session::get('cart');
        foreach ($pro1 as $key => $pro) {

            $pro->goldRate = $goldRate;
            $pro->diamondRate = $diamondRate;
            $pro->labourRate = $labourRate;
            $pro->price = $price;
            $pro->subtotal = $subtotal;
            $pro->ordertotal = $ordertotal;
            $request->session()->push('cart1', $pro);
        }


        $pdfdata = Session::get('cart1');

        $clientdetails = Client::where('userId', Auth::user()->id)->first();


        $inquiry = new Inquiry();

        $inquiry->clientId = $clientdetails['id'];
        $inquiry->clientEmail = Auth::user()->email;
        $inquiry->clientPhoneNumber = $clientdetails['phoneNumber'];
        $inquiry->createBy = Auth::user()->id;
        $inquiry->modifyBy = Auth::user()->id;


        $settings = Settings::first();

        if ($inquiry->save()) {


            foreach ($data as $key => $value) {


                $InquiryProduct = new InquiryProduct();
                $InquiryProduct->inquiryId = $inquiry->id;
                $InquiryProduct->productId = $value['id'];
                $InquiryProduct->productQuantity = $value['qty'];
                $InquiryProduct->productPrice = $price;
                $InquiryProduct->totalAmount = $value['qty'] * $price;
                $InquiryProduct->createBy = Auth::user()->id;
                $InquiryProduct->modifyBy = Auth::user()->id;
                $InquiryProduct->save();

            }


            $id = $inquiry->id;
            $inquiryproduct = InquiryProduct::where('inquiryId', $id)->get();

        }


        $pdf = PDF::loadView('frontend.quatation', compact('clientdetails', 'pdfdata', 'settings', 'goldRate', 'diamondRate', 'labourRate'));

        // Session::forget('cart');
        // Session::forget('cart1');

        $pdf->download('Iconiq-Quatation.pdf');

        return redirect()->back();

    }

}
