<?php

namespace App\Http\Controllers\frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\User;
use Auth;
use App\Client;




class LoginController extends Controller
{
  public function index(){

   return view('frontend.login.index');   
 }
 public function dologin(Request $request){


  $rules = [
  'email'=>'required|email',
  'password' => 'required'

  ];
  $validator = Validator::make($request->all(), $rules);

  if ($validator->fails()) {
   return redirect()->back()
   ->withErrors($validator)  
   ->withInput();
 }
  // $data['password']= Hash::make($request->get('password'));  
 
    $logindetails = array(
                  'email' => $request['email'],
                  'password' => $request['password']);
//client block-unblock check


$email = $request->get('email');
$userid = Client::where('email',$email)->first();

if($userid['clientStatus'] == 'false'){
   Auth::logout();
  return redirect()->back()->withErrors(['email' =>"You Are Block By Admin..!"]);
}

 
// client block-unblock check over 
   if (Auth::attempt($logindetails))
    { 
            if(Auth::user()->hasRole('client'))
            {
                              
                return Redirect::to('/');

            }    
             Auth::logout();
            return redirect()->back()
                        ->withErrors(['email' =>"Only User Can Login Here..!"]);
                
   } 
    else
        {
          return redirect()->back()
            ->withErrors(['email' =>'Invalid Login Details.']);
        }
}

public function logout()
{
  Auth::logout(); 
  return redirect('/');
   
}



}

