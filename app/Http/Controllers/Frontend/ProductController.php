<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\SubCategoryController;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use App\Category;
use App\SubCategory;
use App\Product;
use App\ProductImage;
use Auth;


class ProductController extends Controller
{

// category page listing

    public function index()
    {
        $subcategory=SubCategory::join('product','product.subcategoryId','=','subcategory.id')->select('subcategory.*')->distinct()->get();
        return view('frontend.product.category', compact('subcategory'));


    }

    // catagory product listing

    public function category($id)
    {

        if (!Auth::check()) {

            return redirect::to('login');
        }

        $checkid = Product::where('subCategoryId', '=', $id)->first();

            $product = Product::where('subCategoryId', '=', $id)->where('status','Active')->paginate(12);

            foreach ($product as $row) {

                $row->productImage = ProductImage::where('productId', '=', $row->id)->groupBy()->value('productImageName');
            }

            $subcategory = SubCategory::where('id', '!=', $id)
                ->where('subCategoryName','!=','NEW ARRIVALS')
                ->orderBy('subCategoryName', 'asc')
                ->get();

            $newArrival = SubCategory::where('subCategoryName','NEW ARRIVALS')->first();

            return view('frontend.product.products', compact('product', 'subcategory', 'id','newArrival'));

    }

// single product
    public function single_product($id)
    {
        if (!Auth::check()) {

            return redirect::to('login');
        }
        $single_product = Product::where('id', '=', $id)->where('status','Active')->first();
        $product_image = ProductImage::where('productId', '=', $id)->get();

        $related_product = Product::where('subCategoryId', '=', $single_product['subCategoryId'])
            ->where('id', '!=', $id)
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();
        foreach ($related_product as $row) {

            $row->productImage = ProductImage::where('productId', '=', $row->id)->groupBy()->value('productImageName');
        }

        return view('frontend.product.single_product', compact('single_product', 'product_image', 'related_product'));
    }


// ajax product filter
    public function gendersearch(Request $request)
    {


        $id = Input::get('id');

        // $amount=Input::get('amount');

        // $price=explode("-", $amount[0]);

        // $female=Input::get('female');
        $gender = Input::get('gender');
        $goldQuality = Input::get('goldQuality');
        $product = Product::where('subCategoryId', '=', $id)->where('status','Active');


        // print_r(count($product));


        // if(!empty($price))
        // {
        //   $product= $product->whereBetween('totalAmount', [intval($price[0]), intval($price[1])]);

        // }

        if (!empty($gender)) {
            $product = $product->whereIn('gender', $gender);

        }


        if (!empty($goldQuality)) {
            $product = $product->whereIn('goldQuality', $goldQuality);

        }

        $product = $product->paginate(10);


        foreach ($product as $row) {

            $row->productImage = ProductImage::where('productId', '=', $row->id)->groupBy()->value('productImageName');
        }


        $result = (string)View::make('frontend.product.ajaxgendersearch')->with('product', $product)->render();
// $result='alert("ok")';

        if (!empty($result)) {

            return [
                'value' => $result,
                'status' => true
            ];

        } else {

            return [
                'value' => "",
                'status' => false
            ];
        }
    }

    public function sortsearch(Request $request)
    {
        $id = Input::get('id');
        $sortBy = Input::get('sortBy');
        $pgi = Input::get('show');


        $product = Product::where('subCategoryId', '=', $id)->where('status','Active');

        if ($sortBy[0] == "new") {

            $product = $product->orderBy('id', 'desc');
        }

        if ($sortBy[0] == "desc") {

            $product = $product->orderBy('totalAmount', 'desc');
        }
        if ($sortBy[0] == "asc") {

            $product = $product->orderBy('totalAmount', 'asc');
        }

        $product = $product->paginate(10);

        foreach ($product as $row) {

            $row->productImage = ProductImage::where('productId', '=', $row->id)->groupBy()->value('productImageName');

        }

        $result = (string)View::make('frontend.product.ajaxgendersearch')->with('product', $product)->render();

        if (!empty($result)) {

            return [
                'value' => $result,
                'status' => TRUE
            ];
        }
        return [
            'value' => "",
            'status' => FALSE
        ];
    }


}
