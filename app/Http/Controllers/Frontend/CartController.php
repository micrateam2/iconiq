<?php

namespace App\Http\Controllers\frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\SubCategoryController;
use Illuminate\Support\Facades\Input;
use Auth;
use DB;
use App\Category;
use App\SubCategory;
use App\Product;
use App\ProductImage;


class CartController extends Controller
{
  public function getproduct(Request $request){  


   $id=Input::get('id');
   $qty=0;

   $cartproduct=Product::where('id',$id)->where('status','Active')->first();
   $productImage=ProductImage::where('productId',$id)->groupBy()->first();
   $cartproduct->productImage = $productImage->productImageName;   
   $cartproduct->qty=$qty;


   
   $items = Session::get('cart', []);

   foreach ($items as $item) {
     if ($item['id'] == $id) {
      return ['value'=>'Product already exist in cart!','status'=>false];              
    }         

  }

  $request->session()->push('cart', $cartproduct);      
  $data =  $request->session()->get('cart');
  
  
  $result = (string) \View::make('frontend.product.ajaxcart')->with('data',$data)->render();  
  return ['value'=>$result,'status'=>true];  

  
}
public function session_data(){
  $data = Session::get('cart');
  $result = (string) \View::make('frontend.product.ajaxcart')->with('data',$data)->render();  
  return ['value'=>$result,'status'=>true];

}
public function session_r($id){
  

   Session::forget('cart.' . $id);
  
  
   $data = Session::get('cart');
 
   return view('frontend.productcart',compact('data'));
}

public function session_remove(Request $request){

   Session::forget('cart.' . $request->get('cpid'));
   $data = Session::get('cart');
   $result = (string) \View::make('frontend.product.ajaxcart')->with('data',$data)->render();  
   return ['value'=>$result,'status'=>true];
   
}


public function storecart(Request $request){

 $productId=$request['id'];
 $qty=$request['qty'];
 $subtotal=$request['subtotal'];
 $totalAmount=$request['totalAmount'];

} 
public function getqtyset(Request $request){

  $productId= $request['productId'];
  $qty=$request['qty'];
  
$data = Session::all();

 $items = Session::get('cart',[]);

   foreach ($items as $item) {
     if ($item['id'] == $productId) {
       $item['qty'] = 0 + $qty;
       

      return ['value'=>'','status'=>true];              
    }         
       }

  
 $result= Session::put('cart', $items);
  // $result=Session::put('cart', $items);
  // dd($result);
  


}
}
