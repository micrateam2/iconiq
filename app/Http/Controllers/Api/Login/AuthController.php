<?php

namespace App\Http\Controllers\Api\Login;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Comman\Comman;
use Validator;
use Auth;
use Role;
use DB;
use Hash;
use App\ApiSession;
use App\User;

class AuthController extends Controller
{
    public function authenticate(Request $request)
    {
        $rule = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['status' => '0', 'message' => 'validation fail', 'data' => ['errors' => $validate->errors()->all()]], 400);
        }

        $checkvalidation = User::where('email',$request['email'])->first();

        if(!empty($checkvalidation)){
            $role = DB::table('role_user')->where('user_id',$checkvalidation->id)->value('role_id');

            if($role == 1) {
                return response()->json(['status' => '0', 'message' => 'Only User Can Login Here','data'=>array()], 400);
            }

            if(Hash::check($request->get('password'),$checkvalidation->password)){
//                if($checkvalidation->is_valid == "True" && $checkvalidation->is_block == 'False' && $checkvalidation->is_authenticate == "True"){

                    $loginData = $checkvalidation;
                    $token = Comman::setSessionData($checkvalidation->id);
                    $status = 1;
                    $message = 'Login successfully.';
                    $data = array('token'=>$token,'user'=>$loginData);

            } else {
                $status = 0;
                $message = 'Invalid Password..!';
                $data = null;
            }
        } else{
            $status = 0;
            $message = 'Invalid Email ..!';
            $data = null;
        }

        $returnData = [];
        $returnData['status'] = $status;
        $returnData['message'] = $message;
        $returnData['data'] = $data;
        echo json_encode($returnData);
        exit();

    }

}
