<?php

namespace App\Http\Controllers\Api\Category;

use App\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Comman\Comman;
use Validator;
use Auth;
use Role;
use DB;
use Hash;
use URL;
use App\ApiSession;
use App\User;
use App\Slider;
use App\Contactus;
use Mail;
class CategoryController extends Controller
{

    /**
     * Category APi
     * @param Request $request
     */
    public function index(Request $request)
    {

        $user = Comman::checkApisSession($request->token);


        $data = SubCategory::join('product', 'product.subcategoryId', '=', 'subcategory.id')->select('subcategory.*')->distinct()->get();

        if ($data) {
            foreach ($data as $row) {
                $row->subCategoryImage = URL::to('public/' . $row->subCategoryImage);
            }
            $returnData = [];
            $returnData['status'] = 1;
            $returnData['message'] = 'Category Get Sucessfully';
            $returnData['data'] = $data;
            echo json_encode($returnData);
            exit();
        } else {
            $returnData = [];
            $returnData['status'] = 1;
            $returnData['message'] = 'No Record Found';
            $returnData['data'] = array();
            echo json_encode($returnData);
            exit();
        }


    }

    /**
     * Slider APi
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function product_slider(Request $request)
    {
        $data = Slider::where('sliderStatus', 'true')->orderBy('id', 'DESC')->get();

        foreach ($data as $item) {

            if (!empty($item->sliderImage) && file_exists('public/' . $item->sliderImage)) {
                $item->sliderImage = URL::to('public/' . $item->sliderImage);
            } else {
                $item->sliderImage = URL::to('public/default_image/default_product.jpg');
            }
        }

        if (count($data)> 0) {
            return response()->json(['status' => 1, 'msg' => 'Slider Get Sucessfully', 'data' => $data], 200);
        }

        return response()->json(['status' => 0, 'msg' => 'Slider is not found', 'data' => []], 400);
    }

    /**
     * About us APi
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function about_us(Request $request){

            $data = '<div class="col-md-12"><h1>About Us</h1><div class="intro-content"><h3>Fashion jewelry is one of the fastest growing categories in the retail world. </h3><p style="text-align: justify;">At Iconiq, we believe, the finest of jewels need not come with the heftiest of price tags. Designed by exceptional artists and crafted by master craftsmen from around the world, Iconiq is all about affordable luxury, exclusive, standout pieces starring in a sumptuous and smooth shopping experience.  </p>'.
                '<br><p style="text-align: justify;">Each piece of jewelry on iconiqjewellery.com comes with the Iconiq assurance of quality and durability. We know the demands on every woman’s time - we create our pieces with utmost care so that you don’t need to spend hours caring for it. A Iconiq jewel is like your best friend, your 3AM buddy, your BFF, someone who knows just what to do or say to bring that special glow on your face, make you feel fabulous inside and look great outside. Someone you can take for granted..</p><br><p style="text-align: justify;">With a highly competent management team consisting of top business school, fashion and technology institute alumnus and e-retail company Iconiq, we aim to create a great lifestyle experience for our esteemed customers. We hope you enjoy shopping on our site but in case we ever disappoint you, please feel free to reach out to us at info@iconiqjewellery.com</p></div></div>'.
                '<div class="container"><div class="title"><h1><i class="fa fa-star"></i> Chairman`s Message <i class="fa fa-star"></i></h1><div class="title-border"></div></div><div class="">'.
               '<p style="text-align: justify;"><span class="quotes_top">&nbsp;</span>I am delighted to present our website showcasing our spectacular jewellery collection and connect you with our stores offline and online. Iconiq Diamong jewellery is one of the leading jewellery brands in India with an enduring commitment to purity and quality.  we have always prided ourselves on our ability to offer our customers superior quality products that give value for money. We have over 150+ showrooms worldwide, with a strong presence in both India and the Middle East, and are well on our way to achieving our aim of becoming the world`s number one jewellery group.</p><br><p style="text-align: justify;">'.
            'Our extensive jewellery collection, impeccable after-sale service and world-class facilities along with uncompromising dedication to personal attention offer a remarkable service experience to our customers. We consider each sale as an everlasting relationship, and believe in keeping our customers always satisfied by protecting their rights and offering the finest products through our quality conscious efforts. Without you, our efforts wouldn`t have been as successful as they are today. I thank you sincerely for all your support and wish you have a wonderful buying experience with us.'.
            '</p></div></div>';


        return response()->json(['status' => 1, 'msg' => 'About us Get Sucessfully', 'data' => $data], 200);
    }

    public function contact_us(Request $request){

        $rule = [
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email',
            'phoneNumber' => 'required|digits:10',
            'subject' => 'required',
            'message' => 'required',
        ];

        $validate = Validator::make($request->all(), $rule);

        if ($validate->fails()) {
            return response()->json(['status' => '0', 'message' => 'validation fail', 'data' => ['errors' => $validate->errors()->all()]], 400);
        }

        $user['firstName'] = $request->get('firstName');
        $user['lastName'] = $request->get('lastName');
        $user['email'] = $request->get('email');
        $user['phoneNumber'] = $request->get('phoneNumber');
        $user['subject'] = $request->get('subject');
        $user['message'] = $request->get('message');

        $contact = new Contactus();
        $contact->fill($user);

        if($contact->save()){

            $title = "Thanks for Being Our Awesome Customer! ";
            $title1 = "Hello.!! Admin You Get A New Conatct Inquiry";
            $name = $request->get('firstName') . '' . $request->get('lastName');
            $email = $request->get('email');
            $phoneNumber = $request->get('phoneNumber');
            $subject = $request->get('subject');
            $message1 = $request->get('message');
            $data1 = array('title' => $title, 'name' => $name, 'email' => $email);
            $data = array('title' => $title1, 'name' => $name, 'phoneNumber' => $phoneNumber, 'subject' => $subject, 'message1' => $message1, 'email' => $email);

            Mail::send('Admin.emails.contactusEmail', $data1, function ($message) use ($data1) {
                $message->from('info@iconiqjewellery.com')->subject($data1['title']);
                $message->to($data1['email']);
            });
            Mail::send('Admin.emails.contactusAdmin', $data, function ($message) use ($data) {
                $message->from($data['email'])->subject($data['title']);
                $message->to('info@iconiqjewellery.com');
            });


            return response()->json(['status' => 1, 'msg' => 'Thank you For your valuable Response', 'data' => $contact], 200);
        }
    }

}
