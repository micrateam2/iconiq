<?php

namespace App\Http\Controllers\Api\Product;

use App\Client;
use App\Helpers\Comman\Comman;
use App\Http\Controllers\Controller;
use App\Inquiry;
use App\InquiryProduct;
use App\Product;
use App\ProductImage;
use App\Settings;
use Illuminate\Http\Request;
use PDF;
use URL;
use Validator;


class ProductController extends Controller {
	public function index(Request $request) {

		$user = Comman::checkApisSession($request->token);

		$category_id = $request->get('category_id');
		$gender = $request->get('gender');
		$caret = $request->get('caret');
		$sort_by = $request->get('sort_by');
		$search = $request->get('search');

		$product = Product::where('status', 'Active');

		if (!empty($category_id)) {

			$product = $product->where('subCategoryId', '=', $category_id);
		}

		if (!empty($gender)) {
			$product = $product->whereIn('gender', $gender);
		}

		if (!empty($caret)) {
			$product = $product->whereIn('goldQuality', $caret);

		}

		if (!empty($search)) {
			$product = $product->where('productItemNumber', 'LIKE', '%' . $search . '%');
		}

		if (!empty($sort_by)) {
			if ($sort_by == 'new') {
				$product = $product->orderBy('id', 'desc');
			}
		}
		$product = $product->where('isblocked', 'False')
			->select('id', 'productItemNumber', 'goldWeight', 'diamondWeight', 'subCategoryId')
			->paginate(12);

		if ($product) {

			foreach ($product as $row) {
				$image = ProductImage::where('productId', '=', $row->id)->groupBy()->value('productImageName');

				if (!empty($image) && file_exists('public/' . $image)) {
					$imagezUrl = URL::to('public/' . $image);
				} else {
					$imagezUrl = URL::to('public/default_image/default_product.jpg');
				}
				$row->product_image = $imagezUrl;
				$row->category_id = $row->subCategoryId;

			}
			$returnData = [];
			$returnData['status'] = 1;
			$returnData['message'] = 'Product Get Sucessfully';
			$returnData['data'] = $product;
			echo json_encode($returnData);
			exit();
		} else {
			$returnData = [];
			$returnData['status'] = 1;
			$returnData['message'] = 'No Record Found';
			$returnData['data'] = array();
			echo json_encode($returnData);
			exit();
		}

	}

	/**
	 * Single Product APi
	 * @param Request $request
	 */
	public function product(Request $request) {
		$user = Comman::checkApisSession($request->token);

		$rule = [
			'product_id' => 'required',
		];

		$validate = Validator::make($request->all(), $rule);

		if ($validate->fails()) {
			return response()->json(['status' => '0', 'message' => 'validation fail', 'data' => ['errors' => $validate->errors()->all()]], 400);
		}

		$data = Product::where('id', $request['product_id'])->first();

		$product_image = ProductImage::where('productId', $request['product_id'])->where('status', 'true')->select('productImageName')->get();
		$image = array();
		foreach ($product_image as $item) {

			if (!empty($item->productImageName) && file_exists('public/' . $item->productImageName)) {
				$item->productImageName = URL::to('public/' . $item->productImageName);
			} else {
				$item->productImageName = URL::to('public/default_image/default_product.jpg');
			}
			$image[] = $item;
		}

		if (!empty($data)) {

			$data['images'] = $image;
		}

		/**
		 * Related Item
		 */

		$related_product = Product::where('subCategoryId', '=', $data['subCategoryId'])
			->where('id', '!=', $request['product_id'])
			->orderBy('id', 'desc')
			->limit(10)
			->get();
		foreach ($related_product as $row) {
			$row->productImage = ProductImage::where('productId', '=', $row->id)->groupBy()->value('productImageName');

			if (!empty($row->productImage) && file_exists('public/' . $row->productImage)) {
				$row->productImage = URL::to('public/' . $row->productImage);
			} else {
				$row->productImage = URL::to('public/default_image/default_product.jpg');
			}
		}

		$product = array(
			'product' => $data,
			'related_product' => $related_product,
		);

		if (!empty($data)) {
			return response()->json(['status' => 1, 'msg' => 'Product Get Sucessfully', 'data' => $product], 200);
		}

		return response()->json(['status' => 0, 'msg' => 'Product is not found', 'data' => []], 400);

	}

	public function checkout(Request $request) {

		$user = Comman::checkApisSession($request->token);

		$data = str_replace(array("\n", "\r", "\t"), '', $request['data']);

		$goldRate = $request['gold_rate'];
		$diamondRate = $request['diamond_rate'];
		$labourRate = $request['labour_rate'];

		$data = json_decode($data);

		$ordertotal = 0;

		$product = array();

		foreach ($data as $row) {
			$datas = Comman::product_data($row->product_id);
			$qty = $row->quantity;
			$diamondWeight = $datas['diamondWeight'];
			$goldWeight = $datas['goldWeight'];
			$gold = $goldRate * $goldWeight;
			$diamond = $diamondRate * $diamondWeight;
			$weight = $diamondWeight + $goldWeight;
			$totallabour = $weight * $labourRate;
			$price = $diamond + $gold + $totallabour;
			$subtotal = $qty * $price;

			$product[] = array(
				'id' => $datas['id'],
				'productItemNumber' => $datas['productItemNumber'],
				'goldWeight' => $datas['goldWeight'],
				'diamondWeight' => $datas['diamondWeight'],
				'qty' => $row->quantity,
				'price' => $price,
				'subtotal' => $subtotal,
				'goldRate' => $goldRate,
				'diamondRate' => $diamondRate,
				'labourRate' => $labourRate,
				'productImage' => $datas['productImageName'],
			);

			$ordertotal += $subtotal;
		}

		$clientdetails = Client::where('userId', $user['user_id'])->first();

		$inquiry = new Inquiry();

		$inquiry->clientId = $clientdetails['id'];
		$inquiry->clientEmail = $user['email'];
		$inquiry->clientPhoneNumber = $clientdetails['phoneNumber'];
		$inquiry->createBy = $user['user_id'];
		$inquiry->modifyBy = $user['user_id'];

		$settings = Settings::first();

		if ($inquiry->save()) {

			foreach ($product as $key => $value) {

				$InquiryProduct = new InquiryProduct();
				$InquiryProduct->inquiryId = $inquiry->id;
				$InquiryProduct->productId = $value['id'];
				$InquiryProduct->productQuantity = $value['qty'];
				$InquiryProduct->productPrice = $price;
				$InquiryProduct->totalAmount = $value['qty'] * $price;
				$InquiryProduct->createBy = $user['user_id'];
				$InquiryProduct->modifyBy = $user['user_id'];
				$InquiryProduct->save();

			}

			$id = $inquiry->id;
			$inquiryproduct = InquiryProduct::where('inquiryId', $id)->get();

		}

		$pdfdata = $product;

		$pdf = PDF::loadView('frontend.quatation', compact('clientdetails', 'pdfdata', 'settings', 'goldRate', 'diamondRate', 'labourRate'));


		$pdf->download('Iconiq-Quatation.pdf');

		return response()->json(['status' => 1, 'msg' => 'Quatation Get Sucessfully', 'data' => []], 200);

	}

	public function inquiryProducts(Request $request) {

		$user = Comman::checkApisSession($request->token);

		$clientdetails = Client::where('userId', $user['user_id'])->first();

		$inquiry = InquiryProduct::inquiryProduct()
			->where('inquiry.clientId', $clientdetails['id'])
			->orderBy('inquiryproduct.inquiryId', 'DESC')
			->get();

		$product = array();

		foreach ($inquiry as $row) {
			$row['productItemNumber'] = $row->product->productItemNumber;
			$row['gold_weight'] = $row->product->goldWeight;
			$row['diamond_weight'] = $row->product->diamondWeight;
			$row['productImage'] = URL::to('public/' . ProductImage::where('productId', '=', $row->productId)->groupBy()->value('productImageName'));

			$product[$row->inquiryId][] = $row;
		}

		$product_data = array();

		if (count($product) > 0) {
			foreach ($product as $value => $key) {
				$product_data[] = array(
					'inquiry_id' => $value,
					'inquiry_data' => $key,
				);
			}
		}

		if (count($product_data) <= 0) {
			return response()->json(['status' => 0, 'msg' => 'No Record Found..!', 'data' => []], 401);
		}
		return response()->json(['status' => 1, 'msg' => 'Inquiry List Get Sucessfully', 'data' => $product_data], 201);

	}
}
