<?php

namespace App;
use App\Category;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
	protected $fillable = [
	'categoryId','subCategoryName','subCategoryImage','createBy','modifyBy'
	];

protected $table='subcategory';

  public function category()
  {
    return $this->belongsTo('App\Category','categoryId');
  }

  public function product() {

      return $this->hasMany('App\Product','subCategoryId');
    }
	
}
