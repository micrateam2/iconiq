<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Contactus extends Model
{
  protected $fillable = [
     'firstName','lastName','email','phoneNumber','message','createBy','modifyBy','subject'
 ];

 protected $table='contactus';  
}
