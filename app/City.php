<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table='cities';

    public function client() {

      return $this->hasMany('App\Client','cityID');
    }
}
