<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacture extends Model
{
    protected $fillable = [
        'name','phoneNumber','address','image','status','createBy','modifyBy'];

    protected $table='manufacture';
}
