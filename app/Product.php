<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

   protected $table='product';

   protected $fillable = [
	'productId','categoryId','subCategoryId','productItemNumber','diamondShape','diamondQualitySize','diamondPisces','diamondWeight','goldQuality','goldWeight','stoneShape','stoneQuality','stoneSize','stonePisces','stoneWeight','isActive','gender','featureProduct','createBy','modifyBy',
	'status','manufactureId','isblocked'
   ];

	  public function category()
  {
    return $this->belongsTo('App\Category','categoryId');
  }

   public function subcategory()
  {
    return $this->belongsTo('App\SubCategory','subCategoryId');
  }

   public function productimage() {

      return $this->hasMany('App\ProductImage','productId');
    }
public function inquiryproduct() {

  return $this->hasMany('App\inquiryproduct','productId');
}

}
