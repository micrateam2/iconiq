<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
      protected $table='states';

      public function client() {

      return $this->hasMany('App\Client','stateID');
    }
}
