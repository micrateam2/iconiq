<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table='productimage';
    protected $fillable = [
	'productId','productImageName','status','createBy','modifyBy'
	];


	  public function product()
  {
    return $this->belongsTo('App\product','productId');
  }
}
