<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InquiryProduct extends Model
{
    protected $fillable = [
        'inquiryId', 'productId', 'productQuantity', 'productPrice', 'totalAmount', 'createBy', 'modifyBy'];

    protected $table = 'inquiryproduct';


    public function inquiry()
    {

        return $this->belongsTo('App\Inquiry', 'inquiryId');
    }

    public function product()
    {

        return $this->belongsTo('App\Product', 'productId');
    }

    public static function inquiryProduct(){
        return static::leftJoin('inquiry','inquiry.id','=','inquiryproduct.inquiryId')
            ->select('inquiryproduct.*','inquiry.clientId as inquiry_client');
    }

}