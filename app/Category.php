<?php

namespace App;
use App\SubCategory;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
       'categoryName','categoryImage','createBy','modifyBy'
   ];
   protected $table='category';
   public function subcategory() {

      return $this->hasMany('App\SubCategory','categoryId');
    }

    public function product() {

      return $this->hasMany('App\Product','categoryId');
    }

  
}
