<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Country;
use App\State;
use App\City;

class Client extends Model
{
     protected $fillable = [
      'userId','firstName','lastName','address','phoneNumber','email','password','countryID','stateID',
      'cityID','pinCode','clientImage','createBy','modifyBy'
  ];

  protected $table='client';

  public function country()
  {
    return $this->belongsTo('App\Country','countryID');
  }

  public function state()
  {
    return $this->belongsTo('App\State','stateID');
  }

   public function cities()
  {
    return $this->belongsTo('App\City','cityID');
  }

   public function user(){
   return $this->hasMany('App\User','userId');    
   }

  public function inquiry()
  {
     return $this->hasMany('App\Inquiry','clientId');
  }

  
}
