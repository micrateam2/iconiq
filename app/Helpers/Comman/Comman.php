<?php
/**
 * Created by PhpStorm.
 * User: Punit
 * Date: 4/11/2018
 * Time: 11:27 AM
 */

namespace App\Helpers\Comman;
use App\ApiSession;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use App\Product;
use App\ProductImage;
use URL;

class Comman{

    public static function setSessionData($userId)
    {
        if (empty($userId)) {
            return "User id is empty.";
        } else {
            /*  FIND USER ID IN API SESSION AVAILABE OR NOT  */
            $getApiSessionData = ApiSession::where('user_id', $userId)->first();
            if ($getApiSessionData) {
                if ($getApiSessionData->delete()) {
                    $apiSession = new ApiSession();
                    /*  SET SESSION DATA  */
                    $sessionData = [];
                    $sessionData['session_id'] = md5(rand());
                    $sessionData['user_id'] = $userId;
                    $sessionData['login_time'] = \Carbon\Carbon::now();
                    $sessionData['active'] = 1;
                    $apiSession->fill($sessionData);
                    if ($apiSession->save()) {
                        return $apiSession->session_id;
                    } else {
                        return FALSE;
                    }
                } else {
                    return FALSE;
                }
            } else {
                $apiSession = new ApiSession();
                /*  SET SESSION DATA  */
                $sessionData = [];
                $sessionData['session_id'] = md5(rand());
                $sessionData['user_id'] = $userId;
                $sessionData['login_time'] = \Carbon\Carbon::now();
                $sessionData['active'] = 1;
                $apiSession->fill($sessionData);
                if ($apiSession->save()) {
                    return $apiSession->session_id;
                } else {
                    return FALSE;
                }
            }
        }
    }

    public static function checkApisSession($sessionId)
    {
        $checkSessionExist = ApiSession::where('session_id', $sessionId)->first();

        if ($checkSessionExist) {
            $sessionData = [];
            $sessionData['id'] = ($checkSessionExist->id) ? $checkSessionExist->id : '';
            $sessionData['session_id'] = ($checkSessionExist->session_id) ? $checkSessionExist->session_id : '';
            $sessionData['user_id'] = ($checkSessionExist->user_id) ? $checkSessionExist->user_id : '';
            $sessionData['email'] = $checkSessionExist->user_data->email;
            $sessionData['active'] = ($checkSessionExist->active) ? $checkSessionExist->active : '';
            $sessionData['login_time'] = ($checkSessionExist->login_time) ? $checkSessionExist->login_time : '';
            return $sessionData;
        } else {
            return array();
        }
    }

    public static function product_data($id){

        $data = Product::where('id', $id)->first();

        $data['productImageName'] = ProductImage::where('productId', $id)->where('status', 'true')->value('productImageName');

        if (!empty($data['productImageName']) && file_exists('public/' . $data['productImageName'])) {
            $data['productImageName'] = URL::to('public/' . $data['productImageName']);
        } else {
            $data['productImageName'] = URL::to('public/default_image/default_product.jpg');
        }

        return $data;
    }
}