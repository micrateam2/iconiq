<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiSession extends Model
{
    protected $table = 'api_session';
    protected $fillable = [
        'session_id', 'user_id', 'active','login_time'
    ];

    public function user_data()
    {
        return $this->belongsTo('App\User','user_id');
    }

}
