<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
     protected $fillable = [
     'clientId', 'clientEmail','clientPhoneNumber','createBy','modifyBy'];

  protected $table='inquiry';

public function client()
  {
    return $this->belongsTo('App\Client','clientId');
  }

  
   public function inquiryproduct() {

      return $this->hasMany('App\InquiryProduct','inquiryId');
    }
}
