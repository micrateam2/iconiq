<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable=['sliderName','sliderImage','createBy','modifyBy'];
    protected $table='slider';
}
