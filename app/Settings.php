<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
	  protected $fillable = [
      'email','phoneNumber','officeNumber','address','createBy','modifyBy'
  ];
    protected $table='settings';

    
}
