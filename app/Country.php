<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
   protected $table='country';

   public function client() {

      return $this->hasMany('App\Client','countryID');
    }
}
