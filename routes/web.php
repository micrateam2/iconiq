<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//Admin Route
Route::get('admin/login', function () {
	if (!Auth::check()) {
		return view('Admin.login');
	}
	else{
		return Redirect::to("admin/dashboard");
	}
});

Route::post('admin/login', 'Admin\AuthController@dologin');


/*404 page route*/
Route::get('404',function(){
	return view('errors.404');
});

Route::group(['prefix' => 'admin', 'middleware' => ['adminauth','role:admin']], function() {
	
	Route::get('/dashboard', 'Admin\HomeController@index');
	Route::get('logout', 'Admin\AuthController@logout');

	/*category*/
	Route::get('category','Admin\CategoryController@index');
	Route::get('category/create','Admin\CategoryController@create');
	Route::post('category/store','Admin\CategoryController@store');
	Route::get('category/edit/{id}','Admin\CategoryController@edit');
	Route::PUT('category/update/{id}','Admin\CategoryController@update');
	Route::get('category/remove/{id}','Admin\CategoryController@remove');

	/*subcategory*/
	Route::get('subcategory', 'Admin\SubCategoryController@index');
	Route::get('subcategory/create','Admin\SubCategoryController@create');
	Route::post('subcategory/store','Admin\SubCategoryController@store');
	Route::get('subcategory/edit/{id}','Admin\SubCategoryController@edit');
	Route::PUT('subcategory/update/{id}','Admin\SubCategoryController@update');
	Route::get('subcategory/remove/{id}','Admin\SubCategoryController@remove');
	

	/*client*/
	Route::get('client', 'Admin\ClientController@index');
	Route::get('client/create','Admin\ClientController@create');
	Route::post('client/store','Admin\ClientController@store');
	Route::get('client/edit/{id}','Admin\ClientController@edit');
	Route::PUT('client/update/{id}','Admin\ClientController@update');
	Route::get('client/remove/{id}','Admin\ClientController@remove');
    Route::get('client/block/{id}','Admin\ClientController@block');

	/*change password*/
	Route::post('client/changepassword','Admin\ClientController@changepassword');

	/*inquiry*/
	Route::get('inquiry', 'Admin\InquiryController@index');
	Route::get('inquiry/remove/{id}','Admin\InquiryController@remove');

	/*inquiry product*/
		route::get('inquiryproduct/{id}','Admin\InquiryProductController@index');

    /**
     * 24/01/2018 Working Manufacture Code
     *
     */
    /**
     * manufacture Routes
     */

    Route::get('manufacture', 'Admin\ManufactureController@index');
    Route::get('manufacture/create','Admin\ManufactureController@create');
    Route::post('manufacture/store','Admin\ManufactureController@store');
    Route::get('manufacture/edit/{id}','Admin\ManufactureController@edit');
    Route::PUT('manufacture/update/{id}','Admin\ManufactureController@update');
    Route::get('manufacture/remove/{id}','Admin\ManufactureController@destroy');
    Route::get('manufacture/block/{id}','Admin\ManufactureController@changestatus');


	/*Product*/
	Route::get('product', 'Admin\ProductController@index');
	Route::get('product/create','Admin\ProductController@create');
	Route::post('product/store','Admin\ProductController@store');
	Route::get('product/edit/{id}','Admin\ProductController@edit');
	Route::PUT('product/update/{id}','Admin\ProductController@update');
	Route::get('product/remove/{id}','Admin\ProductController@remove');
	Route::get('product/removeimage/{id}','Admin\ProductController@imageremove');
	 Route::POST('product/view/{id}','Admin\ProductController@getdetail');
	 Route::POST('product/upload','Admin\ProductController@importProduct');
	 Route::get('product/sampledownload','Admin\ProductController@sampledownload');
    Route::get('product/block/{id}','Admin\ProductController@changestatus');

	/*contactus*/
	Route::get('contactus','Admin\ContactusController@index');
	Route::get('contactus/remove/{id}','Admin\ContactusController@remove');
    Route::POST('contactus/view/{id}','Admin\ContactusController@getdetail');

	/*slider*/ 
	Route::get('slider','Admin\SliderController@index');
	Route::get('slider/create','Admin\SliderController@create');
	Route::post('slider/store','Admin\SliderController@store');
	Route::get('slider/edit/{id}','Admin\SliderController@edit');
	Route::PUT('slider/update/{id}','Admin\SliderController@update');
	Route::get('slider/remove/{id}','Admin\SliderController@remove');
	Route::get('slider/block/{id}','Admin\SliderController@block');

	/*Setting*/
	Route::get('setting','Admin\SettingController@index');
	Route::get('setting/create','Admin\SettingController@create');
	Route::post('setting/store','Admin\SettingController@store');
	Route::get('setting/edit/{id}','Admin\SettingController@edit');
	Route::PUT('setting/update/{id}','Admin\SettingController@update');
	Route::get('setting/remove/{id}','Admin\SettingController@remove');
	

	/* 404 Error Page Route*/
	Route::get('404',function(){
		return view('errors.404Admin');
	});

	
});

Route::auth();

/* front end routes*/

Route::get('/','Frontend\FrontendController@index');
Route::get('products','Frontend\ProductController@index');
Route::get('product/{id}','Frontend\ProductController@product');
Route::get('products/{id}','Frontend\ProductController@category');
Route::get('single_product/{id}','Frontend\ProductController@single_product');

/* Cart*/
Route::post('cart','Frontend\CartController@getproduct');
Route::get('session','Frontend\CartController@session_data');
Route::get('re/{id}','Frontend\CartController@session_r');
Route::post('re','Frontend\CartController@session_remove');
Route::post('qty','Frontend\CartController@qty');
Route::get('productcart','Frontend\FrontendController@cart');
Route::post('storecart','Frontend\PdfController@getquotation');
Route::post('getqty','Frontend\CartController@getqtyset');



/*Filter Route*/
Route::post('gendersearch','Frontend\ProductController@gendersearch');
Route::post('sortsearch','Frontend\ProductController@sortsearch');


  /* login page frontend side*/


Route::get('login', function () {
	if (!Auth::check()) {
		 return view('frontend.login.index');
	}
	else{
		return Redirect::to("/");
	}
});

  // Route::get('login','Frontend\LoginController@index');
  Route::patch('frontend/client/login','Frontend\LoginController@dologin');
  Route::get('client/logout','Frontend\LoginController@logout');


  /* collection routes*/
Route::get('collection/{id}','Frontend\FrontendController@collection');

Route::get('contactus','Frontend\FrontendController@contactus');
Route::POST('contactus/store','Frontend\FrontendController@store');

/*AboutUs*/
Route::get('aboutus','Frontend\FrontendController@aboutus');

/*Search Route*/

Route::post('searchdata','Frontend\FrontendController@searchdata');


/*pdf generation*/
Route::get('/pdf','Frontend\PdfController@quatation');
Route::get('/quatation', function(){
		  return view('frontend.quatation1');
    
});

Route::group(['prefix' => 'api'], function() {

    Route::POST('/login','Api\Login\AuthController@authenticate');

    Route::group(['middleware'=>'ApiAuth'],function(){
        Route::POST('/getcategory','Api\Category\CategoryController@index');
        Route::POST('/get_slider','Api\Category\CategoryController@product_slider');
        Route::POST('/about-us','Api\Category\CategoryController@about_us');
        Route::POST('/contact-us','Api\Category\CategoryController@contact_us');

        Route::POST('/product_list','Api\Product\ProductController@index');
        Route::POST('/get_product_info','Api\Product\ProductController@product');
        Route::POST('/checkout','Api\Product\ProductController@checkout');
        Route::POST('/inquiry-list','Api\Product\ProductController@inquiryProducts');

    });





});

