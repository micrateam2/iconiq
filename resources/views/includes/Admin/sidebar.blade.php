<div class="page-sidebar-wrapper">
                    
                    <div class="page-sidebar navbar-collapse collapse">
                       
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            
                            <li class="nav-item">
                                <a href="{{url::to('admin/dashboard')}}" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                               
                            </li>
                            <li class="heading">
                                <h3 class="uppercase">Features</h3>
                            </li>
                            <li class="nav-item {{ Request::is('admin/category') ? 'active open' : '' || Request::is('admin/category/create') ? 'active open' : '' }}  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Category</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{Request::is('admin/category') ? 'active open' : '' }}  ">
                                        <a href="{{url::to('admin/category')}}" class="nav-link ">
                                            <span class="title">View</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ Request::is('admin/category/create') ? 'active open' : ''}}  ">
                                        <a href="{{url::to('admin/category/create')}}" class="nav-link ">
                                            <span class="title">Add</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item {{ Request::is('admin/subcategory') ? 'active open' : '' || Request::is('admin/subcategory/create') ? 'active open' : '' }}   ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">SubCategory</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  {{ Request::is('admin/subcategory') ? 'active open' : '' }} ">
                                        <a href="{{url::to('admin/subcategory')}}" class="nav-link ">
                                            <span class="title">View</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ Request::is('admin/subcategory/create') ? 'active open' : '' }}  ">
                                        <a href="{{url::to('admin/subcategory/create')}}" class="nav-link ">
                                            <span class="title">Add</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item {{ Request::is('admin/client') ? 'active open' : '' || Request::is('admin/client/create') ? 'active open' : '' }}  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Clients</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ Request::is('admin/client') ? 'active open' : '' }} ">
                                        <a href="{{url::to('admin/client')}}" class="nav-link ">
                                            <span class="title">View</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ Request::is('admin/client/create') ? 'active open' : '' }}  ">
                                        <a href="{{url::to('admin/client/create')}}" class="nav-link ">
                                            <span class="title">Add</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item {{ Request::is('admin/manufacture') ? 'active open' : '' || Request::is('admin/manufacture/create') ? 'active open' : '' }}  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Manufactures</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ Request::is('admin/manufacture') ? 'active open' : ''}} ">
                                        <a href="{{url::to('admin/manufacture')}}" class="nav-link ">
                                            <span class="title">View</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ Request::is('admin/manufacture/create') ? 'active open' : '' }} ">
                                        <a href="{{url::to('admin/manufacture/create')}}" class="nav-link ">
                                            <span class="title">Add</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                             <li class="nav-item {{ Request::is('admin/product') ? 'active open' : '' || Request::is('admin/product/create') ? 'active open' : '' }}  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Products</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ Request::is('admin/product') ? 'active open' : ''}} ">
                                        <a href="{{url::to('admin/product')}}" class="nav-link ">
                                            <span class="title">View</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ Request::is('admin/product/create') ? 'active open' : '' }} ">
                                        <a href="{{url::to('admin/product/create')}}" class="nav-link ">
                                            <span class="title">Add</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            
                            <li class="nav-item  {{ Request::is('admin/inquiry') ? 'active open' : ''}}">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Inquiry</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  {{ Request::is('admin/inquiry') ? 'active open' : ''}}">
                                        <a href="{{ URL::to('admin/inquiry')}}" class="nav-link ">
                                            <span class="title">View</span>
                                        </a>
                                    </li>
                                </ul>                   
                                
                            </li>
                             <li class="nav-item  {{ Request::is('admin/contactus') ? 'active open' : ''}}">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">ContactUs</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ Request::is('admin/contactus') ? 'active open' : ''}} ">
                                        <a href="{{ URL::to('admin/contactus')}}" class="nav-link ">
                                            <span class="title">View</span>
                                        </a>
                                    </li>
                                </ul>                   
                                
                            </li>

                             <li class="nav-item {{ Request::is('admin/slider') ? 'active open' : '' || Request::is('admin/slider/create') ? 'active open' : '' }}  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Slider</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item {{ Request::is('admin/slider') ? 'active open' : ''}}  ">
                                        <a href="{{url::to('admin/slider')}}" class="nav-link ">
                                            <span class="title">View</span>
                                        </a>
                                    </li>
                                    <li class="nav-item {{ Request::is('admin/slider/create') ? 'active open' : ''}}   ">
                                        <a href="{{url::to('admin/slider/create')}}" class="nav-link ">
                                            <span class="title">Add</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item {{ Request::is('admin/setting') ? 'active open' : '' || Request::is('admin/setting/create') ? 'active open' : '' }}  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Settings</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  {{ Request::is('admin/setting') ? 'active open' : '' }} ">
                                        <a href="{{url::to('admin/setting')}}" class="nav-link ">
                                            <span class="title">View</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>

                            
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>