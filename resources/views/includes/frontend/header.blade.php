

<?php $user = DB::table('settings')->where('status', 'true')->first();?>

<!-- header -->
<header class="clearfix">

	<!-- top bar -->
	<div class="top-bar">
		<div class="container">
			
			<!-- contact data -->
			<ul class="fast-contact pull-left">
				<li>
					<i class="fa fa-phone"></i>
					<a href="tel:+91-1234567890">+91-{{$user->phoneNumber}}</a>
				</li>
				<li>
					<i class="fa fa-envelope"></i>
					<a href="mailto:{{$user->email}}">{{$user->email}}</a>
				</li>
			</ul>

			<div class="pull-right">
				
				<!-- search form -->
				{{  Form::open(array('url'=>'/searchdata' , 'method' =>'POST','class'=>'top-searchform form-inline','id'=>'ajax_search'))}}

				<!-- <form action="#" method="POST" class="top-searchform form-inline"> -->
				<div class="form-group">
					{{ Form::text('search','',array('class'=>'form-control','data-required'=>'1','id'=>'search','required'=>'true','placeholder'=>"Search here..."))}}
					<!-- <input type="text" class="form-control" name="serch" id="search" placeholder="Search here..."> -->
				</div>
				<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
				<!-- </form> -->
				{{ Form::close()}}


			</div>

			<div class="clearfix"></div>

		</div>
	</div> <!-- ./ top bar -->

	<!-- navigation -->
	<nav class="navbar navbar-default navbar-static-top animated" id="main-menu">
		<div class="container">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">MENU</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand logo pull-left" href="{{URL::to('/')}}"><img src="{{URL::to('frontend/assets/images/Iconiq.png')}}" width="125px" height="50px"></a>
			</div>


			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="{{Request::is('/') ? 'active' : ''}}"><a href="{{URL::to('/')}}">Home</a></li>

					<li class="{{Request::is('products') ? 'active' : '' || Request::is('products/*') ? 'active' : '' || Request::is('products/*') ? 'active' : '' || Request::is('single_product/*') ? 'active' : '' || Request::is('productcart') ? 'active' : ''}}"><a href="{{URL::to('products')}}">Products</a></li>
					<li class="{{ Request::is('aboutus') ? 'active' : ''}}"><a href="{{URL::to('aboutus')}}">About</a></li>
					<li class="{{ Request::is('contactus') ? 'active' : ''}}"><a href="{{URL::to('contactus')}}">Contact</a></li>

				</ul>
				<ul class="nav navbar-nav navbar-right hidden-sm"> 
					<li class="dropdown">
						@if(Auth::User()  && Auth::user()->hasRole('client'))					
						<ul style="margin-top:20px;">
							<strong><a title="Purchase"  href="#">{{Auth::User()->first_name}}</a></strong>	&nbsp;
							<a title="Purchase"  href="{{URL::to('client/logout')}}">Logout</a>

						</ul>

						@else
						<a title="Purchase"  href="{{URL::to('login')}}">Login</a>

						@endif
					</li>
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-shopping-bag animated bounceIn">
								<!-- <span>{{count(Session::get('cart'))}}</span> -->
							</i>
						</a>
						<ul class="dropdown-menu cart-toggl animated bounceIn" id="cartproduct">
							<li class="carthead">YOUR SHOPPING CART</li>
							<li class="cartp">
									<!-- <div class="pull-left">	                                      
                                        
										<img src="" alt="prod_name">
										<p>
											<strong class="pname" >Product Name</strong>
											<span>QTY: <b>2</b></span>
										</p>
									</div>
									<div class="pull-right">
										<span>100</span>
										<a href="#"><i class="fa fa-times"></i></a>
									</div> -->
									No Items Found In Your Cart
								</li>
								
								
								<!-- <li class="cart-chk-action">
									<a href="{{URL::to('/cart')}}" class="medium-button button-red pull-left">Cart</a>
									<p class="pull-right"><strong>TOTAL</strong> $280</p>
								</li> -->
							</ul>
						</li>
					</ul>
					
				</div>
				
			</div>
		</nav> <!-- ./ navigation -->
		
	</header><!-- ./ header -->
	<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery-ui.js')}}"></script>

	<script>
		$(document).ready(function(){
			$.ajax({
				url:"{{url::to('/session')}}",
				type: 'GET',

				success: function(result){  

					if(result.status){
						$("#cartproduct").html(result.value);

					}                           
				},       

			});
			$(document).on('click', '.remove', function(){

				var id=$(this).data('id');
				$.ajax({
					url: "{{url::to('re')}}",
					data: { 
						_token: "{{ csrf_token() }}",
						'cpid' : id
					},
					type: "post",
					cache: false,
					success: function (result) {

						if(result.status){
							$("#cartproduct").html(result.value);

						}

					}  
				});

			});
		});
	</script>
	




