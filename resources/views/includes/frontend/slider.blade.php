<!-- hero slider -->
<ul id="heroslider">
	@if(empty($slider))

	<div class="alert "><strong><center> &nbsp;No Slider Image!! </center></strong></div>
	@else   
	@foreach($slider as $row2)
	<li>
		<a href="#slide{{$row2->id}}">
			<img src="{{URL::to('public/'.$row2->sliderImage)}}" alt="">
		</a>
	</li>
	 @endforeach  
            @endif
</ul>
		<!-- ./ hero slider -->