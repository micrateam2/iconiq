 
<?php $user = DB::table('settings')->where('status', 'true')->first();?>

 <footer style="background-image:url('{{URL::to('frontend/assets/images/backgrounds/footer-bg.jpg')}}');">
        <div class="container">
        
       
            <!-- upper footer -->
            <div class="row">
                <div class="upper-footer">
                
                    <!-- links -->
                    <div class="col-md-3 col-sm-6">
                        <div class="widget link-widget">
                            <h4 class="widget-heading">Our Links</h4>
                            <ul class="widget-data">                                
                                <li><a href="{{URL::to('/')}}">Home</a></li>
                                <li><a href="{{URL::to('products')}}">Products</a></li>
                                <li><a href="{{URL::to('contactus')}}">Contact us</a></li>
                                <li><a href="{{URL::to('aboutus')}}">About us</a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <!-- links -->
                    <div class="col-md-3 col-sm-6">
                        <div class="widget link-widget">
                           <!--  <h4 class="widget-heading">Links</h4> -->
                            <!-- <ul class="widget-data">
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">Our Blog</a></li>
                                <li><a href="#">Affilates</a></li>
                            </ul> -->
                        </div>
                    </div>
                    
                    <!-- links -->
                    <div class="col-md-3 col-sm-6">
                        <div class="widget link-widget">
                            <!-- <h4 class="widget-heading">Links</h4> -->
                            <!-- <ul class="widget-data">
                                <li><a href="#">EULA</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms & Conditions</a></li>
                            </ul> -->
                        </div>
                    </div>
                    
                    <!-- address links -->
                    <div class="col-md-3 col-sm-6">
                        <div class="widget location-widget">
                            <a href="http://iconiqjewellery.com"><h4 class="widget-heading">ICONIQJEWELLERY.COM</h4></a>
                            <ul class="widget-data">
                                <li>{{$user->address}} <i class="fa fa-paper-plane"></i></li>
                                <li><span>tel: </span> +91-{{$user->phoneNumber}} <i class="fa fa-phone"></i></li>
                                <li><span>email: </span> {{$user->email}} <i class="fa fa-envelope"></i></li>
                            </ul>
                        </div>
                    </div>
                
                </div> 
            </div> <!-- ./ upper footer -->
            
            
            <!-- middle footer -->
   <!--          <div class="row">
                <div class="middle-footer">
                    
                    cta
                    <div class="col-md-4 col-sm-6">
                        <div class="cta-call">
                            <h4>Call us Today</h4>
                            <a href="tel:1234567890">+91-1234567890</a>
                        </div>
                    </div>
                    
                    follow us
                    <div class="col-md-4 col-sm-6">
                        <div class="footer-social">
                            <h4>Follow Us</h4>
                            <ul>
                                <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="insta"><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li class="google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    
                   <.-- payments metonds--.>
                    <div class="col-md-4 col-sm-12">
                        <div class="payment-accepts">
                            <img src="{{URL::to('frontend/assets/images/payment.png')}}" alt="we accepts all cards">
                        </div>
                    </div>
                    
                </div>
            </div> ./ middle footer  -->
            
            
            <!-- copyright -->
            <div class="row">
                <div class="copyright">
                
                    <!-- copyright info -->
                    <div class="cp-info pull-left">
                        <p>&copy; 2016 - 2017 iconiqjewellery.com all rights reserved</p>
                    </div>
                    
                    <!-- copyright links -->
                 <!--    <div class="cp-links pull-right">
                        <ul>
                            <li><a href="#">end user license agreement</a></li>
                            <li><a href="#">privacy policy</a></li>
                            <li><a href="#">terms & conditions</a></li>
                        </ul>
                    </div> -->
                    
                </div>
            </div>
            
        </div>
    </footer>       