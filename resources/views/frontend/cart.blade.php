@extends('layouts.frontend')
@section('title')
ICONIQ 
@endsection
@section('content')
	
	  <div class="checkout">
                <div class="container">

					<!-- page title -->
                    <div class="check-anchor clearfix mb40">
                        <div class="holder">
                            <ul>
                                <li class="active"><a href="#"><i class="fa fa-star"></i> Shopping Cart <i class="fa fa-star"></i></a></li>
                            </ul>
                            <div class="holder-border"></div>
                        </div>
                    </div>
					
					<!-- cart -->
                    <div class="check-infos">
                        <div class="row">
						
							<!-- cart products -->
                            <div class="col-md-8">
                                <div class="check-details">
									
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Details</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Subtotal</th>
                                                <th></th>
                                            </tr>
                                        </thead>
										
                                        <tbody class="check-body">
										
											<!-- prod 1 -->
                                            <tr>
                                                <td><img src="assets/images/check1.png" alt=""></td>
                                                <td>
                                                    <h6>Grey California Dress</h6>
                                                    <p>Size: <span>XL</span></p>
                                                    <p>Color: <span>Black</span></p>
                                                </td>
                                                <td>$ 3199.00</td>
                                                <td>
                                                    <input type="text" placeholder="1">
                                                </td>
                                                <td>$ 3199.00</td>
                                                <td>
                                                    <a href="#"><img src="assets/images/delete.png" alt=""></a>
                                                </td>
											</tr>
											
											<!-- prod 2 -->
                                            <tr>
												<td><img src="assets/images/check2.png" alt=""></td>
												<td>
													<h6>Brown Leather Hand Bag</h6>
													<p>Size: <span>XL</span></p>
													<p>Color: <span>White</span></p>
												</td>
												<td>$ 199.00</td>
												<td>
													<input type="text" placeholder="1">
												</td>
												<td>$ 199.00</td>
												<td>
													<a href="#"><img src="assets/images/delete.png" alt=""></a>
												</td>
                                            </tr>
											
                                        </tbody>
                                    </table>
									
									<!-- cart footer -->
                                    <div class="coupon">
                                        <a href="#" class="update">Add more items</a>
                                        <a href="#" class="proceed">Proceed to Quote</a>
                                    </div>
									
                                </div>
                            </div> <!-- ./ cart products -->
							
							<!-- cart calc -->
                            <div class="col-md-4">
                                <div class="check-aside">
									
									<!-- counts -->
                                    <div class="orders mb20">
                                        <h6>Cart totals</h6>
                                        <p>Cart Subtotal: <span>$ 4 122</span></p>
                                        <p>Shipping and Handling: <span>$ 250</span></p>
                                        <p class="bd0"><strong>Order Total: <span>$ 4 372</span></strong></p>
                                    </div>
									
									<!-- shippings -->
                                    <div class="shipping">
                                        <h6>CALCULATE sHIPPING</h6>
                                        <select class="select">
                                            <option value="Select Size">Select State</option>
                                            <option value="Albania">Bihar</option>
                                            <option value="Australia">West Bengal</option>
                                            <option value="UK">Sikkim</option>
                                            <option value="US">Madhypradesh</option>
                                        </select>
                                        <select class="select">
                                            <option value="Select Size">Select City</option>
                                            <option value="Albania">Jalpaiguri</option>
                                            <option value="Australia">Malbazar</option>
                                            <option value="UK">Damdim</option>
                                            <option value="US">Chalsa</option>
                                        </select>
                                        <input type="text" placeholder="Zip/Postcode">
                                       <a href="{{URL::to('pdf')}}" class="update"> <input type="submit" value="Get Quatation"></a>
                                    </div> <!-- ./ shippings -->
									
                                </div>
                            </div> <!-- ./ cart calc -->
							
                        </div>
                    </div> <!-- ./ cart -->

                </div>
            </div>


@endsection