@extends('layouts.frontend')
@section('title')
ICONIQ

@endsection

@section('content')
@include('includes.frontend.slider') 

<!-- collections -->
<div class="collection clearfix pt40">
    <div class="container">
        <div class="row">
            <div class="check-anchor clearfix">
                <div class="holder">
                    <ul>
                        <li class="active"><a href="#"><i class="fa fa-star"></i> Product Categories <i class="fa fa-star"></i></a></li>
                    </ul>
                    <div class="holder-border"></div>
                </div>
            </div>
            <div class="cat-masonry">
            @if(empty($subcategory))

                <div class="alert "><strong><center> &nbsp;No Data Found In This Page.!! </center></strong></div>
            @else
                @foreach($subcategory as $row)
                    <div class="item">
                        <img src="{{URL::to('public/'.$row->subCategoryImage)}}" alt="">
                        <div class="cat-infos left-shadow">
                            <div class="ci-data">
                                <h3>{{$row->subCategoryName}}</h3>
                                <a href="{{URL::to('products/'.$row->id)}}"> View All <i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            <div class="clear"></div>
        </div>


    </div>
</div>
</div> <!-- ./ collections -->



<!-- new arrivals -->
<div class="arrivals">

    {{--<div class="container">--}}
        {{--<div class="filters">--}}

           {{--<!-- filter [filtering with class1, class2, class3] add product to make filtering <li class="class2">HERE</li>  -->--}}
           {{--<div class="filter clearfix">--}}
            {{--<div class="holder">--}}
                {{--<ul>--}}
                    {{--<li><a href="#" class="active" data-filter="*">All</a></li>--}}
                     {{--@if(empty($subcategory))--}}

                    {{--<div class="alert "><strong><center> &nbsp;No Category!! </center></strong></div>--}}
                    {{--@else--}}
                    {{--@foreach($subcategory as $row1)--}}
                    {{--<li><a href="#" data-filter=".class{{$row1->id}}"><i class="fa fa-star"></i> {{ $row1->subCategoryName}}</a></li>--}}
                    {{--@endforeach--}}
                    {{--@endif--}}
                {{--</ul>--}}
                {{--<div class="holder-border"></div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="clear"></div>--}}


        <!-- products holder -->
        {{--<div class="demo1 clearfix">--}}
            {{--<ul class="filter-container clearfix">--}}
               {{--@if(empty($product))--}}

               {{--<div class="alert "><strong><center> &nbsp;No Data Found In This Page.!! </center></strong></div>--}}
               {{--@else--}}
               {{--@foreach($product as $row)--}}
               <!-- product -->
               {{--<li class="class{{$row->subCategoryId}}">--}}
                {{--<div class="arrival-overlay">--}}
                    {{--@if(!empty($row->productImage) && file_exists('public/'.$row->productImage))--}}
                    {{--{{ $imagezUrl = URL::to('public/'.$row->productImage)}}--}}
                    {{--@else--}}
                    {{--{{$imagezUrl = URL::to('public/default_image/default_product.jpg')}}--}}
                    {{--@endif--}}
                    {{--<img src="{{ $imagezUrl }}" width="270" height="320"/>--}}

                    {{--<div class="arrival-mask">--}}
                    {{--<a href="#" data-id="{{$row->id}}" class="medium-button button-red add-cart cartid" onclick="cartsend({{$row->id}})">Add to Cart</a>--}}
                         {{--<a href="#" class="medium-button button-red add-cart">Add to Cart</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="arr-content">--}}
                   {{--<a href="{{URL::to('single_product/'.$row->id)}}">--}}
                    {{--<p class="low-price">{{substr(' '.$row->productItemNumber, 0, 25)}}</p>--}}

                    {{--<p><strong style="color:#DF7D15";>GWt:</strong>{{' '.round($row->goldWeight,2).'  GM'}}||<strong style="color:#DF7D15";>DWt:</strong>{{' '.round($row->diamondWeight,2).'  CT'}}</p>--}}

                        {{--<p>{{substr($row->productName, 0, 25)}}</p>--}}
                    {{--</a>--}}
                    {{--<ul>--}}
                      {{--<li><span class="low-price">--}}

                      {{--</li> --}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</li>--}}
            {{--@endforeach--}}
            {{--@endif--}}
        {{--</ul>--}}
    {{--</div>--}}
            <!-- ./ products holder -->

{{--</div>--}}
{{--</div>--}}
</div>
<!-- ./ new arrivals -->

<script>
    function cartsend(id){
        // var id=$('.cartid').data('id');
        // alert(id);
        $.ajax({
            url: "{{URL::to('/cart')}}",
            data: { 
                _token: "{{ csrf_token() }}",
                'id' : id
            },
            type: "post",
            cache: false,
            success: function (result) {
                console.log(result); 
                if(result.status){
                 $("#cartproduct").html(result.value);
             } 
             else{
                // alert(result.value);
            }   
        }  
    });
    }

</script>
@endsection