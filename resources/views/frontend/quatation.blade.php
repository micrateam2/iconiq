<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Iconiq | Product Quatation</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for invoice sample" name="description" />
        <meta content="" name="author" />

<style type="text/css">
.page-title{
  font-size: 50px;
  font-family: transition-timing-function: initial;
}

    .invoice-content-2 {
  background-color: #fff;
  padding: 80px 70px; }
  .invoice-content-2.bordered {
    border: 1px solid;
    border-color: #e7ecf1; }
  .invoice-content-2 .invoice-head {
    margin-bottom: 50px; }
    .invoice-content-2 .invoice-head .invoice-logo {
      text-align: center; }
      .invoice-content-2 .invoice-head .invoice-logo > img {
        float: left;
        margin-right: 45px; }
      .invoice-content-2 .invoice-head .invoice-logo > h1 {
        float: left;
        font-size: 17px;
        font-weight: 700;
        color: #39424a;
        margin-top: 48px; }
    .invoice-content-2 .invoice-head .company-address {
      text-align: right;
      font-size: 14px;
      color: #7e8691; }
  .invoice-content-2 .invoice-title {
    font-size: 12px;
    font-weight: 600;
    letter-spacing: 1px;
    color: #9ea8b7; }
  .invoice-content-2 .invoice-desc {
    font-size: 14px;
    color: #4e5a64;
    font-weight: 600; }
    .invoice-content-2 .invoice-desc.inv-address {
      font-size: 13px;
      color: #7e8691;
      font-weight: 400; }
    .invoice-content-2 .invoice-desc.grand-total {
      font-size: 16px;
      font-weight: 700; }
  .invoice-cust-add {
    margin-bottom: 100px; 
    margin-right: -1000px;
    width: 1000px;

}
.invoice-body div:{
  /*padding-top: auto;*/
  margin-top: 100px;
}
  .invoice-content-2 .invoice-body .text-center {
    text-align: center; }
  .invoice-content-2 .invoice-body tr:last-child {
    border-bottom: 1px solid #e7ecf1; }
  .invoice-content-2 .invoice-body th, .invoice-content-2 .invoice-body td {
    vertical-align: middle;
    padding-left: 40px;
    padding-right: 40px; }
    .invoice-content-2 .invoice-body th:first-child, .invoice-content-2 .invoice-body td:first-child {
      padding-left: 0; }
    .invoice-content-2 .invoice-body th:last-child, .invoice-content-2 .invoice-body td:last-child {
      padding-right: 0; }
  .invoice-content-2 .invoice-body h3 {
    font-size: 14px;
    font-weight: 600;
    color: #4e5a64;
    margin-bottom: 0; }
  .invoice-content-2 .invoice-body p {
    font-size: 13px;
    color: #7e8691; }
  .invoice-content-2 .print-btn {
    float: right;
    font-size: 14px;
    font-weight: 600;
    letter-spacing: 1px; }

@media (max-width: 1024px) {
  .invoice-content-2 .invoice-head .invoice-logo > img {
    width: 130px;
    margin-top: 26px; } }

@media (max-width: 991px) {
  .invoice-content-2 {
    padding: 40px 30px; } }

@media (max-width: 480px) {
  .invoice-content-2 .invoice-head .invoice-logo > h1 {
    margin-top: 10px; }
  .invoice-content-2 .invoice-head .company-address {
    font-size: 12px; } }

</style>

<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-md">
        <div class="page-wrapper">
   
                        <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content" style="margin-top: -50px;">
                     
                        <h1 class="page-title">Product Quatation
                            <small></small>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="invoice-content-2 bordered">
                            <div class="row invoice-head">
                                <div class="col-md-7 col-xs-6">
                                    <div class="invoice-logo">
                                      <div style="height: auto;font-weight: bold;color: #fa0;font-size: 46px;line-height: 64px;padding: 0 15px 0 0;margin: 0 25px 0 0; margin-left:-450px; margin-top: -70px;" href="http://localhost/iconiq">
                                        <span style=" color: #e53a40;">I</span>
                                        CONIQ
                                      </div>
                                    
                                    </div>
                                </div>
                                <div class="col-md-5 col-xs-6">
                                    <div class="company-address"><br>
                                        <span class="bold uppercase">Iconiq Diamond Jewelery</span>
                                        <br/><p> {{$settings['address']}}</p>
                                        <br/>
                                        <span class="bold">T</span> {{$settings['phoneNumber']}}
                                        <br/>
                                        <span class="bold">E</span> {{$settings['email']}}
                                        <br/>
                                        <span class="bold">W</span> www.keenthemes.com </div>
                                </div>
                                
                            </div>
                            
                            <div class="row invoice-cust-add" style="margin-top: -120px;">
                                <span class="" style="width: 200px; display: inline-block;">
                                    <h2 class="invoice-title uppercase">Customer</h2>
                                    <p class="invoice-desc">{{$clientdetails['firstName'] . ' ' . $clientdetails['lastName']}}</p>
                                </span>
                                <span class="" style="width: 150px; display: inline-block;">
                                    <h2 class="invoice-title uppercase">Date</h2>
                                    <p class="invoice-desc">{{Carbon\Carbon::now()->format('d/m/Y')}}</p>
                                </span>
                                <span class="" style="width: 300px; display: inline-block;">
                                    <h2 class="invoice-title uppercase">Address</h2>
                                    <p class="invoice-desc inv-address">{{$clientdetails['address']}}</p>
                                </span>
                            </div>

                      
                            <div class="row invoice-body">

                                <div class="">
                                <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class="invoice-title uppercase">Gold Rate</th>
                                                <th class="invoice-title uppercase">Diamond Rate</th>
                                                <th class="invoice-title uppercase">Labour Rate</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                      <tr>
                                                <td class="text-center sbold">{{$goldRate}}</td>
                                                <td class="text-center sbold">
                                                <p>
                                                    {{$diamondRate}}
                                                    </p>

                                                </td>
                                                <td class="text-center sbold">{{$labourRate}}</td>
                                                
                                            </tr>   
                                            </tbody>
                                    </table>
                                    <hr>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th class="invoice-title uppercase">Product Image</th>
                                                <th class="invoice-title uppercase">Details</th>
                                                <th class="invoice-title uppercase">Quantity</th>
                                                <th class="invoice-title uppercase">Rate</th>
                                                <th class="invoice-title uppercase text-center">Total</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php $ordertotal = 0; ?>
                                     
                                        @foreach($pdfdata as $row)
                                            <tr>
                                                <td class="text-center sbold"><img src="{{URL::to('public/'.$row['productImage'])}}" width="50" height="50" /></td>
                                                <td class="text-center sbold" style="width: 30px;">
                                                <p>
                                                    {{$row['productItemNumber']}}
                                                    Gold Wgt:-{{$row['goldWeight']}}
                                                    Diamond Wgt:-{{$row['diamondWeight']}}
                                                    </p>

                                                </td>
                                                <td class="text-center sbold">{{$row['qty']}}</td>
                                                <td class="text-center sbold">{{$row['price']}}</td>
                                                <td class="text-center sbold">{{$row['subtotal']}}</td>
                                            </tr>   
                                           <div style="display: none;"> {{$ordertotal += $row['qty'] * $row['subtotal']}}</div>
                                        @endforeach
                                      
                                        </tbody>
                                    </table>
                                </div>
                            </div>
  
                            <div class="row invoice-subtotal" style="margin-top: 10px;">
                               <span class="" style="width: 200px; display: inline-block;">
                                    <h2 class="invoice-title uppercase">Order Total</h2>
                                    <p class="invoice-desc grand-total">{{$ordertotal}}</p>
                                </span>
                            </div>
                            
                        </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>

</html>