<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
    
    <title>Quatation</title>

    <style type="text/css">
    

body { 
font: 14px/1.4 Georgia, serif; }
#page-wrap { width: 800px; margin: 0 auto; }

textarea { border: 0; font: 14px Georgia, Serif; overflow: hidden; resize: none; }
table { border-collapse: collapse; }
table td, table th { border: 1px solid black; padding: 5px; }

#header { height: 15px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }

#address { width: 250px; height: 150px; float: left; }
#customer { overflow: hidden; }

#logo { text-align: right; float: right; position: relative; margin-top: 25px; border: 1px solid #fff; max-width: 540px; max-height: 100px; overflow: hidden; }
#logo:hover, #logo.edit { border: 1px solid #000; margin-top: 0px; max-height: 125px; }
#logoctr { display: none; }
#logo:hover #logoctr, #logo.edit #logoctr { display: block; text-align: right; line-height: 25px; background: #eee; padding: 0 5px; }
#logohelp { text-align: left; display: none; font-style: italic; padding: 10px 5px;}
#logohelp input { margin-bottom: 5px; }
.edit #logohelp { display: block; }
.edit #save-logo, .edit #cancel-logo { display: inline; }
.edit #image, #save-logo, #cancel-logo, .edit #change-logo, .edit #delete-logo { display: none; }
#customer-title { font-size: 20px; font-weight: bold; float: left; }

#meta { margin-top: 1px; width: 300px; float: right; }
#meta td { text-align: right;  }
#meta td.meta-head { text-align: left; background: #eee; }
#meta td textarea { width: 100%; height: 20px; text-align: right; }

#items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid black; }
#items th { background: #eee; }
#items textarea { width: 80px; height: 50px; }
#items tr.item-row td { border: 0; vertical-align: top; }
#items td.description { width: 300px; }
#items td.item-name { width: 175px; }
#items td.description textarea, #items td.item-name textarea { width: 100%; }
#items td.total-line { border-right: 0; text-align: right; }
#items td.total-value { border-left: 0; padding: 10px; }
#items td.total-value textarea { height: 20px; background: none; }
#items td.balance { background: #eee; }
#items td.blank { border: 0; }

#terms { text-align: center; margin: 20px 0 0 0; }
#terms h5 { text-transform: uppercase; font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
#terms textarea { width: 100%; text-align: center;}

textarea:hover, textarea:focus, #items td.total-value textarea:hover, #items td.total-value textarea:focus, .delete:hover { background-color:#EEFF88; }




    </style>
    
</head>

<body>

    <div id="page-wrap">

        <textarea id="header">QUATATION</textarea>
        
        <div id="identity">
        
            <textarea id="address">Iconiq Diamond Jewelery
        <p>{{$settings['address']}}</p>
        <p>{{$settings['phoneNumber']}}</p>
        </textarea>

            <div id="logo">
                <img id="image" src="{{URL::to('frontend/assets/images/Iconiq.png')}}" width="300px" height="80px" alt="logo" />
            </div>
        
        </div>
        
        <div style="clear:both"></div>
        
        <div id="customer">

            <textarea id="customer-title"><p>{{$clientdetails['firstName'] . ' ' . $clientdetails['lastName']}}</p>
            <p>{{$clientdetails['address']}}</p></textarea>

            <table id="meta">
                
                <tr>

                    <td class="meta-head">Date</td>
                    <td><textarea id="date">{{Carbon\Carbon::now()->format('d/m/Y')}}</textarea></td>
                </tr>
                

            </table>
        
        </div>
        
        <table id="items">
        
          <tr>
              <th>Item</th>
              <th>Description</th>
              <th>Unit Cost</th>
              <th>Quantity</th>
              <th>Price</th>
          </tr>
          <?php $ordertotal = 0; ?>
                                     
        @foreach($pdfdata as $row)
          <tr class="item-row">
              <td class="item-name"><div class="delete-wpr"><textarea><img src="{{URL::to('public/'.$row['productImage'])}}" width="50" height="50" /></textarea></div></td>
              <td class="description"><textarea>
              {{$row['productItemNumber']}}
              Gold Wgt:{{$row['goldWeight']}}
              Diamond Wgt:{{$row['diamondWeight']}}
              </textarea></td>
              <td><textarea class="cost">{{$row['price']}}</textarea></td>
              <td><textarea class="qty">{{$row['qty']}}</textarea></td>
              <td><span class="price">{{$row['subtotal']}}</span></td>
          </tr>
          <div style="display: none;"> {{$ordertotal += $row['qty'] * $row['subtotal']}}</div>
          @endforeach
          <tr>

              <td colspan="2" class="blank"> </td>
              <td colspan="2" class="total-line">Total</td>
              <td class="total-value"><div id="total">{{$ordertotal}}</div></td>
          </tr>
        
        </table>
        
        <div id="terms">
          <h5>Terms</h5>
          <textarea>NET 30 Days. Finance Charge of 1.5% will be made on unpaid balances after 30 days.</textarea>
        </div>
    
    </div>
    
</body>

</html>