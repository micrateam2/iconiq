@extends('layouts.frontend')
@section('title')
ICONIQ 
@endsection

@section('content')

<div class="checkout">
  <div class="container">

   <!-- page title -->
   <div class="check-anchor clearfix mb40">
    <div class="holder">
      <ul>
        <li class="active"><a href="#"><i class="fa fa-star"></i> Shopping Cart <i class="fa fa-star"></i></a></li>
      </ul>
      <div class="holder-border"></div>
    </div>
  </div>

  <!-- cart -->
  <div class="check-infos">
    <div class="row">    
     {{  Form::open(array('url'=>'/storecart' , 'method' =>'POST','files'=>'true'))}}   


     <!-- cart products -->
     <div class="col-md-8">
      <div class="check-details">
       <div>
         <input type="number" name="goldRate" id="goldRate" class="goldRate" min="1" placeholder="    Gold Rate">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <input type="number" name="diamondRate" id="diamondRate" class="diamondRate" min="1" placeholder="   Diamond Rate">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <input type="number" name="labourRate" id="labourRate" class="labourRate" min="1" placeholder="   Labour Rate">
       </div><br>

       <table class="table">
        <thead>
          <tr>
            <th></th>
            <th>Details</th>
            <th>Gold Weight</th>
            <th>Diamond Weight</th>
            <th>Quantity</th>
            <th>Subtotal</th>
            <th></th>
          </tr>
        </thead>

        <tbody class="check-body">

         @if(empty($data))
         <tr>
          <td colspan="6" align="center">
            No Items Found In Your Cart
          </td>
        </tr>
        @else
        <div style="display: none;">{{$sum=0}} </div>
        @foreach($data as $key =>$row)

        <tr>
          <td><a href="{{URL::to('single_product/'.$row->id)}}"><img src="{{URL::to('public/'.$row->productImage)}}" alt="" width="30px" height="30px"></td>
          <td>
            <input type="hidden" name="productId[]" class="productId" data-id="{{$row->id}}" value="{{$row->id}}" >
            <a href="{{URL::to('single_product/'.$row->id)}}"><h6>{{str_limit($row->productItemNumber,20)}}</h6></a>


          </td>
          <td>{{$row->goldWeight}}                         
            <input type="hidden" placeholder="1" name="goldWeight[]" class="goldWeight" data-id="{{$row->id}}"  id="goldWeight-{{$row->id}}" value="{{$row->goldWeight}}" >
          </td>
          <td>{{$row->diamondWeight}}                         
            <input type="hidden" placeholder="1" name="diamondWeight[]" class="diamondWeight" data-id="{{$row->id}}"  id="diamondWeight-{{$row->id}}" value="{{$row->diamondWeight}}" >
          </td>
          <td>
            <input type="button" onclick="decrementValue({{$row->id}})" data-id="{{$row->id}}" class="minqty" value="-"  />

            <input type="number" style="width: 50px; text-align: center;" placeholder="1" min="0" data-id="{{$row->id}}" name="qty[]" id="qty-{{$row->id}}" class="qty" value="0" readonly="true" />

            <input type="button" onclick="incrementValue({{$row->id}})" class="plusqty" data-id="{{$row->id}}" value="+" />

          </td>
          <input type="hidden" name="subtotal[]" value="{{$row->totalAmount}}" data-id="{{$row->id}}">
          <td>₹&nbsp<span class="sub-total" id='subtotal-{{$row->id}}'>0</span></td>
          <td><a href="{{URL::to('/re/'.$key)}}" class="remove" data-id="{{$key}}"><img src="{{URL::to('frontend/assets/images/delete.png')}}" alt=""></a>
          </td>
        </tr>

        <div style="display: none;">{{$sum=$sum+$row->qty*$row->totalAmount}} </div>                    
        @endforeach                    
        @endif

      </tbody>
    </table>

    <!-- cart footer -->
    <div class="coupon">
      <a href="{{URL::to('/')}}" class="update">Add more items</a>
    </div>

  </div>
</div> <!-- ./ cart products -->

<!-- cart calc -->
<div class="col-md-4">
  <div class="check-aside">

   <!-- counts -->
   <div class="orders mb20">
    <h6>Cart totals</h6>
    <p>Order Total:
     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp₹<span id="total">
     @if(empty($data))
     0
     @else
     {{number_format($sum,0,'.',',')}}
   </span>
   <input type="hidden" name="totalAmount" class="totalAmount" readonly="true" id="totalAmount" value="{{$sum}}">
   @endif
 </p>
           <!-- <p>Shipping and Handling: <span>$ 250</span></p>
           <p><strong>Order Total: 
           <span id="">
              @if(empty($data))
              ₹ 0
              @else
              ₹ {{number_format($sum,0,'.',',')}}
              </span>
              <!-- <input type="hidden" name="totalAmount" class="totalAmount" readonly="true" id="totalAmount" value="{{$sum}}"> -->
              <!-- @endif -->          
              <!-- </strong></p> --> 
            </div>
         
            <!-- shippings -->
            <div class="shipping" >
              <input type="submit" value="Get Quatation" id="getquote">
            </div> <!-- ./ shippings -->
          
            {{Form::close()}} 
          </div>
        </div> <!-- ./ cart calc -->

      </div>
    </div> <!-- ./ cart -->

  </div>
</div>


<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery-ui.js')}}"></script>



<script type="text/javascript">  

$(document).ready(function() {
  $('#getquote').prop('disabled', true);
//      $(':input[type="submit"]').prop('disabled', false );
     
//         if($(.totalAmount).val() == 0 | '') {
//            $(':input[type="submit"]').prop('disabled', true);
//         }
 
 });

 function incrementValue(productId){
    // get quantity
    var qty= parseInt($('#qty-'+productId).val()) + 1;

    var goldWeight=parseFloat($('#goldWeight-'+productId).val());
    var goldRate=parseFloat($('#goldRate').val());
    var diamondRate =parseFloat($('#diamondRate').val());
    var diamondWeight =parseFloat($('#diamondWeight-'+productId).val());
    var labourRate =parseFloat($('#labourRate').val());

    if(goldRate>0){
     var gold = goldWeight * goldRate;
   }else{
    var gold = 0;
  }
  if(diamondRate>0){
   var diamond =diamondRate * diamondWeight;
 }else{
  var diamond=0;
}
var weight = goldWeight + diamondWeight;
if(labourRate>0){
 var totallabour = weight * labourRate;
}else{
  var totallabour = 0;
}
var total = totallabour + gold + diamond;



    // set incremented quantity
    $('#qty-'+productId).val(qty);
    
    var totalrate= qty * total ;
    var t =totalrate.toFixed(2)
    var subTotal =t;

    $('#subtotal-' + productId).text(subTotal);

    var stotal=$('#subtotal-' + productId).text();
    // alert(stotal);
    //   if(qty==0){
    //   // alert("hello");
    //         $(':input[type="submit"]').prop('disabled', true );
    //       }
    //       else{
    //         // alert("hi");
    //         $(':input[type="submit"]').prop('disabled', false );
    //       }

    qtyset(productId, qty);
    cartPriceUpdate();   
    
  } 

  function decrementValue(productId){

    // get quantity
     var qty=parseFloat($('#qty-'+productId).val());
    if(qty>0){

       var qty= parseInt($('#qty-'+productId).val()) - 1;
     
    }else{
      
        var qty=0;
    
    }

    var goldWeight=parseFloat($('#goldWeight-'+productId).val());
    var goldRate=parseFloat($('#goldRate').val());
    var diamondRate =parseFloat($('#diamondRate').val());
    var diamondWeight =parseFloat($('#diamondWeight-'+productId).val());
    var labourRate =parseFloat($('#labourRate').val());

        if(goldRate>0){
         var gold = goldWeight * goldRate;
       }else{
        var gold = 0;
      }
      if(diamondRate>0){
       var diamond =diamondRate * diamondWeight;
     }else{
      var diamond=0;
    }
    var weight = goldWeight + diamondWeight;
    if(labourRate>0){
     var totallabour = weight * labourRate;
    }else{
      var totallabour = 0;
    }
    var total = totallabour + gold + diamond;


    // set Decrementquantity


    $('#qty-'+productId).val(qty);
    
      // $('#qty-'+productId).val(1);

      var totalrate= qty * total ;
      var t =totalrate.toFixed(2)
      var subTotal =t;


      $('#subtotal-' + productId).text(subTotal);
          qtyset(productId, qty);
      cartPriceUpdate();   

    } 
  //for get total price of cart
  function cartPriceUpdate(){
    var totalPrice = 0;

    $('span.sub-total').each(function() {
     
      totalPrice += parseInt($(this).text());
     
       if(totalPrice==0){
      // alert("hello");
            $(':input[type="submit"]').prop('disabled', true );
          }
          else{
            // alert("hi");
            $(':input[type="submit"]').prop('disabled', false );
          }
      // alert(parseInt($(this).text()));
    });

    $('#total').text(totalPrice);

  }

  function qtyset(productId, qty){

    // var a1 =$('#qty-'+productId).val(qty);
    var qty= parseInt($('#qty-'+productId).val());


    $.ajax({
      url: "{{URL::to('/getqty')}}",
      dataType: 'json',
      contentType: 'application/json',

      data: JSON.stringify( { "qty": qty, "productId": productId , "_token": '{{ csrf_token() }}'} ),
      type: "POST",
      success: function (result) {


      }
    });  
  }

</script>
@endsection