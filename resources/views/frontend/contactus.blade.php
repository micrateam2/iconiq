@extends('layouts.frontend')
@section('title')
ICONIQ

@endsection

@section('content')

<div class="contact-content">

    <!-- google map -->
    <div class="contact-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3719.5236944983203!2d72.84776391543731!3d21.211072185899955!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04efd0d1def8b%3A0xbfc1ae3b7ef3d53b!2sIconiq+Jewellery!5e0!3m2!1sen!2sin!4v1504159934209" width="1920" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
   </div>


   <!-- contact form -->
   <div class="contact">
    <div class="container">

      <!-- contact form title -->
      <div class="title">
        <h1><i class="fa fa-star"></i> Get in Touch <i class="fa fa-star"></i></h1>
        <div class="title-border"></div>
    </div>
    <p class="subtitle">But unfortunately for most of us our role as gardener has never been explained to us. And in misunderstanding our role, we have allowed seeds of all types, both good and bad, to enter our inner garden.</p>


    <div class="row">

     <!-- form -->
     <div class="col-md-8">
        <div class="row">
            <div class="col-md-12" >
                @if(Session::has('message'))
                {!! Session::get('message') !!}
                @endif
            </div>
        </div>
        <h2>Contact Us</h2>
        <div class="col-md-12" id='errormessage'></div>
        {{  Form::open(array('url'=>'contactus/store','method'=>'POST','class'=>'form-horizontal','id'=>'contact-form','files'=>'true'))}}
        <ul style="display:inline-block;">
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        {{ Form::text('firstName','',array('class'=>'message','data-required'=>'1','id'=>'firstName','placeholder'=>'Firstname*'))}}
        
        {{ Form::text('lastName','',array('class'=>'message','data-required'=>'1','id'=>'lastName','placeholder'=>'Lastname*'))}}


        {{ Form::text('email','',array('class'=>'message','data-required'=>'1','id'=>'email','placeholder'=>'Email*'))}}

        {{ Form::text('phoneNumber','',array('class'=>'message','data-required'=>'1','id'=>'phoneNumber','placeholder'=>'Phone Number*'))}}

        {{ Form::text('subject','',array('class'=>'message','data-required'=>'1' ,'id'=>'subject','placeholder'=>'Subject*'))}}

        {{Form::textarea('message','', array('class' => 'name','id'=>'message','placeholder'=>'Message*'))}}

        {{ Form::submit('submit',array('class'=>'medium-button button-red','id'=>'submit' )) }}


        {{ Form::close()}}
        <div id="msg" class="message"></div>

    </div>

    <!-- contact infos -->
    <div class="col-md-4 touch">

        <h2>INFORMATION</h2>
        <p><i class="fa fa-map-marker"></i> {{$contact->address}}</p>
        @if($contact->officeNumber !="")
        <p><i class="fa fa-home"></i> +91 {{$contact->officeNumber}}</p>
        
        @endif
        <p><i class="fa fa-phone"></i> +91 {{$contact->phoneNumber}}</p>
        <a href="#">
            <p style="color: #ea5748;" class="mb40"><i class="fa fa-envelope"></i> {{$contact->email}}</p>
        </a>

                                <!-- <h2>Business hours</h2>
                                <p class="mb50">Monday – Friday: 9am to 20 pm
                                    <br> Saturday: 9am to 17 pm
                                    <br> Sunday: day off</p>

                                <h2>Stay Social</h2>
                                <ul>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                                </ul>
                            -->
                        </div>
                    </div>
                </div>

            </div> <!-- ./ contact form -->

        </div>
        <!-- page level Script -->
        <script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery-ui.js')}}"></script>

        <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
        <script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.nicescroll.min.js')}}"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg4pyLP5u8IchhmIiT_CnJB2K-VPXCWGg&callback=initializeContact" async defer></script>
        <script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.isotope.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('frontend/assets/js/masonry.pkgd.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.bxslider.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.carouFredSel.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('frontend/assets/js/smoothproducts.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('frontend/assets/js/slippry.min.js')}}"></script>
        <script type="text/javascript" src="{{URL::to('frontend/assets/js/script.js')}}"></script>
        <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.js"></script>
        <script>
            $(document).ready(function(){

                $("#contact-form").submit(function(){

                    var firstName = $("#firstName").val();
                    var lastName = $("#lastName").val();
                    var email = $("#email").val();
                    var phoneNumber = $("#phoneNumber").val();
                    var subject = $("#subject").val();
                    var message = $("#message").val();
                    var error = $("#errormessage");

                    var mobile_filter = /^[1-9]{1}[0-9]{9}$/;  
                    var email_filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

                    if(firstName == "" && lastName == "" && email == "" && phoneNumber == "" && subject == "" && message == "")
                    {
                        error.text("Please Entre All Fields");
                        return false;
                    }
                    else if(firstName == "")
                    {
                        error.text( "Please Enter Firstname." );
                        return false;
                    }
                    else if(lastName == "")
                    {
                        error.text( "Please Enter LastName." );
                        return false;
                    }
                    else if(email == "")
                    {
                        error.text( "Please Enter Email Address." );
                        return false;
                    }
                    else if(!email_filter.test(email))
                    {
                        error.text( "Please Enter valid Email" );
                        return false;
                    }
                    else if(phoneNumber == "")
                    {
                        error.text( "Please Enter Phone Number." );
                        return false;
                    }
                    else if(!mobile_filter.test(phoneNumber))
                    {
                        error.text( "Please Enter valid  Phone Number" );
                        return false;
                    }
                    else if(subject== ""){
                        error.text("Please Enter Subject Name.");
                    }
                    else if(message== ""){
                        error.text("Please Enter Message");
                    }

                    


                });


            });
        </script>

        @endsection
