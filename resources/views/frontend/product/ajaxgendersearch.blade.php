
@if(count($product) <= 0)

<div class="alert "><strong style="margin-left: 300px;"> &nbsp;No Product Found.!! </strong></div>

@else 
  @foreach($product as $row)
               <!-- product -->
               
               <li class="class{{$row->subCategoryId}}">
                <div class="arrival-overlay">
                    @if(!empty($row->productImage) && file_exists('public/'.$row->productImage))
                    <?php $imagezUrl = URL::to('public/'.$row->productImage) ?>
                    @else                                  
                    <?php $imagezUrl = URL::to('public/default_image/default_product.jpg') ?>
                    @endif
                    <img src="{{ $imagezUrl }}" width="270" height="320"/> 

                    <div class="arrival-mask">
                    <a href="#" data-id="{{$row->id}}" class="medium-button button-red add-cart cartid" onclick="cartsend({{$row->id}})">Add to Cart</a>
                        <!-- <a href="#" class="medium-button button-red add-cart">Add to Cart</a> -->
                    </div>
                </div>
                <div class="arr-content">
                    <a href="{{URL::to('single_product/'.$row->id)}}">
                    <p class="low-price">{{substr(' '.$row->productItemNumber, 0, 25)}}</p>
                    <p><strong style="color:#DF7D15";>GWt:</strong>{{' '.round($row->goldWeight,2).'  GM'}}||<strong style="color:#DF7D15";>DWt:</strong>{{' '.round($row->diamondWeight,2).'  GM'}}</p>
                        <!-- <p>{{substr($row->productName, 0, 25)}}</p> -->
                    </a>                    
                    <ul>
                       <!--  <li><span class="low-price"><?php //echo '₹'.round($row->totalAmount,0).'/-'?></span></li> -->
                    </ul>
                </div>
            </li>
        
            @endforeach  
            @endif
      