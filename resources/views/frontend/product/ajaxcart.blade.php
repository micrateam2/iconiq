<li class="carthead">YOUR SHOPPING CART</li>
@if(empty($data))
<li class="clear cartp"> No Items Found In Your Cart</li>
@else
@foreach($data as $key =>$row)
								<li class="clear cartp">
									<div class="pull-left">
										<img src="{{URL::to('public/'.$row->productImage)}}" alt="prod_name">
										<p>
											<strong class="pname">{{substr($row->productItemNumber, 0, 25)}}</strong>
											<span>QTY: <b>
											{{$row->qty}}
											</b></span>
										</p>
									</div>
									<div class="pull-right">
										<a href="#" class="remove" data-id="{{$key}}"><i class="fa fa-times"></i></a>
									</div>
								</li>
								
@endforeach
								<li class="cart-chk-action clear">
									<a href="{{URL::to('productcart')}}" class="medium-button button-red pull-left">checkout</a>
									<!-- <p class="pull-right"><strong>TOTAL</strong> $280</p> -->
								</li>
							</ul>
						</li>

@endif

