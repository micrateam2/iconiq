@extends('layouts.frontend')
@section('title')
ICONIQ 
@endsection
@section('content')
<!-- product data -->   
<div class="product-page container">
    <div class="row">

        <div class="col-md-6">
            <div class="single-img">
                <div class="sp-wrap">
                   @if(empty($product_image))

                   <div class="alert "><strong><center> &nbsp;No Image Found.!! </center></strong></div>
                   @else
                   @foreach($product_image as $row)

                   <a href="{{URL::to('public/'.$row->productImageName)}}"><img src="{{url::to('public/'.$row->productImageName)}}" alt=""></a>

                   @endforeach
                   @endif
               </div>
               <div id="test"></div>
           </div>
       </div>

       <!-- product actions -->
       <div class="col-md-6">
        <div class="single-desc">

            <!-- product tags -->
            <!-- product small data -->

            <div class="middle-single">
                <h1>{{substr(' '.$single_product->productItemNumber, 0, 25)}}</h1>
                <div class="reviews">
                   <h4></h4>
               </div>
               <div class="clear"></div>
           </div>

           <!-- product price -->
          <!--  <div class="single-price">
            <ul>              
               <li>Total Amount:<span class="low-price">₹ <?php //echo round($single_product->totalAmount,0)?>/-</span></li>
           </ul> 
       </div> -->

       <!-- product decs smalll data -->
       <div class="single-infos">
        <p><span style="color:#DF7D15";>Gold Quality:</span> {{$single_product['goldQuality'].'  K' }}</p>
        <!-- <p><span>Availability:</span>  {{ $single_product['isActive'] =="true"? "In Stock" : "Out Of Stock"}}</p> -->
        <p><span style="color:#DF7D15";>Diamond Shape:</span> {{$single_product['diamondShape'] }}</p>
        <p><span style="color:#DF7D15";>Gross Weight:</span> {{ $single_product['goldWeight'] + $single_product['diamondWeight']}} CT</p>
        <p><span style="color:#DF7D15";>Net Weight:</span> {{$single_product['goldWeight'] + $single_product['diamondWeight'] + $single_product['stoneWeight']}}</p>
    </div>

    <!-- <li class="dl-head"><strong>General</strong></li> -->


    <!-- customize purchase section -->
    <!-- product action -->
    <div class="prod-end">
        <a href="#"
        data-id="{{$single_product->id}}" class="medium-button button-red add-cart cartid" onclick="cartsend({{$single_product->id}})">Add to Cart</a>
        <input type="text" placeholder="1">
        <div class="clear"></div>
    </div>

    <!-- product single description -->


    <!-- product share -->
</div>
</div> <!-- ./ product actions -->

</div>
</div> <!-- ./ product data -->


<!-- product tabbed description -->
<div class="tabs-single">
    <div class="container">

        <div id="tabs">

            <!-- tab nav -->
            <ul>
                <li class="active" ><a href="#tabs-1">ADDITIONAL INFORMATION</a></li>
                <li ><a href="#tabs-2">Description</a></li>
                <!--  <li><a href="#tabs-3">REVIEWS</a></li> -->
            </ul>
            <div class="tab-border"></div>

            <div id="tabs-2">
                <p>We possess within us two minds. So far I have written only of the conscious mind.. Our subconscious mind contains such power and complexity that it literally staggers the imagination. We know that this subconscious mind controls and orchestrates our bodily functions, from pumping blood to all parts of our body. We possess within us two minds. So far I have written only of the conscious mind.. Our subconscious mind contains such power and complexity that it literally staggers the imagination. We know that this subconscious mind controls and orchestrates our bodily functions, from pumping blood to all parts of our body.</p>
            </div>
            <div id="tabs-1">
                <ul class="desc-list">
                    <!-- <li class="dl-head"><strong>General</strong></li> -->
                    <!-- <li class="dl-info">
                        <strong>Diamond Shape:</strong>
                        <p>{{$single_product['diamondShape']}}</p>
                    </li> -->
                    <li class="dl-info">
                        <strong>Diamond QualitySize:</strong>
                        <p>{{$single_product['diamondQualitySize'].'  K'}}</p>
                    </li>
                    <li class="dl-info">
                        <strong>Diamond Pisces:</strong>
                        <p>{{$single_product['diamondPisces']}}</p>
                    </li>
                    <li class="dl-info">
                        <strong>Diamond Weight:</strong>
                        <p>{{$single_product['diamondWeight'].'  CT'}} </p>
                    </li>
                    <!-- <li class="dl-info">
                        <strong>Gold Quality</strong>
                        <p>{{$single_product['goldQuality']}}</p>
                    </li> -->
                    <li class="dl-info">
                        <strong>Gold Weight:</strong>
                        <p>{{$single_product['goldQuality'].'  K'}}</p>
                    </li>
                    <li class="dl-info">
                        <strong>Stone Shape:</strong>
                        <p>{{$single_product['stoneShape ']}}</p>
                    </li>
                    <li class="dl-info">
                        <strong>Stone Quality:</strong>
                        <p>{{$single_product['stoneQuality'].'  K'}}</p>
                    </li>
                    <li class="dl-info">
                        <strong>Stone Size:</strong>
                        <p>{{$single_product['stoneSize'].' mm'}}</p>
                    </li>
                    <li class="dl-info">
                        <strong>Stone Pisces:</strong>
                        <p>{{$single_product['stonePisces']}}</p>
                    </li>
                    <li class="dl-info">
                        <strong>Stone Weight:</strong>
                        <p>{{$single_product['stoneWeight'].'  GM'}}</p>
                    </li>
                </ul>

            </div>


        </div>

    </div>
</div> <!-- ./ product tabbed description -->


<!-- related items -->
<div class="feat-items">
    <div class="container">

        <!-- heading -->
        <h1>RELATED ITEMS</h1>

        <div class="list_carousel1 responsive">
            <ul id="relslider">

                <!-- product -->
                @if(empty($related_product))

                <div class="alert "><strong><center> &nbsp;No Related Item Found.!! </center></strong></div>
                @else   
                @foreach($related_product as $row)
                <li>
                   <div class="arrival-overlay">
                    @if(!empty($row->productImage) && file_exists('public/'.$row->productImage))
                    <?php $imagezUrl = URL::to('public/'.$row->productImage) ?>
                    @else                                  
                    <?php $imagezUrl = URL::to('public/default_image/default_product.jpg') ?>
                    @endif
                    <img src="{{ $imagezUrl }}" width="270" height="320"/> 

                    <div class="arrival-mask">
                        <a href="#" data-id="{{$row->id}}" class="medium-button button-red add-cart cartre" onclick="cartsendre({{$row->id}})">Add to Cart</a>
                        
                    </div>
                </div>
               <div class="arr-content">
                    <a href="{{URL::to('single_product/'.$row->id)}}">
                    <p class="low-price">{{substr(' '.$row->productItemNumber, 0, 25)}}</p>
                    <p><strong style="color:#DF7D15";>Gold Wt:</strong>{{' '.$row->goldWeight.'  GM'}}</p>
                    <p><strong style="color:#DF7D15";>Diamond Wt:</strong>{{' '.$row->diamondWeight.'  GM'}}</p>
                        <!-- <p>{{substr($row->productName, 0, 25)}}</p> -->
                    </a>                    
                    <ul>
                       <!--  <li><span class="low-price"><?php //echo '₹'.round($row->totalAmount,0).'/-'?></span></li> -->
                    </ul>
                </div>
            </li>
            @endforeach
            @endif
            <!-- product -->
            <li></li>

            <li></li>
        </ul>

        <div class="clearfix"></div>

        <!-- releted ites slide nav -->
        <div class="arrows">
            <a id="prev1" class="prev1" href="#"><i class="fa fa-angle-left"></i></a>
            <a id="next1" class="next1" href="#"><i class="fa fa-angle-right"></i></a>
        </div>

    </div>

</div>
</div> <!-- ./ related items -->

<!-- scripts -->
<script>

    <!-- related slider -->
    function relatedSlide(){
        $('#relslider').carouFredSel({
            responsive: true,
            width: '100%',
            scroll: 1,
            auto:false,
            prev: '#prev1',
            next: '#next1',
            items: {
                width: 300,
                height: 430, 
                visible: {
                    min: 1,
                    max: 4
                }
            }
        });
    }

    <!-- tabs -->
    $("#tabs").tabs();

    <!-- product showcase -->
    $(window).load(function() {
        $('.sp-wrap').smoothproducts();
        relatedSlide();
    });

</script>


<script>
    function cartsend(id){
        var id=$('.cartid').data('id');
        
        $.ajax({
            url: "{{url::to('/cart')}}",
            data: { 
                _token: "{{ csrf_token() }}",
                'id' : id
            },
            type: "post",
            cache: false,
            success: function (result) {
                // console.log(result); 
                if(result.status){
                   $("#cartproduct").html(result.value);

               } 
               else{
                // alert(result.value);
            }   
        }  
    });
    }

// related item cart send

    function cartsendre(id){
        

        $.ajax({
            url: "{{url::to('/cart')}}",
            data: { 
                _token: "{{ csrf_token() }}",
                'id' : id
            },
            type: "post",
            cache: false,
            success: function (result) {
                // console.log(result); 
                if(result.status){
                   $("#cartproduct").html(result.value);

               } 
               else{
                // alert(result.value);
            }   
        }  
    });
    }
</script>
<!-- page lavel Script -->
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery-ui.js')}}"></script>
<!-- 
<script type="text/javascript" src="{{URL::to('frontend/assets/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.nicescroll.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.isotope.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/masonry.pkgd.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.bxslider.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.carouFredSel.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/smoothproducts.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/slippry.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/script.js')}}"></script>
-->
@endsection

