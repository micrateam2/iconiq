@extends('layouts.frontend')
@section('title')
    ICONIQ
@endsection

@section('content')

    <div class="shop-main container">
        <div class="row">

            <!-- filters -->
            <div class="col-md-3">
                <aside class="left-shop">


                    <!-- categories -->
                    <div class="shop-categories mb30">
                        <h1 class="asidetitle">Categories</h1>
                        <ul>
                            @foreach($subcategory as $row1)
                                <li><a href="{{URL::to('products/'.$row1->id)}}">{{$row1->subCategoryName}}</a></li>
                            @endforeach
                            <li><a href="{{URL::to('products/'.$newArrival->id)}}">{{$newArrival->subCategoryName}}</a></li>
                        </ul>
                    </div>

                {{  Form::open(array('url'=>'' , 'method' =>'POST','class'=>'form-horizontal','id'=>'ajax_data','files'=>'true'))}}

                <!-- price -->
                    <!--    <div class="shop-price mb30">
                           <h1 class="asidetitle">Price</h1>
                           <p><!-- <input type="text" id="amount"> -->


                    <!--
                              </p>
                              <div id="slider-range">

                              </div>  -->
                {{Form::hidden('id', $id)}}
                <!--      </div> -->


                    <!-- colors -->
                    <div class="brands mb30">
                        <h1 class="asidetitle">Gender</h1>

                        <ul>
                            <li>
                                {{Form::checkbox('gender[]', 'female','' , array('class' => '','id'=>'women','onclick'=>'search_ajax()'))}}
                                Women <span></span>
                            </li>
                            <li>{{Form::checkbox('gender[]', 'male','' , array('class' =>'','id'=>'women','onclick'=>'search_ajax()'))}}
                                Male <span></span>
                            </li>
                        </ul>

                    </div>
                    <!-- sizes -->
                    <div class="brands mb30">

                        <h1 class="asidetitle">Gold Caret</h1>

                        <ul>
                            <li>
                                {{Form::checkbox('goldQuality[]', '16','' , array('class' =>'','id'=>'16','onclick'=>'search_ajax()'))}}
                                16K <span></span>
                            </li>
                            <li>
                                {{Form::checkbox('goldQuality[]', '18','' , array('class' =>'','id'=>'18','onclick'=>'search_ajax()'))}}
                                18K <span></span>
                            </li>
                            <li>
                                {{Form::checkbox('goldQuality[]', '20','' , array('class' =>'','id'=>'20','onclick'=>'search_ajax()'))}}
                                20K <span></span>
                            </li>
                            <li>
                                {{Form::checkbox('goldQuality[]', '22','' , array('class' =>'','id'=>'22','onclick'=>'search_ajax()'))}}
                                22K <span></span>
                            </li>
                            <li>
                                {{Form::checkbox('goldQuality[]', '24','' , array('class' =>'','id'=>'24','onclick'=>'search_ajax()'))}}
                                24K <span></span>
                            </li>

                        </ul>

                    </div>

                    {{ Form::close()}}


                </aside>
            </div> <!-- ./ filters -->
            <!-- products -->
            <div class="col-md-9">
                <div class="shop-content">
                    <!-- toolbar -->
                    <div class="toolbar">


                    {{  Form::open(array('url'=>'' , 'method' =>'POST','class'=>'form-horizontal','id'=>'sort_ajax','files'=>'true'))}}

                    <!-- shorting -->
                        <div class="sort-select">
                            <label>Sort by</label>

                            {{Form::select('sortBy[]', array(''=>'Popularity','new'=>'Newest First','desc' => 'High To Low','asc'=>'Low To High'), 'sortBy', array('class' => 'name','onchange'=>'order_ajax()'))}}


                        </div>

                        {{Form::hidden('id', $id)}}



                        {{ Form::close()}}

                    </div>

                </div> <!-- ./ toolbar -->
                <!-- product list -->
                <!-- <div class="arrivals"> -->
                <div class="container col-md-16">
                    <div class="filters col-md-16" style="width: 1090px">


                        <!-- products holder -->
                        <ul class="filter-container clearfix">
                            <div class="demo1 clearfix product" id="product">
                                @if(count($product) == 0)

                                    <div class="alert"><strong style="margin-left: 300px;"> &nbsp;No Product Found In This
                                            Category.!! </strong></div>
                            @else
                                @foreach($product as $row)
                                    <!-- product -->
                                        <li class="class{{$row->subCategoryId}}">
                                            <div class="arrival-overlay">
                                                @if(!empty($row->productImage) && file_exists('public/'.$row->productImage))
                                                    <?php $imagezUrl = URL::to('public/' . $row->productImage) ?>
                                                @else
                                                    <?php $imagezUrl = URL::to('public/default_image/default_product.jpg') ?>
                                                @endif
                                                <img src="{{ $imagezUrl }}" width="270" height="320"/>

                                                <div class="arrival-mask">
                                                    <a href="#" data-id="{{$row->id}}"
                                                       class="medium-button button-red add-cart cartid"
                                                       onclick="cartsend({{$row->id}})">Add to Cart</a>
                                                    <!-- <a href="#" class="medium-button button-red add-cart">Add to Cart</a> -->
                                                </div>
                                            </div>
                                            <div class="arr-content">
                                                <a href="{{URL::to('single_product/'.$row->id)}}">
                                                    <p class="low-price">{{substr(' '.$row->productItemNumber, 0, 25)}}</p>
                                                    <p><strong style="color:#DF7D15"
                                                               ;>GWt:</strong>{{' '.round($row->goldWeight,2).'  GM'}}||<strong
                                                                style="color:#DF7D15"
                                                                ;>DWt:</strong>{{' '.round($row->diamondWeight,2).'  CT'}}
                                                    </p>
                                                <!-- <p>{{substr($row->productName, 0, 25)}}</p> -->
                                                </a>
                                                <ul>
                                                <!--  <li><span class="low-price"><?php //echo '₹'.round($row->totalAmount,0).'/-'?></span></li> -->
                                                </ul>
                                            </div>
                                        </li>
                                        @endforeach
                                        @endif
                            </div> <!-- ./ products holder -->
                        </ul>

                    </div>
                </div>
                <!-- </div> -->


                <!-- ./ product list -->


                <!-- pagination -->
                <div class="shop-pag">

                    <!-- pg info -->
                    <!--  <p class="pag-p">Items <span>1 to 12</span> of 78 Total</p> -->

                    <div class="right-pag">

                        <!-- start pagination -->
                        {{ $product->links() }}

                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div> <!-- ./ pagination -->

        </div>
        <!-- ./ products -->


    </div>
    </div>


    <!-- scripts -->

    <!-- price range Script -->
    <script>

        //
        <!-- price range slider -->
        // $(function() {
        //     $("#slider-range").slider({
        //         range: true,
        //         min: 1000,
        //         max: 500000,
        //         values: [1000,500000],
        //         slide: function(event, ui) {
        //             $("#amount").val(ui.values[0]+"-"+ui.values[1]);
        //         },
        //         change: function(event, ui) {
        //             $("#amount").val(ui.values[0]+"-"+ui.values[1]);
        //             search_ajax();
        //         },
        //     });
        //     $("#amount").val($("#slider-range").slider("values",0)+"-"+$("#slider-range").slider("values",1));
        // });

    </script>

    <!-- filter ajax script -->
    <script>
        function search_ajax() {

            $.ajax({
                url: "{{URL::to('gendersearch')}}",
                data: $('#ajax_data').serialize(),
                type: "POST",
                success: function (result) {
                    console.log(result.status);
                    if (result.status) {

                        $(".product").html(result.value);
                    }

                }
            });

        }


    </script>
    <!-- order wise ajax script -->
    <script>
        function order_ajax() {
            $.ajax({
                url: "{{URL::to('sortsearch')}}",
                data: $('#sort_ajax').serialize(),
                type: "POST",
                success: function (result) {
                    // console.log(result);
                    // alert("ok");

                    if (result.status) {

                        // console.log('hoi');
                        // console.log('hoi');
                        $(".product").html(result.value);
                    }

                }
            });

        }


    </script>

    <!-- pagelabel scripts -->


    <!--Start Cart Script -->
    <script>
        function cartsend(id) {
            // var id=$('.cartid').data('id');
            // alert(id);
            $.ajax({
                url: "{{url::to('/cart')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    'id': id
                },
                type: "post",
                cache: false,
                success: function (result) {
                    console.log(result);
                    if (result.status) {
                        $("#cartproduct").html(result.value);
                    }
                    else {
                        // alert(result.value);
                    }
                }
            });
        }

    </script>
    <!-- End Cart Script -->
    @endsection