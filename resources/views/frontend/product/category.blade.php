@extends('layouts.frontend')
@section('title')
ICONIQ 
@endsection

@section('content')

<div class="pt0">
	<div class="shop-main container pt0">
		<div class="row">
			
			<!-- page title -->
			<div class="check-anchor clearfix">
				<div class="holder">
					<ul>
						<li class="active"><a href="#"><i class="fa fa-star"></i> Product Categories <i class="fa fa-star"></i></a></li>
					</ul>
					<div class="holder-border"></div>
				</div>
			</div>
			<div class="cat-masonry">
				@if(empty($subcategory))

				<div class="alert "><strong><center> &nbsp;No Data Found In This Page.!! </center></strong></div>
				@else	
				@foreach($subcategory as $row)	
				<div class="item">
					<img src="{{URL::to('public/'.$row->subCategoryImage)}}" alt="">
					<div class="cat-infos left-shadow">
						<div class="ci-data">
							<h3>{{$row->subCategoryName}}</h3>
							<a href="{{URL::to('products/'.$row->id)}}"> View All <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>
				@endforeach
				@endif
				<div class="clear"></div>
			</div> 
			
		
			
			
			
		</div>
	</div>
</div>
@endsection
