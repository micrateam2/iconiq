@extends('layouts.frontend')
@section('title')
    ICONIQ
@endsection
@section('content')
    <!-- form -->
    <div class="product-page container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6 col-centered">
                    <div class="row">


                        <h1 class="col-md-12"> Login </h1>

                        {{  Form::open(array('url'=>'frontend/client/login' , 'method' =>'patch','class'=>'form-horizontal','id'=>'login','files'=>'true'))}}
                        <div class="col-md-12">
                            <ul style="text-align:left; color:red; font-weight:bold;">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-md-12 form-group text-center">
                            {{ Form::text('email','',array('placeholder' => 'Email','class'=>'form-control','data-required'=>'1'))}}
                        </div>
                        <br/>
                        <div class="col-md-12 form-group">
                            {{Form::password('password', array('placeholder' => 'Password','id'=>'password','class'=>'form-control','data-required'=>'1'))}}
                        </div>
                        <br/>
                        <div class="col-md-12 form-group">
                            {{Form::submit('Login', array('class' => 'medium-button button-red col-md-6'))}}

                            <a href="{{URL::to('password/reset')}}" class="pull-left col-md-6">forget password?</a>

                        </div>
                        <div id="msg" class="message"></div>
                        {{ Form::close()}}
                    </div>

                    <div class="row"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- form  end-->
@endsection
