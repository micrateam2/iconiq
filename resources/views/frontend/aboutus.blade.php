@extends('layouts.frontend')
@section('title')
ICONIQ

@endsection

@section('content')
<!-- intro section -->
<div class="introduction">
    <div class="container">
       <div class="row">

          <!-- intro 1 -->
          <div class="col-md-12">
             <h1>About Us</h1>
             <div class="intro-content">
                <!-- <img src="{{URL::to('frontend/assets/images/intro.jpg')}}" alt=""> -->
                <h3>
                    Fashion jewelry is one of the fastest growing categories in the retail world. </h3>
                    
                    <p style="text-align: justify;">
                    At Iconiq, we believe, the finest of jewels need not come with the heftiest of price tags. Designed by exceptional artists and crafted by master craftsmen from around the world, Iconiq is all about affordable luxury, exclusive, standout pieces starring in a sumptuous and smooth shopping experience.  </p>
                    <br>
                    <p style="text-align: justify;">
                    Each piece of jewelry on iconiqjewellery.com comes with the Iconiq assurance of quality and durability. We know the demands on every woman’s time - we create our pieces with utmost care so that you don’t need to spend hours caring for it. A Iconiq jewel is like your best friend, your 3AM buddy, your BFF, someone who knows just what to do or say to bring that special glow on your face, make you feel fabulous inside and look great outside. Someone you can take for granted..</p>
                    <br>
                    <p style="text-align: justify;">
                        With a highly competent management team consisting of top business school, fashion and technology institute alumnus and e-retail company Iconiq, we aim to create a great lifestyle experience for our esteemed customers. We hope you enjoy shopping on our site but in case we ever disappoint you, please feel free to reach out to us at info@iconiqjewellery.com

                    </p>

                </div>
            </div>

            <!-- intro 2 -->
						<!-- <div class="col-md-6">
							<h1>introduction</h1>
							<div class="intro-content">
								<img src="{{URL::to('frontend/assets/images/intro.jpg')}}" alt="">
								<h4>10 years on the global market. We work since 1999.</h4>
								<p>We possess within us two minds. So far I have written only of the conscious mind. I would now like to introduce you to your second mind, the hidden and mysterious subconscious. Our subconscious mind contains such power and complexity that it literally staggers the imagination.</p>
							</div>
						</div> -->

					</div>
				</div>
			</div> <!-- ./ intro section -->
			
			<!-- our team -->
        <div class="">
            <div class="container">

              <!-- team title -->
              <div class="title">
                <h1><i class="fa fa-star"></i> Chairman's Message <i class="fa fa-star"></i></h1>
                <div class="title-border"></div>
            </div>
            <div class="">
               <p style="text-align: justify;">
                <span class="quotes_top">&nbsp;</span>
                  I am delighted to present our website showcasing our spectacular jewellery collection and connect you with our stores offline and online. Iconiq Diamong jewellery is one of the leading jewellery brands in India with an enduring commitment to purity and quality.  we have always prided ourselves on our ability to offer our customers superior quality products that give value for money. We have over 150+ showrooms worldwide, with a strong presence in both India and the Middle East, and are well on our way to achieving our aim of becoming the world's number one jewellery group.</p>
                  <br>
                 <p style="text-align: justify;">
                 Our extensive jewellery collection, impeccable after-sale service and world-class facilities along with uncompromising dedication to personal attention offer a remarkable service experience to our customers. We consider each sale as an everlasting relationship, and believe in keeping our customers always satisfied by protecting their rights and offering the finest products through our quality conscious efforts. Without you, our efforts wouldn't have been as successful as they are today. I thank you sincerely for all your support and wish you have a wonderful buying experience with us. 
            </p>
        </div>




    </div>
</div> <!-- ./ our team -->
			
			<!-- about page section -->
            <div class="about-content">

                <!-- services -->
                <div class="services">
                    <div class="container">

                      <!-- service title -->
                      <div class="title">
                        <h1><i class="fa fa-star"></i> Our Benefits <i class="fa fa-star"></i></h1>
                        <div class="title-border"></div>
                    </div>

                    <div class="row">

                     <!-- service -->
                     <div class="col-md-3">
                        <div class="serv-box">
                            <i class="fa fa-plane"></i>
                            <h4>FAST DELIVERY</h4>
                            <div class="serv-border"></div>
                            <p style="text-align: justify;">Iconiq is all about affordable luxury, exclusive, standout pieces starring in a sumptuous and smooth shopping experience.<br><br></p>
                        </div>
                    </div>

                    <!-- service -->
                    <div class="col-md-3">
                        <div class="serv-box">
                            <i class="fa fa-eye"></i>
                            <h4>Retine Ready</h4>
                            <div class="serv-border"></div>
                            <p style="text-align: justify;">
                            All Products Designed by exceptional artists and crafted by master craftsmen from around the world.<br><br></p>
                        </div>
                    </div>

                    <!-- service -->
                    <div class="col-md-3">
                        <div class="serv-box">
                            <i class="fa fa-comments-o"></i>
                            <h4>Quality Support</h4>
                            <div class="serv-border"></div>
                            <p style="text-align: justify;">
                            All products come with BIS Hallmarked 916 Gold, IGI Certified Diamond jewellery, PGI certified Platinum jewellery and hallmarked silver jewellery</p>
                        </div>
                    </div>

                    <!-- service -->
                    <div class="col-md-3">
                        <div class="serv-box">
                            <i class="fa fa-shield"></i>
                            <h4>100% Guarantee</h4>
                            <div class="serv-border"></div>
                            <p style="text-align: justify;">
                            Our value-added services include lifelong free maintenance, one-year free insurance coverage and buy-back guarantee for all ornaments.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div> <!-- ./ services -->


    <br><br>    


<!-- testimonials -->
<!-- <div class="testimonials3">
    <div class="container">
 -->
      <!-- testimonials title -->
      <!-- <div class="title">
        <h1><i class="fa fa-star"></i> Testimonials <i class="fa fa-star"></i></h1>
        <div class="title-border"></div>
    </div> -->

    <!-- testimonials slider -->
    <!-- <ul class="bxslider">

        <li>
            <p>Don't cry because it's over, smile because it happened. We are team of creative photographers. We passionate with photography and other creative things. If you are looking professional photography theme with endless possibilities, you come in right place. This template consist of well-organized layers. Tons of features waiting for you. </p>
            <span class="user">Astrit Bublaku / CEO at Kikirik</span>
        </li>

        <li>
            <p>Don't cry because it's over, smile because it happened. We are team of creative photographers. We passionate with photography and other creative things. If you are looking professional photography theme with endless possibilities, you come in right place. This template consist of well-organized layers. Tons of features waiting for you. .</p>
            <span class="user">Alexander Samokhin / CEO of Company</span>
        </li>
 -->
    <!-- </ul>  -->
    <!-- ./ testimonials slider -->

<!-- </div> -->
<!-- </div> ./ testimonials -->

</div> <!-- ./ about page section -->
@endsection