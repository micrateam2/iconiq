@extends('layouts.admin')
@section('title')
iconiq
@endsection
@section('pagelevel_css')
 <link href="{{URL::to('public/admin/assets/pages/css/error.min.css')}}" rel="stylesheet" type="text/css" />
 @endsection
@section('content')
 <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="{{url::to('/admin')}}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>System</span>
                                </li>
                            </ul>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> 404 Page 
                            
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="row">
                            <div class="col-md-12 page-404">
                                <div class="number font-green"> 404 </div>
                                <div class="details">
                                    <h3>Oops! You're lost.</h3>
                                    <p> We can not find the page you're looking for.
                                        <br/>
                                        <a href="{{url::to('/admin')}}"> Return Home<i class="icon-home"></i> </a></p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                @endsection
@section('pagelevel_plugins')
@endsection
@section('pagelevel_script')
@endsection