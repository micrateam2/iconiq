<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>IconiQ - Diamond Jewellery</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #1 for 404 page option 2" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,800,900,200,100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Noticia+Text:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,400,200,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />


    <link rel="stylesheet" type="text/css" href="{{URL::to('frontend/assets/css/bootstrap.css')}}" media="screen">

    <!-- font icon -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('frontend/assets/css/font-awesome.css')}}" media="screen">
    
    <!-- bx slider -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('frontend/assets/css/jquery.bxslider.css')}}" media="screen">
    
    <!-- slippry slider -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('frontend/assets/css/slippry.css')}}" media="screen">

    <!-- main stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('frontend/assets/css/style.css')}}" media="screen">
    
    <!-- responsive stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('frontend/assets/css/responsive.css')}}" media="screen">

    <!-- favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{URL::to('frontend/assets/images/favicons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" href="{{URL::to('frontend/assets/images/favicons/favicon-32x32.png')}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{URL::to('frontend/assets/images/favicons/favicon-16x16.png')}}" sizes="16x16">
    <link rel="shortcut icon" href="{{URL::to('frontend/assets/images/favicons/favicon.ico')}}"> 
    <link href="{{ URL::asset('public/admin/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('public/admin/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('public/admin/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ URL::asset('public/admin/assets/global/css/components-md.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ URL::asset('public/admin/assets/global/css/plugins-md.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ URL::asset('public/admin/assets/pages/css/error.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->
    @include('includes.frontend.header')
    <body class=" page-404-full-page">
        <div>
            <div class="row">
                <div class="col-md-12 page-404">
                    <div class="number font-red"> 404 </div>
                    <div class="details">
                        <h3>Oops! You're lost.</h3>
                        <p> We can not find the page you're looking for.
                            <br/>
                            <a href="{{URL::to('/')}}"> Return home </a> or try the search bar below. </p>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 page-404">
                    <div class="number font-red"></div>
                        <div class="details">
                           

                            </div>
                        </div>
                    </div>
                </div>

                @include('includes.frontend.footer')
      
<!-- BEGIN CORE PLUGINS -->

<script src="{{ URL::asset('public/admin/assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('public/admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('public/admin/assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('public/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{ URL::asset('public/admin/assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<!-- END THEME LAYOUT SCRIPTS -->
</body>


<!-- scripts -->
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.nicescroll.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.isotope.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/masonry.pkgd.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.bxslider.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.carouFredSel.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/smoothproducts.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/slippry.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('frontend/assets/js/script.js')}}"></script>
</html>