<!doctype html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- title -->
    <title>IconiQ - Diamond Jewellery</title>

    <!-- fonts & icons -->
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,800,900,200,100' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Noticia+Text:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,400,200,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('frontend/assets/css/bootstrap.css')}}" media="screen">

    <!-- font icon -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('frontend/assets/css/font-awesome.css')}}" media="screen">
    
    <!-- bx slider -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('frontend/assets/css/jquery.bxslider.css')}}" media="screen">
    
    <!-- slippry slider -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('frontend/assets/css/slippry.css')}}" media="screen">

    <!-- main stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('frontend/assets/css/style.css')}}" media="screen">
    
    <!-- responsive stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('frontend/assets/css/responsive.css')}}" media="screen">

    <!-- favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{URL::to('frontend/assets/images/favicons/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" href="{{URL::to('frontend/assets/images/favicons/favicon-32x32.png')}}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{URL::to('frontend/assets/images/favicons/favicon-16x16.png')}}" sizes="16x16">
    <link rel="shortcut icon" href="{{URL::to('frontend/assets/images/favicons/favicon.ico')}}">

</head>
<body>

    <!-- preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    
    
    <!-- header and slider container -->
    <div id="container">
        
	 @include('includes.frontend.header')


     </div>
     
    <!-- ./ header and slider container -->

	
	
	<!-- home page contents -->
      
    <div id="content">

	 @yield('content')

    </div> 	<!-- ./ home page contents -->
	
	
	
	
	<!-- footer -->
	 @include('includes.frontend.footer')
  <!-- ./ footer -->

	

	<!-- scripts -->
    
    <script type="text/javascript" src="{{URL::to('frontend/assets/js/bootstrap.js')}}"></script>
    
    <!-- <script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.nicescroll.min.js')}}"></script> -->
    <script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.isotope.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('frontend/assets/js/masonry.pkgd.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.bxslider.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('frontend/assets/js/jquery.carouFredSel.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('frontend/assets/js/smoothproducts.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('frontend/assets/js/slippry.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::to('frontend/assets/js/script.js')}}"></script>


    
</body>
</html>