@extends('layouts.admin')
@section('title')
iconiq
@endsection
@section('pagelevel_css')  <!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{ URL::to('public/admin/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::to('public/admin/assets/pages/css/invoice-2.min.css')}}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->       
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="{{URL::to('public/admin/assets/layouts/layout/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::to('public/admin/assets/layouts/layout/css/themes/darkblue.min.css')}}." rel="stylesheet" type="text/css" id="style_color" />
<link href="{{URL::to('public/admin/assets/layouts/layout/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" /> 
<!-- END HEAD -->
@endsection
@section('content') 
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->                        
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="#">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>General</span>
                </li>
            </ul>

        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Invoice 2
            <small>invoice sample</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="invoice-content-2 bordered">
            <div class="row invoice-head">
                <div class="col-md-7 col-xs-6">
                    <div class="invoice-logo">
                        <img src="{{URL::to('frontend/assets/images/Iconiq.jpg')}}" class="img-responsive" alt="" />
                        <h1 class="uppercase">Invoice</h1>
                    </div>
                </div>
                <div class="col-md-5 col-xs-6">
                    <div class="company-address">
                        <span class="bold uppercase">ICONIQ INC.</span>
                        <p>{{$contact->address}}</p>
                        <span class="bold">T</span> 
                        {{$contact->phoneNumber}}
                        <br/>
                        <span class="bold">E</span> {{$contact->email}}
                        <br/>
                        <span class="bold">W</span> www.iconiqjewellery.com </div>
                    </div>
                </div>
                <div class="row invoice-cust-add">
                    <div class="col-xs-4">
                        <h2 class="invoice-title uppercase">Client Name</h2>
                        <p class="invoice-desc">{{$client_inquiry->client->firstName . ' ' .$client_inquiry->client->lastName}}</p>
                    </div>
                    <div class="col-xs-4">
                        <h2 class="invoice-title uppercase">Date</h2>
                        <p class="invoice-desc">{{Carbon\Carbon::parse($client_inquiry->client->created_at)->format('d/m/Y')}}</p>
                    </div>
                    <div class="col-xs-4">
                        <h2 class="invoice-title uppercase">Phone Number</h2>
                        <p class="invoice-desc">{{$client_inquiry->client->phoneNumber}}</p>
                    </div>                                  
                </div>

                <div class="row invoice-body">
                    <div class="col-xs-12 table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>                                            
                                  <th class="invoice-title text-center">  Product Image</th>

                                  <th class="invoice-title text-center">Item No</th>                                  
                                  <th class="invoice-title text-center">Gold Weight</th>
                                  <th class="invoice-title text-center">Diamond Weight</th>
                                  <th class="invoice-title text-center">Quantity </th>
                                  <th class="invoice-title text-center">Amount</th>
                              </tr>
                          </thead>
                          <tbody>
                           <div style="display:none;">{{$sum=0}}</div> 
                            @foreach($product_inquiry as $row)
                            <tr>
                                <td><center><img src="{{URL::to('public/'.$row->productImage)}}" alt="" width="70px" height="60px"></center></td>

                                <td class="text-center sbold">{{$row->product->productItemNumber}}</td>
                                <td class="text-center sbold">{{$row->product->goldWeight}}</td>

                                <td class="text-center sbold">{{$row->product->diamondWeight}}</td>

                                <td class="text-center sbold">{{$row->productQuantity}}</td>

                                <td class="text-center sbold">{{$row->totalAmount * $row->productQuantity }}</td>
                            </tr>


                           <div style="display: none;"> {{$sum= ($row->totalAmount  * $row->productQuantity)+$sum}}
                           </div>
                        
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row invoice-subtotal">

            <div class="col-xs-12" align="right">
                <h2 class="invoice-title uppercase">Total</h2>
                <p class="invoice-desc grand-total"><?php echo $sum;?></p>
            </div>
        </div>

    </div>
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->
<a href="javascript:;" class="page-quick-sidebar-toggler">
    <i class="icon-login"></i>
</a>

<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->

</div>
<!-- BEGIN QUICK NAV -->

<div class="quick-nav-overlay"></div>

@endsection
@section('pagelevel_plugins')
<script src="{{ URL::to('public/admin/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('public/admin/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('public/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
@endsection
@section('pagelevel_script')