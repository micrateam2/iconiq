@extends('layouts.admin')
@section('title')
Iconiq-Product
@endsection
@section('pagelevel_css')
<link href="{{ URL::to('public/admin/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<div class="page-content-wrapper">
	<!--BEGIN CONTENT BODY-->
	<div class="page-content">

		<!-- BEGIN PAGE BAR -->
		<div class="page-bar">
			<ul class="page-breadcrumb">
				<li>
					<a href="index.html">Home</a>
					<i class="fa fa-circle"></i>
				</li>
				<li>
					<a href="#">Product</a>
				</li>
			</ul>

		</div>
		<!-- END PAGE BAR -->
		<!-- BEGIN PAGE TITLE-->
		<h1 class="page-title"> Product</h1>

		<!-- END PAGE TITLE-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN EXAMPLE TABLE PORTLET-->
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption font-dark">
							<span class="caption-subject bold uppercase"> Product Import Deatils </span>
						</div>
					</div>
					<div class="portlet-body">

						@if(count($wrongdata)==0)

						<h3>Product data import sucessfully..!</h3>
						<div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">

                                        <a href="{{ URL::to('admin/product/create')}}" class="btn sbold green"> Add Product
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
						@else		
						<h3>Please Check Data..!!</h3>
						<div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">

                                        <a href="{{ URL::to('admin/product/create')}}" class="btn sbold green"> Add Product
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
						<table class="table table-striped table-bordered table-hover table-checkable order-column" >
							<thead>
								<tr>	
									<th>Category Name</th>
									<th>Subcategory Name</th>
									<th>Manufacture Name</th>
									<th>Product ItemNumber</th>
									<th>Gold Quality</th>
									<th>Gold Weight</th>
									<th>Gender</th>
									<th>Diamond Shape</th>
									<th>Diamond Quality Size</th>
									<th>Diamond Pisces</th>
									<th>Diamond Weight</th>
									</tr>
							</thead>
							<tbody>
								@foreach($wrongdata as $row)
								<tr>
									<td>{{$row['category']}}</td>
									<td>{{$row['subCategory']}}</td>
									<td>{{$row['manufacture']}}</td>
									<td>{{$row['productItemNumber']}}</td>
									<td>{{$row['goldQuality']}}</td>
									<td>{{$row['goldWeight']}}</td>
									<td>{{$row['gender']}}</td>
									<td>{{$row['diamondShape']}}</td>
									<td>{{$row['diamondQualitySize']}}</td>
									<td>{{$row['diamondPisces']}}</td>
									<td>{{$row['diamondWeight']}}</td>
									
								</tr>
								@endforeach

							</tbody>
						</table>

						@endif	

					</div>
				</div>
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
		<!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>
<!--END CONTENT BODY-->
</div>


@endsection
@section('pagelevel_plugins')
<script src="{{ URL::to('public/admin/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('public/admin/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('public/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
@endsection
@section('pagelevel_script')
<script src="{{ URL::to('public/admin/assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
@endsection