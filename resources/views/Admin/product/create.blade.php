@extends('layouts.admin')
@section('title')
    Iconiq - Create Product
@endsection
@section('pagelevel_css')
    <link href="{{URL::to('public/admin/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{URL::to('public/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{URL::to('public/admin/assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{URL::to('public/admin/assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{URL::to('public/admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{URL::to('public/admin/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{URL::to('public/admin/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{URL::to('public/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}"
          rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!--BEGIN CONTENT BODY-->
        <div class="page-content">

            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="#">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Product</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Create</span>
                    </li>
                </ul>
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Create Product </h1>
            <!-- END PAGE TITLE-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="portlet light portlet-fit portlet-form bordered">
                        <div class="portlet-title">
                            <div class="caption">
                            <span class="caption-subject sbold uppercase">Product
                            </span>
                            </div>
                            <div class="table-toolbar pull-right">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="btn-group">
                                            <a class="btn sbold green btn-outline sbold" data-toggle="modal"
                                               href="#basic">Bluk Upload <i class="fa fa-plus"></i></a>


                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="portlet-body">
                            <!-- BEGIN FORM-->
                            {{  Form::open(array('url'=>'admin/product/store' , 'method' =>'POST','class'=>'form-horizontal','id'=>'add_product','files'=>'true'))}}
                            <div class="form-body">
                                <ul style="display:inline-block;">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>

                                <div class="portlet-title">
                                    <!--Start Product Details -->
                                    <div class="caption">
                                <span class="caption-subject sbold  ">Product Details
                                </span>
                                    </div>
                                    <hr>


                                    <div class="form-group">

                                        <div class="col-md-3">

                                            {{ Form::select('categoryId',$category,'',array('id'=>'category','class'=>'form-control','data-required'=>'1'))}}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::select('subCategoryId',$SubCategory,'',array('id'=>'subCategory','class'=>'form-control','data-required'=>'1'))}}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::select('manufactureId',$manufacture,'',array('id'=>'subCategory','class'=>'form-control','data-required'=>'1'))}}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-3">
                                            {{ Form::select('gender',array(''=>'Select Gender','male'=>'Male','female'=>'Female'),'',array('id'=>'subCategory','class'=>'form-control','data-required'=>'1'))}}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::text('productItemNumber','',array('class'=>'form-control','data-required'=>'1','required'=>'true','placeholder'=>'Item Style Number'))}}
                                        </div>

                                    </div>

                                </div>
                                <!--End Product Details -->
                                <br>
                                <!--Start Diamond Details -->

                                <div class="portlet-title">

                                    <div class="caption">
                        <span class="caption-subject sbold  ">Diamond Field
                        </span>
                                    </div>
                                    <hr>

                                    <div class="form-group">

                                        <div class="col-md-3">
                                            {{ Form::text('diamondShape','',array('class'=>'form-control','placeholder'=>'Diamond Shape'))}}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::number('diamondQualitySize','',array('class'=>'form-control','data-required'=>'1','placeholder'=>'Diamond Quality Size'))}}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::number('diamondPisces','',array('class'=>'form-control','data-required'=>'1','placeholder'=>'Diamond Pisces'))}}
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <div class="col-md-3">
                                            {{ Form::number('diamondWeight','',array('class'=>'form-control','data-required'=>'1','id'=>'diamondWeight','placeholder'=>'Diamond Weight'))}}
                                        </div>


                                    </div>


                                </div>
                                <!-- End Diamond Details -->
                                <br/>
                                <!--Start Gold  Details-->
                                <div class="portlet-title">

                                    <div class="caption">
                    <span class="caption-subject sbold  ">Gold Field
                    </span>
                                    </div>
                                    <hr>
                                    <div class="form-group">

                                        <div class="col-md-3">
                                            {{ Form::number('goldQuality','',array('class'=>'form-control','data-required'=>'1','required'=>'true','placeholder'=>'Gold Quality'))}}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::number('goldWeight','',array('class'=>'form-control','data-required'=>'1','id'=>'goldWeight','required'=>'true','placeholder'=>'Gold Weight'))}}
                                        </div>

                                    </div>


                                </div>
                                <!-- End Gold Details -->
                                <br/>
                                <!-- Start Stone Details -->
                                <div class="portlet-title">

                                    <div class="caption">
                    <span class="caption-subject sbold  ">Stone Field
                    </span>
                                    </div>
                                    <hr>
                                    <div class="form-group">

                                        <div class="col-md-3">
                                            {{ Form::text('stoneShape','',array('class'=>'form-control','placeholder'=>'Stone Shape'))}}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::text('stoneQuality','',array('class'=>'form-control','placeholder'=>'Stone Quality'))}}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::number('stoneSize','',array('class'=>'form-control','placeholder'=>'Stone Size'))}}
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-md-3">
                                            {{ Form::number('stonePisces','',array('class'=>'form-control','placeholder'=>'Stone Pisces'))}}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::number('stoneWeight','',array('class'=>'form-control','placeholder'=>'Stone Weight','id'=>'stoneWeight'))}}
                                        </div>

                                    </div>

                                </div>
                                <!-- End Stone Details -->
                                <br/>

                                <!--Start Image Upload -->

                                <br>
                                <div class="portlet-title">
                                    <hr>
                                    <div class="caption">
                    <span class="caption-subject sbold  "> Product Image
                    </span>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-3 pull-right"></div>

                                        <div class="col-md-3">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="input-group input-large">
                                                    <div class="form-control uneditable-input input-fixed input-medium"
                                                         data-trigger="fileinput">
                                                        <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                        <span class="fileinput-filename"> </span>
                                                    </div>
                                                    <span class="input-group-addon btn default btn-file">
                                    <span class="fileinput-new"> Select file </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="hidden"> {{ Form::file('productImageName[]',array('multiple'=>'true'))}} </span>
                                                    <a href="javascript:;"
                                                       class="input-group-addon btn red fileinput-exists"
                                                       data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        {{ Form::submit('submit',array('class'=>'btn green')) }}
                                        <a href="{{ URL::to('admin/company')}}" class='btn gray-salsa btn-outline'>Cancel</a>
                                    </div>
                                </div>
                            </div>
                        {{ Form::close()}}
                        <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
            </div>


        </div>
        <!--END CONTENT BODY-->
    </div>
    <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Bulk Upload</h4>
                </div>
                {{  Form::open(array('url'=>'admin/product/upload' , 'method' =>'POST','class'=>'form-horizontal','id'=>'bulk_product','files'=>'true'))}}
                <div class="modal-body">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="input-group input-large">
                            <div class="form-control uneditable-input input-fixed input-medium"
                                 data-trigger="fileinput">
                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                <span class="fileinput-filename"> </span>
                            </div>
                            <span class="input-group-addon btn default btn-file">
                                    <span class="fileinput-new"> Select file </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="hidden" id="importfile"> {{ Form::file('bulkproduct')}} </span>
                            <a href="javascript:;" class="input-group-addon btn red fileinput-exists"
                               data-dismiss="fileinput"> Remove </a>
                        </div>
                    </div>
                    <p>Upload Only .xlsx or .csv </p>
                    <p>Download Sample File <a href="{{URL::to('admin/product/sampledownload')}}">Click Here..</a></p>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn green">Submit</button>
                </div>
                {{ Form::close()}}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
@section('pagelevel_plugins')


    <!-- old-->
    <script src="{{ URL::to('public/admin/assets/global/plugins/select2/js/select2.full.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::to('public/admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::to('public/admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::to('public/admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"
            type="text/javascript"></script>

@endsection
@section('pagelevel_script')


    <script type="text/javascript">
        $(document).ready(function () {
            // for more info visit the official plugin documentation:
            // http://docs.jquery.com/Plugins/Validation


            var form1 = $('#add_product');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }

                },
                rules: {
                    productName: {
                        minlength: 2,
                        required: true
                    },
                    categoryId: {

                        required: true
                    },
                    subCategoryId: {

                        required: true
                    },
                    gender: {
                        required: true
                    },
                    productItemNumber: {
                        required: true
                    },
                    diamondQualitySize: {
                        number: true
                    },
                    diamondPisces: {
                        number: true
                    },
                    diamondWeight: {
                        number: true
                    },

                    goldQuality: {
                        number: true,
                        required: true
                    },
                    goldWeight: {
                        number: true,
                        required: true
                    },

                    stoneSize: {
                        number: true
                    },
                    stonePisces: {
                        number: true
                    },
                    stoneWeight: {
                        number: true
                    },


                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },
                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
            });


        });
    </script>

    <script src="{{ URL::to('public/admin/assets/pages/scripts/form-validation.js')}}" type="text/javascript"></script>
    s
    <script src="{{ URL::to('public/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}"
            type="text/javascript"></script>
@endsection