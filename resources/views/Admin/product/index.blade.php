@extends('layouts.admin')
@section('title')
Iconiq-Product
@endsection
@section('pagelevel_css')
<link href="{{ URL::to('public/admin/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--BEGIN CONTENT BODY-->
    <div class="page-content">

        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Product</a>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Product List </h1>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                {!! Session::get('message') !!}
                @endif
            </div>
        </div>
        <!-- END PAGE TITLE-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"> Product List </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">

                                        <a href="{{ URL::to('admin/product/create')}}" class="btn sbold green"> Add Product
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="subcategory_list">
                            <thead>
                                <tr>

                                    <th>Sub Category</th>
                                    
                                    <th>Item Number</th>
                                    <th>Diamond Weight</th>
                                    <th>Gold Weight</th>
                                    <th>Status</th>

                                    <th>Product Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($product as $row)
                                <tr class="odd gradeX">

                                    <td>{{ $row->subcategory->subCategoryName}}</td>
                                    

                                    <td>{{$row->productItemNumber}}</td>
                                    <td>{{$row->diamondWeight}}</td>
                                    <td>{{$row->goldWeight}}</td>
                                    <td><a href="{{URL::to('admin/product/block/'.$row->id)}}" ><label class="btn {{$row->status == 'Active'? 'green' : 'red'}} btn-outline btn-circle btn-sm active" >{{$row->status}}</label></a></td>
                                    <td>

                                        @if(!empty($row->productImage) && file_exists('public/'.$row->productImage))
                                        <?php $imagezUrl = URL::to('public/'.$row->productImage) ?>
                                        @else                                  
                                        <?php $imagezUrl = URL::to('public/default_image/default_product.jpg') ?>
                                        @endif
                                        <img src="{{ $imagezUrl }}" width="70" height="70"/> 


                                        
                                    </td>
                                    <td> 
                                        <div class="btn-group pull-right">
                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>

                                                    <a data-toggle="modal" href="#basic" class="shows" data-id="{{$row->id}}" ><i class="icon-tag"></i>View  </a>


                                                </li>
                                                <li>
                                                 <a href="{{URL::to('admin/product/edit/'.$row->id)}}">
                                                    <i class="icon-tag"></i> Edit </a>
                                                </li>
                                                <li>
                                                 <a href="{{URL::to('admin/product/remove/'.$row->id)}}">
                                                    <i class="icon-tag"></i> Remove </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <!-- Modal View start -->
                                <div class="modal fade" id="basic" tabindex="-1" role="basic"  aria-hidden="true">

                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ></button>
                                                <h4 class="modal-title">Product Details</h4>
                                            </div>

                                            <div class="modal-body"> 
                                                <div class="scroller" style="height:300px; width:500px; " data-always-visible="1" data-rail-visible1="1">

                                                     <div class="form-group">
                                                    <label class="control-label col-md-4">Item Style Code</label> 
                                                    <div class="col-md-6" >
                                                        <label class="control-label col-md-4" id="productItemNumber"></label> 
                                                    </div>
                                                    <label class="control-label col-md-4">Diamond Quality </label>
                                                    <div class="col-md-6">
                                                        <label class="control-label col-md-4"  id="diamondQty" ></label>K

                                                    </div>

                                                    <label class="control-label col-md-4">Diamond Pisces </label>
                                                    <div class="col-md-6">

                                                        <label class="control-label col-md-4" id="diamondPisces"></label> 
                                                    </div>
                                                    <label class="control-label col-md-4">Diamond Weight </label>
                                                    <div class="col-md-6">

                                                        <label class="control-label col-md-4" id="diamondWeight"></label>gm 
                                                    </div>
                                                    <label class="control-label col-md-4">Gold Quality</label>
                                                    <div class="col-md-6">

                                                        <label class="control-label col-md-4" id="goldQty"></label>K 
                                                    </div>
                                                    <label class="control-label col-md-4">Gold Weight </label>
                                                    <div class="col-md-6">

                                                        <label class="control-label col-md-4" id="goldWeight"></label>gm 
                                                    </div>
                                                    <label class="control-label col-md-4">Stone Quality </label>
                                                    <div class="col-md-6">

                                                        <label class="control-label col-md-4" id="stoneqty"></label> 
                                                    </div>
                                                    <label class="control-label col-md-4">Stone Pisces </label>
                                                    <div class="col-md-6">

                                                        <label class="control-label col-md-4" id="stonePisces"></label> 
                                                    </div>
                                                    <label class="control-label col-md-4">Stone Weight </label>
                                                    <div class="col-md-6">

                                                        <label class="control-label col-md-4" id="stoneWeight"></label>gm 
                                                    </div>
                                                     </div>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal View End -->

                                @endforeach  
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<!--END CONTENT BODY-->
</div>
@endsection
@section('pagelevel_plugins')
<script src="{{ URL::to('public/admin/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('public/admin/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('public/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
@endsection
@section('pagelevel_script')
<script type="text/javascript">
    $(document).ready(function(){
        var table = $('#subcategory_list');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
            [10, 15, 20, -1],
                [10, 15, 20, "All"] // change per page values here
                ],
            // set the initial value
            "pageLength": 10,
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {  // set default column settings
                    "searchable": true,
                    'width' : '25%',
                    'targets': [0]
                }, 
                {

                    "searchable": true,
                    'width' : '20%',
                    "targets": [1]
                },
                {

                    "searchable": true,
                    'width' : '20%',
                    "targets": [2]
                },
                {

                    "searchable": true,
                    'width' : '20%',
                    "targets": [3]
                },
                {

                    "searchable": true,
                    'width' : '20%',
                    "targets": [4]
                },
                
                ],
            "order": [] // set first column as a default sort by asc
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });
    });

</script>


<script>
    $('.shows').click(function(){       
      var id = $(this).data('id');
      $.ajax({  
        type: "POST",  
        data: {id:id,_token:"{{ csrf_token()}}"}, 
        url: "{{('product/view/{id}')}}",

        success:function(value){       

            $.each(value, function(index, item) {           
                $('#productItemNumber').text(item.productItemNumber);
                $('#diamondQty').text(item.diamondQualitySize);
                $('#diamondPisces').text(item.diamondPisces);
                $('#diamondWeight').text(item.diamondWeight);
                $('#goldQty').text(item.goldQuality);
                $('#email').text(item.email);
                $('#goldWeight').text(item.goldWeight);
                $('#stoneqty').text(item.stoneQuality);
                $('#stonePisces').text(item.stonePisces);
                $('#stoneWeight').text(item.stoneWeight);
                

            });
        }
    });
  });

</script>
<script src="{{ URL::to('public/admin/assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
@endsection

