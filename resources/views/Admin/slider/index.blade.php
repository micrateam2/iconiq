@extends('layouts.admin')
@section('title')
Iconiq-Slider
@endsection
@section('pagelevel_css')
<link href="{{ URL::to('public/admin/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--BEGIN CONTENT BODY-->
    <div class="page-content">
        
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Slider</a>
                </li>
            </ul>
            
        </div>
        
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Slider List </h1>
        
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                {!! Session::get('message') !!}
                @endif
            </div>
        </div>
        <!-- END PAGE TITLE-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"> Slider List </span>
                        </div>
                    </div>
                    
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">

                                        <a href="{{ URL::to('admin/slider/create')}}" class="btn sbold green"> Add Slider
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="category_list">
                            <thead>
                                <tr>
                                    <th>Serial Number</th>
                                    <th>Slider Name</th>
                                    <th>Slider Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($slider as $row)
                               <tr class="odd gradeX">
                                <td>CT{{ $row->id}}</td>
                                <td>{{ $row->sliderName}}</td>
                                <td>
                                    @if(!empty($row->sliderImage) && file_exists('public/'.$row->sliderImage))
                                    <?php $imagezUrl = URL::to('public/'.$row->sliderImage) ?>
                                    @else
                                    <?php $imagezUrl = URL::to('public/default_image/default_category.jpg') ?>
                                    @endif
                                    <img src="{{ $imagezUrl }}" width="100" height="100"/>

                                </td>
                                <td>    
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{URL::to('admin/slider/edit/'.$row->id)}}">
                                                    <i class="icon-tag"></i> Edit </a>
                                                </li>
                                                <li>
                                                    <a href="{{URL::to('admin/slider/remove/'.$row->id)}}">
                                                        <i class="icon-tag"></i> Remove </a>
                                                    </li>
                                                    <li>
                                                     <?php if($row['sliderStatus']=='true'){ ?>
                                                             <a href="{{URL::to('admin/slider/block/'.$row->id)}}">
                                                               <i class="icon-tag"></i> Block </a>
                                                              <?php } else{ ?>
                                                               <a href="{{URL::to('admin/slider/block/'.$row->id)}}">
                                                               <i class="icon-tag"></i> UnBlock </a> 
                                                            <?php } ?>
                                                     
                                                       </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!--END CONTENT BODY-->
</div>
@endsection
@section('pagelevel_plugins')
<script src="{{ URL::to('public/admin/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('public/admin/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('public/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
@endsection
@section('pagelevel_script')
<script type="text/javascript">
    $(document).ready(function(){
        var table = $('#category_list');

        // begin first table
        table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ records",
                "infoEmpty": "No records found",
                "infoFiltered": "(filtered1 from _MAX_ total records)",
                "lengthMenu": "Show _MENU_",
                "search": "Search:",
                "zeroRecords": "No matching records found",
                "paginate": {
                    "previous":"Prev",
                    "next": "Next",
                    "last": "Last",
                    "first": "First"
                }
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

            "lengthMenu": [
            [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
                ],
            // set the initial value
            "pageLength": 5,            
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {  // set default column settings
                    "searchable": true,
                    'width' : '25%',
                    'targets': [0]
                }, 
                {

                    "searchable": true,
                    'width' : '20%',
                    "targets": [1]
                },
                {

                    "searchable": true,
                    'width' : '20%',
                    "targets": [2]
                },
                {

                    "searchable": false,
                    'width' : '20%',
                    "targets": [3]
                },
                
                ],
            "order": [] // set first column as a default sort by asc
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
        });
    });

</script>
<script src="{{ URL::to('public/admin/assets/pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
@endsection