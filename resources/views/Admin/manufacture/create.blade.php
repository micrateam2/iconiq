@extends('layouts.admin')
@section('title')
    Iconiq - Create Manufacture
@endsection
@section('pagelevel_css')
    <link href="{{URL::to('public/admin/assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{URL::to('public/admin/assets/global/plugins/select2/css/select2-bootstrap.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{URL::to('public/admin/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{URL::to('public/admin/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{URL::to('public/admin/assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{URL::to('public/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}"
          rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!--BEGIN CONTENT BODY-->
        <div class="page-content">

            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <a href="#">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Manufacture</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Create</span>
                    </li>
                </ul>
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Create Manufacture </h1>
            <!-- END PAGE TITLE-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="portlet light portlet-fit portlet-form bordered">
                        <div class="portlet-title">
                            <div class="caption">
                            <span class="caption-subject sbold uppercase">Manufacture
                            </span>
                            </div>

                        </div>
                        <div class="portlet-body">
                            <!-- BEGIN FORM-->
                            {{  Form::open(array('url'=>'admin/manufacture/store' , 'method' =>'POST','class'=>'form-horizontal','id'=>'add_category','files'=>'true'))}}
                            <div class="form-body">
                                <ul style="display:inline-block;">
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Manufacture Name
                                        <span class="required" aria-required="true"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        {{ Form::text('name','',array('class'=>'form-control','data-required'=>'1','required'=>'true'))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Manufacture phone Number
                                        {{--<span class="required" aria-required="true"> * </span>--}}
                                    </label>
                                    <div class="col-md-4">
                                        {{ Form::text('phoneNumber','',array('class'=>'form-control','data-required'=>'1'))}}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Manufacture Address
                                        {{--<span class="required" aria-required="true"> * </span>--}}
                                    </label>
                                    <div class="col-md-4">
                                        {{ Form::text('address','',array('class'=>'form-control','data-required'=>'1'))}}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Manufacture Image
                                        <span class="required" aria-required="true"> * </span></label>

                                    <div class="col-md-4">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed input-medium"
                                                     data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                <span class="fileinput-new"> Select file </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="hidden"> {{ Form::file('image',array('required'=>'true','accept'=>'image/png,image/gif,image/jpeg'))}} </span>
                                                <a href="javascript:;"
                                                   class="input-group-addon btn red fileinput-exists"
                                                   data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Manufacture Status
                                    <span class="required" aria-required="true"> * </span>
                                </label>
                                <div class="col-md-4">
                                    {{ Form::select('status',array(''=>'Select Status','Active'=>'Active','Deactive'=>'Deactive'),'',array('class'=>'form-control','data-required'=>'1','required'=>'true'))}}
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        {{ Form::submit('submit',array('class'=>'btn green')) }}
                                        <a href="{{ URL::to('admin/manufacture')}}" class='btn gray-salsa btn-outline'>Cancel</a>
                                    </div>
                                </div>
                            </div>
                        {{ Form::close()}}
                        <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
            </div>


        </div>
        <!--END CONTENT BODY-->
    </div>

@endsection
@section('pagelevel_plugins')


    <!-- old-->
    <script src="{{ URL::to('public/admin/assets/global/plugins/select2/js/select2.full.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::to('public/admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::to('public/admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::to('public/admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::to('public/admin/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::to('public/admin/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::to('public/admin/assets/global/plugins/ckeditor/ckeditor.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::to('public/admin/assets/global/plugins/bootstrap-markdown/lib/markdown.js')}}"
            type="text/javascript"></script>
    <script src="{{ URL::to('public/admin/assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js')}}"
            type="text/javascript"></script>
@endsection
@section('pagelevel_script')
    <script type="text/javascript">
        $(document).ready(function () {
            // for more info visit the official plugin documentation:
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#add_category');
            var error1 = $('.alert-danger', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    }

                },
                rules: {
                    categoryName: {
                        minlength: 2,
                        required: true
                    },
                    categoryImage: {
                        required: true
                    },

                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },
                errorPlacement: function (error, element) { // render error placement for each input type
                    var cont = $(element).parent('.input-group');
                    if (cont) {
                        cont.after(error);
                    } else {
                        element.after(error);
                    }
                },
                highlight: function (element) { // hightlight error inputs

                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
                },
                success: function (label) {
                    label
                        .closest('.form-group').removeClass('has-error'); // set success class to the control group
                },
            });


        });
    </script>

    <script src="{{ URL::to('public/admin/assets/pages/scripts/form-validation.js')}}" type="text/javascript"></script>
    <script src="{{ URL::to('public/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}"
            type="text/javascript"></script>
@endsection