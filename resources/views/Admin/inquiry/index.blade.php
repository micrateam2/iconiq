@extends('layouts.admin')
@section('title')
iconiq
@endsection
@section('pagelevel_css')
<link href="{{ URL::to('public/admin/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--BEGIN CONTENT BODY-->
    <div class="page-content">

        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Inquiry list</a>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title">Inquiry list </h1>
        <!-- END PAGE TITLE-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Inquiry list </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">

                                       <!--  <a href="{{URL::to('admin/inquiry')}}" class="btn sbold green"> Inquiry listing
                                            <i class="fa fa-plus"></i>
                                        </a> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="inquiry_list">
                            <thead>
                                <tr>
                                    <th> Sr.No. </th>
                                    <th>Client Name </th>
                                    <th> Client Email </th>                                    
                                    <th> Client PhoneNumber </th>                                  
                                    <th> Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($inquiry as $row)
                                <tr class="odd gradeX">
                                  <td>{{ $row->id}}</td>
                                  <td>{{ $row->client->firstName . ' ' . $row->client->lastName}} </td>                                  
                                  <td>{{ $row->client->email}} </td>
                                  <td>{{ $row->client->phoneNumber}} </td>                              
                                  <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                              <a href="{{url::to('admin/inquiryproduct/'.$row->id)}}">
                                                <i class="icon-tag"></i> View </a>
                                             </li>
                                             <li>
                                                <a href="{{url::to('admin/inquiry/remove/'.$row->id)}}">
                                                    <i class="icon-tag"></i> Delete </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

</div>
@endsection
@section('pagelevel_plugins')

<script src="{{ URL::to('public/admin/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('public/admin/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::to('public/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
@endsection
@section('pagelevel_script')
<script type="text/javascript">
    $(document).ready(function(){
        var table = $('#inquiry_list');

       // begin first table
       table.dataTable({

           // Internationalisation. For more info refer to http://datatables.net/manual/i18n
           "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_  to _END_  of _TOTAL_  records",
            "infoEmpty": "No records found",
            "infoFiltered": "(filtered1 from _MAX_  total records)",
            "lengthMenu": "Show _MENU_",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "paginate": {
                "previous":"Prev",
                "next": "Next",
                "last": "Last",
                "first": "First"
            }
        },

           // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

           // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

           "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

           "lengthMenu": [
           [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
                ],
            // set the initial value
            "pageLength": 5,            
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {  // set default column settings
                    'orderable': true,
                    'targets': [0]
                },
                {

                 "searchable": true,
                 "targets": [1]
             },
             {
                "searchable": true,
                "targets": [2]
            },
            {
                "searchable": true,
                "targets": [3]
            },




            ],
            "order": [
            [1, "desc"]
            ] // set first column as a default sort by asc
        });

       table.on('change', 'tbody tr .checkboxes', function () {
        $(this).parents('tr').toggleClass("active");
    });
   });
</script>
@endsection