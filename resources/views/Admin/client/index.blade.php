@extends('layouts.admin')
@section('title')
iconiq
@endsection
@section('pagelevel_css')
<link href="{{ URL::to('public/admin/assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('public/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <!--BEGIN CONTENT BODY-->
    <div class="page-content">

        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="#">Client list</a>
                </li>
            </ul>
            
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                {!! Session::get('message') !!}
                @endif
            </div>
        </div>
        <h1 class="page-title">client List </h1>
        <!-- END PAGE TITLE-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">client List </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">

                                        <a href="{{URL::to('admin/client/create')}}" class="btn sbold green"> Add client
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="client_list">
                            <thead>
                                <tr>                                    
                                    <th> Name </th>
                                    <th> Address </th>
                                    <th> PhoneNumber </th>
                                    <th> Email </th>                                   
                                    <th> State </th>
                                    <th> City </th>
                                    <th> PinCode </th>
                                    <th> ClientImage </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($client as $row)
                                <tr class="odd gradeX">                                   
                                    <td>{{ $row->firstName . ' ' . $row->lastName}} </td>
                                    <td>{{ $row->address}} </td>
                                    <td>{{ $row->phoneNumber}}</td>
                                    <td>{{ $row->email}} </td>                                   
                                    <td>{{ $row->state->stateName}} </td>
                                    <td> {{ $row->cities->cityName}} </td>
                                    <td>{{ $row->pinCode}} </td>
                                    <td><img src="{{URL::to('public/'.$row->clientImage)}}" width="50" height="30"/></td>                              
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="{{URL::to('admin/client/edit/'.$row->id)}}">
                                                        <i class="icon-tag"></i> Edit </a>
                                                        <a href="{{URL::to('admin/client/remove/'.$row->id)}}">
                                                            <i class="icon-tag"></i> Delete </a>
                                                            <!-- $('input[name="chag_sort"]').val(sort2); -->
                                                            <a class="cp" data-id ="{{$row->id}}">
                                                             <i class="icon-tag"></i> Change Password </a>
                                                             <!-- commentBtn.click(function() {alert(id), alert(name); });-->
                                                             <?php if($row['clientStatus']=='true'){ ?>
                                                             <a href="{{URL::to('admin/client/block/'.$row->id)}}">
                                                               <i class="icon-tag"></i> Block </a>
                                                              <?php } else{ ?>
                                                               <a href="{{URL::to('admin/client/block/'.$row->id)}}">
                                                               <i class="icon-tag"></i> UnBlock </a> 
                                                            <?php } ?>
                                                           </li>
                                                       </ul>
                                                   </div>
                                               </td>
                                           </tr>
                                           @endforeach
                                       </tbody>
                                   </table>
                               </div>
                           </div>
                           <!-- END EXAMPLE TABLE PORTLET-->
                       </div>
                   </div>
                   <!-- END EXAMPLE TABLE PORTLET-->
               </div>
           </div>
           <!--END CONTENT BODY-->
       </div>
       @endsection
       @section('pagelevel_plugins')
       
       <script src="{{ URL::to('public/admin/assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
       <script src="{{ URL::to('public/admin/assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
       <script src="{{ URL::to('public/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
       @endsection
       @section('pagelevel_script')


       // <script type="text/javascript">
//     $(document).ready(function () {
//     // for more info visit the official plugin documentation: 
//     // http://docs.jquery.com/Plugins/Validation

//     var addClient = $('#edit_client');
//     var error1 = $('.alert-danger', addClient);
//     var success1 = $('.alert-success', addClient);

//     addClient.validate({
//         errorElement: 'span', //default input error message container
//         errorClass: 'help-block help-block-error', // default input error message class
//         focusInvalid: false, // do not focus the last invalid input
//         ignore: "", // validate all fields including form hidden input
//         messages: {
//             select_multi: {
//                 maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
//                 minlength: jQuery.validator.format("At least {0} items must be selected")
//             }
//         },
//         rules: {
//             password: {
//                 minlength:5,
//                 required: true
//             },
//             password_confirm: {
//                 required: true,
//                 equalTo: "#password",
//                 minlength:5
//             },

//         },
//         invalidHandler: function (event, validator) { //display error alert on form submit              
//             success1.hide();
//             error1.show();
//             App.scrollTo(error1, -200);
//         },
//         errorPlacement: function (error, element) { // render error placement for each input type
//             var cont = $(element).parent('.input-group');
//             if (cont) {
//                 cont.after(error);
//             } else {
//                 element.after(error);
//             }
//         },
//         highlight: function (element) { // hightlight error inputs

//             $(element)
//                     .closest('.form-group').addClass('has-error'); // set error class to the control group
//                 },
//         unhighlight: function (element) { // revert the change done by hightlight
//             $(element)
//                     .closest('.form-group').removeClass('has-error'); // set error class to the control group
//                 },
//                 success: function (label) {
//                     label
//                     .closest('.form-group').removeClass('has-error'); // set success class to the control group
//                 },
//             });


// });
// </script>


<script type="text/javascript">
  $(document).ready(function(){
     var table = $('#client_list');

       // begin first table
       table.dataTable({

           // Internationalisation. For more info refer to http://datatables.net/manual/i18n
           "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_  to _END_  of _TOTAL_  records",
            "infoEmpty": "No records found",
            "infoFiltered": "(filtered1 from _MAX_  total records)",
            "lengthMenu": "Show _MENU_",
            "search": "Search:",
            "zeroRecords": "No matching records found",
            "paginate": {
                "previous":"Prev",
                "next": "Next",
                "last": "Last",
                "first": "First"
            }
        },

           // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

           // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
            // So when dropdowns used the scrollable div should be removed.
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

           "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

           "lengthMenu": [
           [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
                ],
            // set the initial value
            "pageLength": 5,            
            "pagingType": "bootstrap_full_number",
            "columnDefs": [
                {  // set default column settings
                    'orderable': true,
                    'targets': [0]
                },
                {

                   "searchable": true,
                   "targets": [1]
               },
               {
                "searchable": true,
                "targets": [2]
            },
            {
                "searchable": true,
                "targets": [3]
            },

            {
                "searchable": true,
                "targets": [4]
            },
            {
                "searchable": true,
                "targets": [5]
            },
            {
                "searchable": true,
                "targets": [6]
            },
            {
                "searchable": true,
                "targets": [7]
            },
            {
                "searchable": true,
                "targets": [8]
            },


            ],
            "order": [
            [1, "desc"]
            ] // set first column as a default sort by asc
        });

       $('.cp').click(function(){

        var clientid = $(this).data('id');
        $('#cid').val(clientid);
        $('#modalid').modal('show'); 

    });



       table.on('change', 'tbody tr .checkboxes', function () {
        $(this).parents('tr').toggleClass("active");
    });
   });

</script>


@endsection


<div  class="modal fade responsive-modal-change" id="modalid" tabindex="-1" aria-hidden="true">
    {{  Form::open(array('url'=>'admin/client/changepassword' , 'method' =>'POST','class'=>'form-horizontal','id'=>'change_password','files'=>'true'))}}
    <div class="col-md-8">
     {{Form::hidden('clientid','',array('id'=>'cid','placeholder' => 'Password','class'=>'col-md-12 form-control','data-required'=>'1')) }} 
 </div> 
 <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <!-- <h4 class="modal-title">Responsive & Scrollable</h4> -->
        </div>
        <div class="modal-body">
            <div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">

             <div class="form-group">
                <label class="control-label col-md-3">New Password
                    <span class="required" aria-required="true"> * </span>
                </label>
                <div class="col-md-8">
                   {{Form::password('password', array('placeholder' => 'Password','id'=>'password','class'=>'col-md-12 form-control','data-required'=>'1'))}} 
               </div>                                
           </div>
           <div class="form-group">
            <label class="control-label col-md-3">Comfirm Password
                <span class="required" aria-required="true"> * </span>
            </label>
            <div class="col-md-8">
               {{Form::password('confirm_password', array('placeholder' => 'Confirm Password','class'=>'col-md-12 form-control','data-required'=>'1'))}} 
           </div>                                
       </div>

   </div>
</div>
<div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
    <!-- <button type="button" class="btn green">Save changes</button> -->
    {{ Form::submit('submit',array('class'=>'btn green')) }}
</div>
</div>
</div>
{{ Form::close()}} 
</div>

