-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2017 at 12:49 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iconiq`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoryName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoryImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modifyBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `categoryName`, `categoryImage`, `createBy`, `modifyBy`, `created_at`, `updated_at`) VALUES
(2, 'Jewelery', 'upload/category/category_696.jpeg', '3', '3', '2017-01-19 04:23:41', '2017-01-19 04:23:41');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `stateId` int(10) UNSIGNED NOT NULL,
  `cityName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modifyBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `stateId`, `cityName`, `createBy`, `modifyBy`, `created_at`, `updated_at`) VALUES
(1, 1, 'Surat\r\n', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(10) UNSIGNED NOT NULL,
  `userId` int(10) UNSIGNED NOT NULL,
  `firstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phoneNumber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countryID` int(10) UNSIGNED NOT NULL,
  `stateID` int(10) UNSIGNED NOT NULL,
  `cityID` int(10) UNSIGNED NOT NULL,
  `pinCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clientImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modifyBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `userId`, `firstName`, `lastName`, `address`, `phoneNumber`, `email`, `password`, `countryID`, `stateID`, `cityID`, `pinCode`, `clientImage`, `createBy`, `modifyBy`, `created_at`, `updated_at`) VALUES
(1, 5, 'Abd', 'Patel', 'Test', '1234567890', 'test@gmail.com', '$2y$10$4MZfJUl1DEOx8QrlnVPw3OD7RO9bclDeLRtEuxtnp5kEKB6Z8X7x2', 1, 1, 1, '395006', 'upload/client/client_956.jpg', '1', '1', '2017-01-24 04:48:15', '2017-01-24 04:48:15'),
(2, 6, 'Jatin', 'patel', 'surat', '1234567890', 'jatin@gmail.com', '$2y$10$8IDH0IoaKviD8.ZNFl/bkuy9cDtLawfyLbVTIpx1zBg.bNqTa6Exe', 1, 1, 1, '123456', 'upload/client/client_811.jpg', '1', '1', '2017-01-24 05:01:09', '2017-01-24 05:01:09');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phoneNumber` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `createBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modifyBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(10) UNSIGNED NOT NULL,
  `sortname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `countryName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modifyBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `sortname`, `countryName`, `createBy`, `modifyBy`, `created_at`, `updated_at`) VALUES
(1, 'IN', 'India', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inquiry`
--

CREATE TABLE `inquiry` (
  `id` int(10) UNSIGNED NOT NULL,
  `clientId` int(10) UNSIGNED NOT NULL,
  `clientEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clientPhoneNumber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modifyBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inquiry`
--

INSERT INTO `inquiry` (`id`, `clientId`, `clientEmail`, `clientPhoneNumber`, `createBy`, `modifyBy`, `created_at`, `updated_at`) VALUES
(2, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:24:43', '2017-01-24 05:24:43'),
(3, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:25:21', '2017-01-24 05:25:21'),
(4, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:26:58', '2017-01-24 05:26:58'),
(5, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:39:38', '2017-01-24 05:39:38'),
(6, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:40:20', '2017-01-24 05:40:20'),
(7, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:40:37', '2017-01-24 05:40:37'),
(8, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:41:18', '2017-01-24 05:41:18'),
(9, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:42:29', '2017-01-24 05:42:29'),
(10, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:45:28', '2017-01-24 05:45:28'),
(11, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:45:55', '2017-01-24 05:45:55'),
(12, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:46:17', '2017-01-24 05:46:17'),
(13, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:46:49', '2017-01-24 05:46:49'),
(14, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:47:26', '2017-01-24 05:47:26'),
(15, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:48:29', '2017-01-24 05:48:29'),
(16, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:50:28', '2017-01-24 05:50:28'),
(17, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:53:06', '2017-01-24 05:53:06'),
(18, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:53:41', '2017-01-24 05:53:41'),
(19, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:54:59', '2017-01-24 05:54:59'),
(20, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:56:08', '2017-01-24 05:56:08'),
(21, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 05:58:07', '2017-01-24 05:58:07'),
(22, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 06:00:07', '2017-01-24 06:00:07'),
(23, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 06:01:32', '2017-01-24 06:01:32'),
(24, 2, 'jatin@gmail.com', '1234567890', '6', '6', '2017-01-24 06:05:10', '2017-01-24 06:05:10');

-- --------------------------------------------------------

--
-- Table structure for table `inquiryproduct`
--

CREATE TABLE `inquiryproduct` (
  `id` int(10) UNSIGNED NOT NULL,
  `inquiryId` int(10) UNSIGNED NOT NULL,
  `productId` int(10) UNSIGNED NOT NULL,
  `productQuantity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `productPrice` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `totalAmount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modifyBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `inquiryproduct`
--

INSERT INTO `inquiryproduct` (`id`, `inquiryId`, `productId`, `productQuantity`, `productPrice`, `totalAmount`, `createBy`, `modifyBy`, `created_at`, `updated_at`) VALUES
(1, 3, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:25:21', '2017-01-24 05:25:21'),
(2, 3, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:25:21', '2017-01-24 05:25:21'),
(3, 4, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:26:58', '2017-01-24 05:26:58'),
(4, 4, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:26:58', '2017-01-24 05:26:58'),
(5, 5, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:39:38', '2017-01-24 05:39:38'),
(6, 5, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:39:38', '2017-01-24 05:39:38'),
(7, 6, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:40:20', '2017-01-24 05:40:20'),
(8, 6, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:40:20', '2017-01-24 05:40:20'),
(9, 7, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:40:37', '2017-01-24 05:40:37'),
(10, 7, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:40:37', '2017-01-24 05:40:37'),
(11, 8, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:41:18', '2017-01-24 05:41:18'),
(12, 8, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:41:18', '2017-01-24 05:41:18'),
(13, 9, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:42:29', '2017-01-24 05:42:29'),
(14, 9, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:42:29', '2017-01-24 05:42:29'),
(15, 10, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:45:28', '2017-01-24 05:45:28'),
(16, 10, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:45:28', '2017-01-24 05:45:28'),
(17, 11, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:45:55', '2017-01-24 05:45:55'),
(18, 11, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:45:55', '2017-01-24 05:45:55'),
(19, 12, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:46:17', '2017-01-24 05:46:17'),
(20, 12, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:46:18', '2017-01-24 05:46:18'),
(21, 13, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:46:49', '2017-01-24 05:46:49'),
(22, 13, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:46:49', '2017-01-24 05:46:49'),
(23, 14, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:47:26', '2017-01-24 05:47:26'),
(24, 14, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:47:26', '2017-01-24 05:47:26'),
(25, 15, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:48:29', '2017-01-24 05:48:29'),
(26, 15, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:48:29', '2017-01-24 05:48:29'),
(27, 16, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:50:28', '2017-01-24 05:50:28'),
(28, 16, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:50:28', '2017-01-24 05:50:28'),
(29, 17, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:53:06', '2017-01-24 05:53:06'),
(30, 17, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:53:06', '2017-01-24 05:53:06'),
(31, 18, 6, '2', '12149', '24298', '6', '6', '2017-01-24 05:53:41', '2017-01-24 05:53:41'),
(32, 18, 7, '5', '56963', '284815', '6', '6', '2017-01-24 05:53:41', '2017-01-24 05:53:41'),
(33, 19, 24, '1', '54820', '54820', '6', '6', '2017-01-24 05:54:59', '2017-01-24 05:54:59'),
(34, 19, 23, '1', '50989', '50989', '6', '6', '2017-01-24 05:54:59', '2017-01-24 05:54:59'),
(35, 20, 22, '1', '59564', '59564', '6', '6', '2017-01-24 05:56:08', '2017-01-24 05:56:08'),
(36, 20, 21, '1', '73356', '73356', '6', '6', '2017-01-24 05:56:08', '2017-01-24 05:56:08'),
(37, 21, 23, '1', '50989', '50989', '6', '6', '2017-01-24 05:58:07', '2017-01-24 05:58:07'),
(38, 21, 18, '1', '91105', '91105', '6', '6', '2017-01-24 05:58:07', '2017-01-24 05:58:07'),
(39, 22, 23, '1', '50989', '50989', '6', '6', '2017-01-24 06:00:07', '2017-01-24 06:00:07'),
(40, 22, 15, '1', '53941', '53941', '6', '6', '2017-01-24 06:00:07', '2017-01-24 06:00:07'),
(41, 23, 19, '1', '94038', '94038', '6', '6', '2017-01-24 06:01:32', '2017-01-24 06:01:32'),
(42, 23, 14, '1', '60602', '60602', '6', '6', '2017-01-24 06:01:32', '2017-01-24 06:01:32'),
(43, 24, 11, '1', '58949', '58949', '6', '6', '2017-01-24 06:05:10', '2017-01-24 06:05:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_01_16_045339_Country', 1),
(2, '2014_01_16_045433_states', 1),
(3, '2014_01_16_045434_cities', 1),
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2017_01_04_174723_entrust_setup_tables', 1),
(7, '2017_01_05_072419_client', 1),
(8, '2017_01_05_072453_contactUs', 1),
(9, '2017_01_05_073154_category', 1),
(10, '2017_01_05_073235_subCategory', 1),
(11, '2017_01_05_073406_product', 1),
(12, '2017_01_05_073445_productImage', 1),
(13, '2017_01_05_073636_inquiry', 1),
(14, '2017_01_05_073751_inquiryProduct', 1),
(15, '2017_01_11_104335_slider', 1),
(16, '2017_01_20_064940_subject_add_to_contactus', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'create-users', 'Create Users', 'Create New Users', '2017-01-23 07:49:10', '2017-01-23 07:49:10'),
(2, 'edit-users', 'Edit Users', 'Edit Users', '2017-01-23 07:49:10', '2017-01-23 07:49:10'),
(3, 'delete-users', 'Delete Users', 'Delete Users', '2017-01-23 07:49:10', '2017-01-23 07:49:10');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoryId` int(10) UNSIGNED NOT NULL,
  `subCategoryId` int(10) UNSIGNED NOT NULL,
  `productName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `productItemNumber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diamondShape` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `diamondQualitySize` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `diamondPisces` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `diamondWeight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `diamondRate` int(11) DEFAULT NULL,
  `diamondAmount` int(11) NOT NULL,
  `goldQuality` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `goldWeight` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `goldRate` int(11) NOT NULL,
  `goldAmount` int(11) NOT NULL,
  `stoneShape` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stoneQuality` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stoneSize` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stonePisces` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stoneWeight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stoneRate` int(11) DEFAULT NULL,
  `stoneAmount` int(11) NOT NULL,
  `labourRate` int(11) NOT NULL,
  `labourAmount` int(11) NOT NULL,
  `totalAmount` int(11) NOT NULL,
  `isActive` enum('true','false') COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featureProduct` enum('true','false') COLLATE utf8_unicode_ci NOT NULL,
  `createBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modifyBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `categoryId`, `subCategoryId`, `productName`, `productItemNumber`, `diamondShape`, `diamondQualitySize`, `diamondPisces`, `diamondWeight`, `diamondRate`, `diamondAmount`, `goldQuality`, `goldWeight`, `goldRate`, `goldAmount`, `stoneShape`, `stoneQuality`, `stoneSize`, `stonePisces`, `stoneWeight`, `stoneRate`, `stoneAmount`, `labourRate`, `labourAmount`, `totalAmount`, `isActive`, `gender`, `featureProduct`, `createBy`, `modifyBy`, `created_at`, `updated_at`) VALUES
(1, 2, 11, ' 18K Gold Diamond Pendant', '38PD5091', 'IDEAL CUT', '0.120', '12', '0.24', 10148, 2436, '18', '1.45', 16132, 23391, '0', '0', '0', '0', '0', 0, 0, 250, 3200, 29027, 'true', 'female', 'true', '3', '3', '2017-01-20 00:00:17', '2017-01-20 00:00:17'),
(2, 2, 11, '18K Gold Diamond Pendant', '43MKJ0888P', 'IDEAL CUT', '0.070', '5', '1.4', 6090, 8526, '18', '.955', 10600, 10123, '0', '0', '0', '0', '0', 0, 0, 250, 2500, 21149, 'true', 'female', 'true', '3', '3', '2017-01-20 00:06:25', '2017-01-20 00:06:25'),
(3, 2, 11, '18K Gold Diamond Pendant', '38PD5391', 'VVS 1', '0.070', '5', '1.4', 6090, 8526, '18', '0.78', 10204, 7959, '0', '0', '0', '0', '0', 0, 0, 250, 2300, 18785, 'true', 'female', 'true', '3', '3', '2017-01-20 00:08:12', '2017-01-20 00:08:12'),
(4, 2, 11, ' 18K Gold Diamond Pendant', '38PD5398', 'VVS 1', '0.040', '10', '1.5', 4500, 6750, '18', '0.830', 12068, 10016, '0', '0', '0', '0', '0', 0, 0, 250, 3500, 20266, 'true', 'female', 'true', '3', '3', '2017-01-20 00:10:22', '2017-01-20 00:10:22'),
(5, 2, 11, '18K Gold Diamond Ohm Pendant', '21AP002616', 'IDEAL CUT', '0.60', '10', '1.2', 5220, 6264, '18', '1.248', 10401, 12980, '0', '0', '0', '0', '0', 0, 0, 250, 2500, 21744, 'true', 'female', 'true', '3', '3', '2017-01-20 00:12:45', '2017-01-20 00:12:45'),
(6, 2, 14, '18K Gold Diamond Saniya Nosepin', '89SANIYA06', 'VVS 1', '0.060', '6', '1.2', 5220, 6264, '18', '0.558', 8037, 4485, '0', '0', '0', '0', '0', 0, 0, 250, 1400, 12149, 'true', 'female', 'true', '3', '3', '2017-01-20 00:20:51', '2017-01-20 00:20:51'),
(7, 2, 8, '22K Gold Gents Bracelet', 'BRBAJ-3008', '0', '0', '0', '0', 0, 0, '22', '16.450', 3250, 53463, '0', '0', '0', '0', '0', 0, 0, 250, 3500, 56963, 'true', 'male', 'true', '3', '3', '2017-01-20 00:24:06', '2017-01-20 00:24:06'),
(8, 2, 13, ' 22K Gold Fancy Chain', 'CHBJC-1502', '0', '0', '0', '0', 0, 0, '22', '16.680', 3300, 55044, '0', '0', '0', '0', '0', 0, 0, 250, 4500, 59544, 'true', 'female', 'true', '3', '3', '2017-01-20 00:26:13', '2017-01-20 00:26:13'),
(9, 2, 9, 'Sinor Solitaire Diamond Earring', 'DD00121525', 'vvs', '0.26', '26', '3.2', 22650, 72480, '18', '2.924', 9773, 28576, '0', '0', '0', '0', '0', 0, 0, 250, 6500, 107556, 'true', 'female', 'true', '3', '3', '2017-01-20 00:29:25', '2017-01-20 00:29:25'),
(10, 2, 10, '22K Gold Fancy Peacock Necklace', 'NESGTSG-5019', '0', '0', '0', '0', 0, 0, '22', '21.231', 3500, 74309, 'AP MIXED', 'A1', '2.190', '45', '2.190', 4380, 9592, 250, 8500, 92401, 'true', 'female', 'true', '3', '3', '2017-01-20 00:33:13', '2017-01-20 00:33:13'),
(11, 2, 7, '22K Gold Fancy Bangle', 'BAMLDRG-1009', 'VVS 1', '0.60', '1', '0.012', 9500, 114, '22', '11.990', 4521, 54207, 'MOULDING-MIXED', 'A1', '0.80', '12', '0.80', 160, 128, 250, 4500, 58949, 'true', 'female', 'true', '3', '3', '2017-01-20 00:36:29', '2017-01-20 00:36:29'),
(12, 2, 12, '22K Gold Wedding Bands Ring', 'RICUPRG-4124', '0', '0', '0', '0', 0, 0, '22', '7.940', 3500, 27790, 'SIGINITY', 'A1', '0.350', '30', '3.5', 700, 2450, 250, 3200, 33440, 'true', 'female', 'true', '3', '3', '2017-01-20 00:40:14', '2017-01-20 00:40:14'),
(13, 2, 7, ' 22K Gold Mudra Screw Bangle', 'BAMUDRA-2015', '0', '0', '0', '0', 0, 0, '22', '18.830', 3500, 65905, 'Kundan', 'A1', '3.8', '16', '3.820', 4020, 15356, 250, 3500, 84761, 'true', 'female', 'true', '3', '3', '2017-01-21 03:54:24', '2017-01-21 03:54:24'),
(14, 2, 11, '22K Gold Mudra Pendant', 'LOMUDRA-2020', '0', '0', '0', '0', 0, 0, '22', '15.260', 3500, 53410, 'Kundan,Kemp', 'A1', '2.040', '5', '2.040', 1810, 3692, 250, 3500, 60602, 'true', 'female', 'true', '3', '3', '2017-01-21 03:58:44', '2017-01-21 03:58:44'),
(15, 2, 9, '22K Gold Mudra Hanging Earring', 'STMUDRA-2519', '0', '0', '0', '0', 0, 0, '22', '11.640', 3500, 40740, 'Kundan,Pearl', 'A1', '9.76', '12', '9.760', 994, 9701, 250, 3500, 53941, 'true', 'female', 'true', '3', '3', '2017-01-21 04:01:33', '2017-01-21 04:01:33'),
(16, 2, 7, '22K Gold Mudra Bangle', 'BAMUDRA-2518', '0', '0', '0', '0', 0, 0, '22', '17.750', 3500, 62125, 'Color Stone', 'A1', '3.360', '20', '3.160', 632, 1997, 250, 4500, 68622, 'true', 'female', 'true', '3', '3', '2017-01-21 04:03:01', '2017-01-21 04:03:01'),
(17, 2, 7, '22K Gold Mudra Screw Bangle', 'BAMUDRA-2512', '0', '0', '0', '0', 0, 0, '22', '24.57', 3500, 85995, 'Kundan', 'A1', '0.850', '12', '8.50', 1060, 9010, 250, 4500, 99505, 'true', 'female', 'true', '3', '3', '2017-01-21 04:06:26', '2017-01-21 04:06:26'),
(18, 2, 10, ' 22K Gold Nimah Short Necklace', 'NENIM-2018', '0', '0', '0', '0', 0, 0, '22', '21.150', 3500, 74025, 'Mixed', 'A1', '2.3', '35', '2.3', 4600, 10580, 250, 6500, 91105, 'true', 'female', 'true', '3', '3', '2017-01-21 04:11:11', '2017-01-21 04:11:11'),
(19, 2, 9, ' 22K Gold Rang Jhumki Earring', 'STRANG-2527', '0', '0', '0', '0', 0, 0, '22', '11.88', 3500, 41580, 'Ruby', 'A1', '14.9', '86', '2.98', 14751, 43958, 250, 8500, 94038, 'true', 'female', 'true', '3', '3', '2017-01-21 04:13:56', '2017-01-21 04:13:56'),
(20, 2, 9, '22K Gold Rang Fancy Hanging Earring', 'STRANG-2028', '0', '0', '0', '0', 0, 0, '22', '17.294', 3500, 60529, 'Emerald', 'A1', '1.510', '4', '3.2', 1494, 4781, 250, 7500, 72810, 'true', 'female', 'true', '3', '3', '2017-01-21 04:17:48', '2017-01-21 04:17:48'),
(21, 2, 8, '22K Gold Rang Bracelet', 'BRRANG-2521', '0', '0', '0', '0', 0, 0, '22', '17.352', 3500, 60732, 'Emerald,Ruby', 'A1', '3.10', '33', '1.7', 3573, 6074, 250, 6550, 73356, 'true', 'female', 'true', '3', '3', '2017-01-21 04:20:33', '2017-01-21 04:20:33'),
(22, 2, 8, '22K Gold Rang Bracelet', 'BRRANG-2022', '0', '0', '0', '0', 0, 0, '22', '12.184', 3500, 42644, 'Ruby', 'A1', '5.230', '7', '1.45', 5117, 7420, 250, 9500, 59564, 'true', 'female', 'true', '3', '3', '2017-01-21 04:22:36', '2017-01-21 04:22:36'),
(23, 2, 9, 'Sinor Solitaire Diamond Earring', 'DD00207133', 'vvs', '0.12', '30', '1.5', 22470, 33705, '18', '2.224', 3500, 7784, '0', '0', '0', '0', '0', 0, 0, 250, 9500, 50989, 'true', 'female', 'true', '3', '3', '2017-01-21 04:27:06', '2017-01-21 04:27:06'),
(24, 2, 9, 'Sinor Solitaire Diamond Earring', 'DD00207132', 'VVS', '0.30', '02', '0.60', 68500, 41100, '18', '1.120', 3500, 3920, '0', '0', '0', '0', '0', 0, 0, 250, 9800, 54820, 'true', 'female', 'true', '3', '3', '2017-01-21 04:29:30', '2017-01-21 04:29:30');

-- --------------------------------------------------------

--
-- Table structure for table `productimage`
--

CREATE TABLE `productimage` (
  `id` int(10) UNSIGNED NOT NULL,
  `productId` int(10) UNSIGNED NOT NULL,
  `productImageName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('true','false') COLLATE utf8_unicode_ci NOT NULL,
  `createBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modifyBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `productimage`
--

INSERT INTO `productimage` (`id`, `productId`, `productImageName`, `status`, `createBy`, `modifyBy`, `created_at`, `updated_at`) VALUES
(1, 1, 'upload/product/product_3739.jpg', 'true', '3', '3', '2017-01-20 00:03:42', '2017-01-20 00:03:42'),
(2, 2, 'upload/product/product_8072.jpg', 'true', '3', '3', '2017-01-20 00:06:25', '2017-01-20 00:06:25'),
(3, 3, 'upload/product/product_3292.jpg', 'true', '3', '3', '2017-01-20 00:08:12', '2017-01-20 00:08:12'),
(4, 4, 'upload/product/product_5869.jpg', 'true', '3', '3', '2017-01-20 00:10:22', '2017-01-20 00:10:22'),
(5, 5, 'upload/product/product_3848.jpg', 'true', '3', '3', '2017-01-20 00:12:45', '2017-01-20 00:12:45'),
(6, 6, 'upload/product/product_2675.jpg', 'true', '3', '3', '2017-01-20 00:20:51', '2017-01-20 00:20:51'),
(7, 6, 'upload/product/product_7046.jpg', 'true', '3', '3', '2017-01-20 00:20:51', '2017-01-20 00:20:51'),
(8, 6, 'upload/product/product_6524.jpg', 'true', '3', '3', '2017-01-20 00:20:51', '2017-01-20 00:20:51'),
(9, 6, 'upload/product/product_3203.jpg', 'true', '3', '3', '2017-01-20 00:20:51', '2017-01-20 00:20:51'),
(10, 7, 'upload/product/product_1254.jpg', 'true', '3', '3', '2017-01-20 00:24:06', '2017-01-20 00:24:06'),
(11, 7, 'upload/product/product_9668.jpg', 'true', '3', '3', '2017-01-20 00:24:06', '2017-01-20 00:24:06'),
(12, 7, 'upload/product/product_9830.jpg', 'true', '3', '3', '2017-01-20 00:24:06', '2017-01-20 00:24:06'),
(13, 8, 'upload/product/product_8503.jpg', 'true', '3', '3', '2017-01-20 00:26:14', '2017-01-20 00:26:14'),
(14, 8, 'upload/product/product_3017.jpg', 'true', '3', '3', '2017-01-20 00:26:14', '2017-01-20 00:26:14'),
(15, 9, 'upload/product/product_2908.jpg', 'true', '3', '3', '2017-01-20 00:29:25', '2017-01-20 00:29:25'),
(16, 9, 'upload/product/product_8085.jpg', 'true', '3', '3', '2017-01-20 00:29:25', '2017-01-20 00:29:25'),
(17, 9, 'upload/product/product_2881.jpg', 'true', '3', '3', '2017-01-20 00:29:25', '2017-01-20 00:29:25'),
(18, 9, 'upload/product/product_6654.jpg', 'true', '3', '3', '2017-01-20 00:29:25', '2017-01-20 00:29:25'),
(19, 10, 'upload/product/product_7184.jpg', 'true', '3', '3', '2017-01-20 00:33:13', '2017-01-20 00:33:13'),
(20, 10, 'upload/product/product_9807.jpg', 'true', '3', '3', '2017-01-20 00:33:13', '2017-01-20 00:33:13'),
(21, 11, 'upload/product/product_3749.jpg', 'true', '3', '3', '2017-01-20 00:36:29', '2017-01-20 00:36:29'),
(22, 11, 'upload/product/product_1883.jpg', 'true', '3', '3', '2017-01-20 00:36:30', '2017-01-20 00:36:30'),
(23, 11, 'upload/product/product_5191.jpg', 'true', '3', '3', '2017-01-20 00:36:30', '2017-01-20 00:36:30'),
(24, 12, 'upload/product/product_5200.jpg', 'true', '3', '3', '2017-01-20 00:40:14', '2017-01-20 00:40:14'),
(25, 12, 'upload/product/product_1962.jpg', 'true', '3', '3', '2017-01-20 00:40:14', '2017-01-20 00:40:14'),
(26, 12, 'upload/product/product_8214.jpg', 'true', '3', '3', '2017-01-20 00:40:14', '2017-01-20 00:40:14'),
(27, 12, 'upload/product/product_4608.jpg', 'true', '3', '3', '2017-01-20 00:40:14', '2017-01-20 00:40:14'),
(28, 13, 'upload/product/product_6705.jpg', 'true', '3', '3', '2017-01-21 03:54:24', '2017-01-21 03:54:24'),
(29, 13, 'upload/product/product_4269.jpg', 'true', '3', '3', '2017-01-21 03:54:24', '2017-01-21 03:54:24'),
(30, 13, 'upload/product/product_2315.jpg', 'true', '3', '3', '2017-01-21 03:54:24', '2017-01-21 03:54:24'),
(31, 13, 'upload/product/product_1043.jpg', 'true', '3', '3', '2017-01-21 03:54:24', '2017-01-21 03:54:24'),
(32, 14, 'upload/product/product_6738.jpg', 'true', '3', '3', '2017-01-21 03:58:44', '2017-01-21 03:58:44'),
(33, 15, 'upload/product/product_6557.jpg', 'true', '3', '3', '2017-01-21 04:01:33', '2017-01-21 04:01:33'),
(34, 15, 'upload/product/product_8793.jpg', 'true', '3', '3', '2017-01-21 04:01:33', '2017-01-21 04:01:33'),
(35, 16, 'upload/product/product_3681.jpg', 'true', '3', '3', '2017-01-21 04:03:01', '2017-01-21 04:03:01'),
(36, 16, 'upload/product/product_5558.jpg', 'true', '3', '3', '2017-01-21 04:03:02', '2017-01-21 04:03:02'),
(37, 16, 'upload/product/product_5076.jpg', 'true', '3', '3', '2017-01-21 04:03:02', '2017-01-21 04:03:02'),
(38, 16, 'upload/product/product_521.jpg', 'true', '3', '3', '2017-01-21 04:03:02', '2017-01-21 04:03:02'),
(39, 17, 'upload/product/product_690.jpg', 'true', '3', '3', '2017-01-21 04:06:26', '2017-01-21 04:06:26'),
(40, 17, 'upload/product/product_8565.jpg', 'true', '3', '3', '2017-01-21 04:06:26', '2017-01-21 04:06:26'),
(41, 17, 'upload/product/product_674.jpg', 'true', '3', '3', '2017-01-21 04:06:26', '2017-01-21 04:06:26'),
(42, 18, 'upload/product/product_8240.jpg', 'true', '3', '3', '2017-01-21 04:11:11', '2017-01-21 04:11:11'),
(43, 18, 'upload/product/product_9276.jpg', 'true', '3', '3', '2017-01-21 04:11:11', '2017-01-21 04:11:11'),
(44, 19, 'upload/product/product_9774.jpg', 'true', '3', '3', '2017-01-21 04:13:56', '2017-01-21 04:13:56'),
(45, 19, 'upload/product/product_2031.jpg', 'true', '3', '3', '2017-01-21 04:13:56', '2017-01-21 04:13:56'),
(46, 20, 'upload/product/product_2309.jpg', 'true', '3', '3', '2017-01-21 04:17:48', '2017-01-21 04:17:48'),
(47, 20, 'upload/product/product_8502.jpg', 'true', '3', '3', '2017-01-21 04:17:48', '2017-01-21 04:17:48'),
(48, 21, 'upload/product/product_2744.jpg', 'true', '3', '3', '2017-01-21 04:20:33', '2017-01-21 04:20:33'),
(49, 21, 'upload/product/product_1752.jpg', 'true', '3', '3', '2017-01-21 04:20:33', '2017-01-21 04:20:33'),
(50, 21, 'upload/product/product_2829.jpg', 'true', '3', '3', '2017-01-21 04:20:33', '2017-01-21 04:20:33'),
(51, 22, 'upload/product/product_2066.jpg', 'true', '3', '3', '2017-01-21 04:22:36', '2017-01-21 04:22:36'),
(52, 22, 'upload/product/product_1400.jpg', 'true', '3', '3', '2017-01-21 04:22:37', '2017-01-21 04:22:37'),
(53, 22, 'upload/product/product_6127.jpg', 'true', '3', '3', '2017-01-21 04:22:37', '2017-01-21 04:22:37'),
(54, 22, 'upload/product/product_1958.jpg', 'true', '3', '3', '2017-01-21 04:22:37', '2017-01-21 04:22:37'),
(55, 23, 'upload/product/product_9046.jpg', 'true', '3', '3', '2017-01-21 04:27:06', '2017-01-21 04:27:06'),
(56, 23, 'upload/product/product_2975.jpg', 'true', '3', '3', '2017-01-21 04:27:06', '2017-01-21 04:27:06'),
(57, 23, 'upload/product/product_6028.jpg', 'true', '3', '3', '2017-01-21 04:27:06', '2017-01-21 04:27:06'),
(58, 23, 'upload/product/product_6805.jpg', 'true', '3', '3', '2017-01-21 04:27:06', '2017-01-21 04:27:06'),
(59, 24, 'upload/product/product_8563.jpg', 'true', '3', '3', '2017-01-21 04:29:30', '2017-01-21 04:29:30'),
(60, 24, 'upload/product/product_7784.jpg', 'true', '3', '3', '2017-01-21 04:29:30', '2017-01-21 04:29:30'),
(61, 24, 'upload/product/product_7771.jpg', 'true', '3', '3', '2017-01-21 04:29:30', '2017-01-21 04:29:30'),
(62, 24, 'upload/product/product_4310.jpg', 'true', '3', '3', '2017-01-21 04:29:30', '2017-01-21 04:29:30');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', 'User is Admin of Iconiq', '2017-01-23 07:49:09', '2017-01-23 07:49:09'),
(2, 'client', 'Client', 'User is Client of Iconiq', '2017-01-23 07:49:10', '2017-01-23 07:49:10');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 2),
(6, 2);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(10) UNSIGNED NOT NULL,
  `sliderName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sliderImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sliderStatus` enum('true','false') COLLATE utf8_unicode_ci NOT NULL,
  `createBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modifyBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `sliderName`, `sliderImage`, `sliderStatus`, `createBy`, `modifyBy`, `created_at`, `updated_at`) VALUES
(1, 'Test', 'upload/slider/slider_603.jpg', 'true', '3', '3', '2017-01-19 03:54:48', '2017-01-19 03:54:48'),
(2, 'test1', 'upload/slider/slider_917.jpg', 'true', '3', '3', '2017-01-19 03:55:01', '2017-01-19 03:55:01'),
(3, 'test3', 'upload/slider/slider_786.jpg', 'true', '3', '3', '2017-01-19 03:55:12', '2017-01-19 03:55:12'),
(5, 'slider4', 'upload/slider/slider_646.JPG', 'true', '3', '3', '2017-01-20 23:43:09', '2017-01-20 23:43:09');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `countryId` int(10) UNSIGNED NOT NULL,
  `stateName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modifyBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `countryId`, `stateName`, `createBy`, `modifyBy`, `created_at`, `updated_at`) VALUES
(1, 1, 'Gujarat', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoryId` int(10) UNSIGNED NOT NULL,
  `subCategoryName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subCategoryImage` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modifyBy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `categoryId`, `subCategoryName`, `subCategoryImage`, `createBy`, `modifyBy`, `created_at`, `updated_at`) VALUES
(7, 2, 'Bangels', 'upload/subcategory/subcategory_258.jpg', '3', '3', '2017-01-19 04:24:26', '2017-01-19 04:24:26'),
(8, 2, 'Bracelet', 'upload/subcategory/subcategory_732.jpg', '3', '3', '2017-01-19 04:24:40', '2017-01-19 04:24:40'),
(9, 2, 'Earrings', 'upload/subcategory/category_309.jpg', '3', '3', '2017-01-19 04:24:56', '2017-01-19 04:24:56'),
(10, 2, 'Nackless', 'upload/subcategory/subcategory_642.jpg', '3', '3', '2017-01-19 04:25:14', '2017-01-19 04:25:14'),
(11, 2, 'Pandelset', 'upload/subcategory/subcategory_281.jpg', '3', '3', '2017-01-19 04:25:30', '2017-01-19 04:25:30'),
(12, 2, 'Rings', 'upload/subcategory/subcategory_230.jpg', '3', '3', '2017-01-19 04:25:46', '2017-01-19 04:25:46'),
(13, 2, 'Chain', 'upload/subcategory/subcategory_598.jpg', '3', '3', '2017-01-19 23:30:16', '2017-01-19 23:30:16'),
(14, 2, 'Nosepin', 'upload/subcategory/subcategory_820.jpg', '3', '3', '2017-01-19 23:32:39', '2017-01-19 23:32:39');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone_number`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dhaval', 'Patel', 'thummardhaval64@gmail.com', '$2y$10$7fTgiPt1NJwlhfJ8Pcl0ruWYSRg1dmd47jpluL6njiNedl4dS8.bC', '9687510257', 'DHIG0igGZlM8SAtdrUsvjsZS5yqQvoIinP0zgZQQn2jTq89N9UsMt9qAadeS', '2017-01-23 07:49:11', '2017-01-24 05:01:47'),
(2, 'Mayur', 'Patel', 'meerhirpara009@gmail.com', '$2y$10$uQBfW8QdEdIK2Svaramy5.K//A/3g9pDIkifF..QrtSigLgUJ8aJy', '1234567890', NULL, '2017-01-23 07:49:11', '2017-01-23 07:49:11'),
(3, 'Punit', 'Kathiriya', 'punitkathiriya@gmail.com', '$2y$10$yl/TcXQHN57OH6/wfe8luuXkAiJVVrgtDkZxWC3CYeVcl9WuuakeS', '1234567890', NULL, '2017-01-23 07:49:11', '2017-01-23 07:49:11'),
(4, 'Jatin', 'Raiyani', 'jatin@micrasolution.com', '$2y$10$YALShawNVL6CxeevW4g6FOuaWe14J.7ib0ovaOdwrfjRvLp5VsL66', '8469536345', NULL, '2017-01-23 07:49:12', '2017-01-23 07:49:12'),
(5, NULL, NULL, 'test@gmail.com', '$2y$10$wl0vV4vCQtUA6.iQ9hgRYuKL2OsX8XUi8JZUqL6nnfyWV/gdETMpC', NULL, '67FQkxhKyfvJddRaDHddyIpI6xffDOk419Oo91VFq61vlQz6t0zvELV0yJUJ', '2017-01-24 04:48:15', '2017-01-24 04:58:15'),
(6, 'Jatin', 'patel', 'jatin@gmail.com', '$2y$10$Z5bmFbhtpAnKvb7ArZWodervBIGQCuceVVez25rNn73Ajwun1yCHK', NULL, 're3S5BTEsRvgyNRkQpTXETkDVOD1D7V9ayIqI5fmtWROM1zni344PHHjcDhM', '2017-01-24 05:01:08', '2017-01-24 06:05:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_stateid_foreign` (`stateId`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_userid_foreign` (`userId`),
  ADD KEY `client_countryid_foreign` (`countryID`),
  ADD KEY `client_stateid_foreign` (`stateID`),
  ADD KEY `client_cityid_foreign` (`cityID`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquiry`
--
ALTER TABLE `inquiry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inquiry_clientid_foreign` (`clientId`);

--
-- Indexes for table `inquiryproduct`
--
ALTER TABLE `inquiryproduct`
  ADD PRIMARY KEY (`id`),
  ADD KEY `inquiryproduct_inquiryid_foreign` (`inquiryId`),
  ADD KEY `inquiryproduct_productid_foreign` (`productId`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_categoryid_foreign` (`categoryId`),
  ADD KEY `product_subcategoryid_foreign` (`subCategoryId`);

--
-- Indexes for table `productimage`
--
ALTER TABLE `productimage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productimage_productid_foreign` (`productId`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `states_countryid_foreign` (`countryId`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subcategory_categoryid_foreign` (`categoryId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `inquiry`
--
ALTER TABLE `inquiry`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `inquiryproduct`
--
ALTER TABLE `inquiryproduct`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `productimage`
--
ALTER TABLE `productimage`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_stateid_foreign` FOREIGN KEY (`stateId`) REFERENCES `states` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `client_cityid_foreign` FOREIGN KEY (`cityID`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `client_countryid_foreign` FOREIGN KEY (`countryID`) REFERENCES `country` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `client_stateid_foreign` FOREIGN KEY (`stateID`) REFERENCES `states` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `client_userid_foreign` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `inquiry`
--
ALTER TABLE `inquiry`
  ADD CONSTRAINT `inquiry_clientid_foreign` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `inquiryproduct`
--
ALTER TABLE `inquiryproduct`
  ADD CONSTRAINT `inquiryproduct_inquiryid_foreign` FOREIGN KEY (`inquiryId`) REFERENCES `inquiry` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `inquiryproduct_productid_foreign` FOREIGN KEY (`productId`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_categoryid_foreign` FOREIGN KEY (`categoryId`) REFERENCES `category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_subcategoryid_foreign` FOREIGN KEY (`subCategoryId`) REFERENCES `subcategory` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `productimage`
--
ALTER TABLE `productimage`
  ADD CONSTRAINT `productimage_productid_foreign` FOREIGN KEY (`productId`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `states`
--
ALTER TABLE `states`
  ADD CONSTRAINT `states_countryid_foreign` FOREIGN KEY (`countryId`) REFERENCES `country` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD CONSTRAINT `subcategory_categoryid_foreign` FOREIGN KEY (`categoryId`) REFERENCES `category` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
